package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.StartupController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class RemoveUser extends Composite {

	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	
	Label lblusername;
	CCombo cmbusers;
	String struser;
	Button btnremove;
	Button btncancel;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strype;
	static Display display;
	
	public RemoveUser(Composite parent,int style) {
		// TODO Auto-generated constructor stub
		super(parent, style);
		
		FormLayout formlayout =new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		
		MainShell.lblLogo.setVisible(false);
		MainShell.lblLine.setVisible(false);
		MainShell.lblOrgDetails.setVisible(false);
		
		strToYear =  globals.session[3].toString();
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New romen", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&")+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);
		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New romen",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblLogo , 2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		

		Label lblchangepass = new Label(this, SWT.NONE);
		lblchangepass.setText("Remove User");
		lblchangepass.setFont(new Font(display, "Times New romen", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(45);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblchangepass.setLayoutData(layout);
		
		
		Label lblloginuser = new Label(this, SWT.NONE);
		//lblloginuser.setText("You have logged in as " +globals.session[6]);
		lblloginuser.setFont(new Font(display, "Times New romen", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblchangepass,10);
		layout.left = new FormAttachment(42);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblloginuser.setLayoutData(layout);
		
		if(globals.session[7].equals(-1))
		{
			lblloginuser.setText("You have logged in as  Admin");
		}
		/*if(globals.session[7].equals(0))
		{
			lblloginuser.setText("You have logged in as  Manager");
		}
		if(globals.session[7].equals(1))
		{
			lblloginuser.setText("You have logged in as  Operator");
		}*/
		
		lblusername = new Label(this, SWT.NONE);
		lblusername.setText("&Username:");
		lblusername.setFont(new Font(display, "Times New romen", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblchangepass,50);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(50);
		//layout.bottom = new FormAttachment(36);
		lblusername.setLayoutData(layout);
		lblusername.setVisible(false);
		
		cmbusers = new CCombo(this, SWT.DROP_DOWN | SWT.READ_ONLY);
		cmbusers.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblchangepass,50);
		layout.left = new FormAttachment(51);
		layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(14);
		cmbusers.setLayoutData(layout);
		cmbusers.setVisible(false);
		cmbusers.add("--Please select--");
		cmbusers.select(0);
		
		btnremove = new Button(this,SWT.PUSH);
		btnremove.setText("Remo&ve");
		btnremove.setFont(new Font(display, "Times New romen", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblusername,70);
		layout.left = new FormAttachment(40);
	//	layout.right = new FormAttachment(50);
	//	layout.bottom = new FormAttachment(70);
		btnremove.setLayoutData(layout);
		btnremove.setVisible(false);
		
		btncancel = new Button(this,SWT.PUSH);
		btncancel.setText("&Cancel");
		btncancel.setFont(new Font(display, "Times New romen", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblusername,70);
		layout.left = new FormAttachment(53);
	//	layout.right = new FormAttachment(65);
	//	layout.bottom = new FormAttachment(70);
		btncancel.setLayoutData(layout);
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		
		
		
		try {
			String username = globals.session[6].toString();
			String[] allusers=gnukhata.controllers.StartupController.getAllusers();
			for (int i = 0; i < allusers.length; i++) 
			{
				struser= allusers[i];
				String struserrole=gnukhata.controllers.StartupController.getuserrole(struser);
				cmbusers.add(struser +"-"+ struserrole);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MessageBox msg = new MessageBox(new Shell(), SWT.OK |SWT.ICON_INFORMATION) ;
			msg.setText("Information!");
			msg.setMessage("No user exists");
			msg.open();
			btncancel.getShell().getDisplay().dispose();
			MainShell ms = new MainShell(display);
		}
		
		Display.getCurrent().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				cmbusers.setBackground(FocusBackground);
				cmbusers.setForeground(FocusForeground);
			}
		});
		
		this.pack();
		this.getAccessible();
		this.setEvents();
		
		globals.setThemeColor(this, Background, Foreground);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);

	}

	private void setEvents() {
		// TODO Auto-generated method stub
		if(globals.session[7].toString().equals("-1"))
		{
			lblusername.setVisible(true);
			btnremove.setVisible(true);
			cmbusers.setVisible(true);
			cmbusers.setFocus();
			Display.getCurrent().asyncExec(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					cmbusers.setListVisible(true);
				}
			});
		}
		cmbusers.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR)
				{
						btnremove.setFocus();
					
				}
				
			}
		});
		cmbusers.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(org.eclipse.swt.events.FocusEvent arg0) {
				// TODO Auto-generated method stub
				cmbusers.setListVisible(false);
				
			}
			
			@Override
			public void focusGained(org.eclipse.swt.events.FocusEvent arg0) {
				// TODO Auto-generated method stub
				cmbusers.setListVisible(true);
				
			}
		 
		});
		
		btnremove.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btncancel.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					cmbusers.setFocus();
				}
			}
		});
		btncancel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnremove.setFocus();
				}
			}
		});
		
		
		
		
		
		
		btnremove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				if(cmbusers.isVisible()==true)
				{
					if (cmbusers.getSelectionIndex()==0)
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("Please select a User");
						alert.open();
						cmbusers.setFocus();
						return;
					}
				}
				
				//CustomDialog confirm = new CustomDialog(new Shell());
				MessageBox	 confirm = new MessageBox(new Shell(),SWT.NO | SWT.YES | SWT.ICON_QUESTION);
				confirm.setText("Confirm?");
				confirm.setMessage( "Do you wish to remove "+cmbusers.getItem(cmbusers.getSelectionIndex())+"?");
				
				int answer = confirm.open();
					 
				if(answer == SWT.YES)
				{
					
					String cmbusername = cmbusers.getItem(cmbusers.getSelectionIndex());
					int  usernameindex = cmbusername.indexOf("-"); 
					String strusername = cmbusername.substring(0, usernameindex);
					if (StartupController.removeuser(strusername))
					{
						MessageBox msg2= new MessageBox(new Shell(),SWT.OK |SWT.ICON_INFORMATION);
						msg2.setText("Informarion!");
						msg2.setMessage("User removed successfully");
						cmbusers.remove(cmbusers.getSelectionIndex());
						
						msg2.open();
						if(cmbusers.getItemCount()==1)
						{
							MessageBox msg = new MessageBox(new Shell(), SWT.OK |SWT.ICON_INFORMATION) ;
							msg.setText("Information!");
							msg.setMessage("No more users");
							msg.open();
							btncancel.getShell().getDisplay().dispose();
							MainShell ms = new MainShell(display);
						}
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								cmbusers.setFocus();
							}
						});
						cmbusers.setListVisible(true);
						
						return;
							
					}
					else
					{
						MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						msg.setText("Error!");
						msg.setMessage("Please enter valid Username ");
						msg.open();
						cmbusers.setFocus();
					}
				}
				if(answer == SWT.NO)
				{
					cmbusers.setFocus();
				}
			}
			
		});
		
		btncancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				//MessageBox msgConfirm = new MessageBox(new Shell(), SWT.YES| SWT.NO| SWT.ICON_QUESTION );
				
					btncancel.getShell().getDisplay().dispose();
					MainShell ms = new MainShell(display);
			}
				
			
			});
		
		
		
	}

	public void makeaccessible(Control c)
	{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}


	
	protected void checkSubclass()
	{
		//this is blank method so will disable the check that prevents subclassing of shells.
	}
}
