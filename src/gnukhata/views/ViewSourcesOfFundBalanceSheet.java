package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.reportmodels.sourcesandapplicationoffundsbalancesheet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;

public class ViewSourcesOfFundBalanceSheet extends Composite {

	/**
	 * @param args
	 */
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	Color tableBackground;
	Color tableForeground;
	Color lightBlue;
	TableViewer tbLiaBalanceSheet;
	
	
	TableViewerColumn colaccname;
	TableViewerColumn colamount1;
	TableViewerColumn colamount2;
	
	int tblia_width;
	int tblIndex;
	Button btnBacktoBalanceSheet;
	Button btnPrint;
	
	TableEditor lia_accnameEditor;
	TableEditor lia_tolamtEditor;
	TableEditor lia_amtEditor;
	
	ArrayList<Button> accounts = new ArrayList<Button>();
	String endDateParam = ""; 
	NumberFormat nf;
	Vector<Object> printsources = new Vector<Object>();
	static Display display;
	String strOrgType;
	ODPackage sheetstream;
	public ViewSourcesOfFundBalanceSheet(Composite parent, int style,String endDate,ArrayList<sourcesandapplicationoffundsbalancesheet> sofbal){
		super(parent, style);
		
		strOrgType = globals.session[4].toString();
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		//Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		//layout.top = new FormAttachment(1);
		//layout.left = new FormAttachment(63);
		//layout.right = new FormAttachment(87);
		//layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		//lblLogo.setLocation(getClientArea().width, getClientArea().height);
		//lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		//lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&"));
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails.setLayoutData(layout);

		Label lblOrgDetails1 = new Label(this,SWT.NONE);
		lblOrgDetails1.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails1.setText("For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(70);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails1.setLayoutData(layout);
	/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(5);
		lblLine.setLayoutData(layout);
		
		
		Label lblheadline=new Label(this, SWT.NONE);
		String strdate="";
		endDateParam = endDate;
		lblheadline.setText("\t\tStatement of Sources and Applications of Fund");
		//lblheadline.setText("\nFrom "+" " + globals.session[2]+" To "+strdate);
		lblheadline.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(33);
		layout.bottom = new FormAttachment(8);
		lblheadline.setLayoutData(layout);
		

		Label lblheadline2=new Label(this, SWT.NONE);
		strdate=endDate.substring(8)+" - "+endDate.substring(5,7)+" - "+endDate.substring(0, 4);
		endDateParam = endDate;
		lblheadline2.setText(" " + globals.session[2]+ " To " +strdate);
		lblheadline2.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,2);
		layout.left = new FormAttachment(40);
		layout.bottom = new FormAttachment(8);
		lblheadline2.setLayoutData(layout);
		
		
		tbLiaBalanceSheet= new TableViewer(this, SWT.MULTI|SWT.BORDER|SWT.LINE_SOLID|SWT.FULL_SELECTION);
		tbLiaBalanceSheet.getTable().setLinesVisible (true);
		tbLiaBalanceSheet.getTable().setHeaderVisible (true);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline2,10);
		layout.left = new FormAttachment(5);
		layout.right = new FormAttachment(95);
		layout.bottom = new FormAttachment(91);
		tbLiaBalanceSheet.getTable().setLayoutData(layout);
		
		btnBacktoBalanceSheet =new Button(this,SWT.PUSH);
		btnBacktoBalanceSheet.setText("&Back");
		btnBacktoBalanceSheet.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tbLiaBalanceSheet.getTable(),20);
		layout.left=new FormAttachment(25);
		btnBacktoBalanceSheet.setLayoutData(layout);
		btnBacktoBalanceSheet.setFocus();
		
		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tbLiaBalanceSheet.getTable(),20);
		layout.left=new FormAttachment(50);
		btnPrint.setLayoutData(layout);
				
		
	this.makeaccessible(tbLiaBalanceSheet.getTable());
	this.getAccessible();
	//this.pack();
	this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
	tblia_width = tbLiaBalanceSheet.getTable().getClientArea().width;

	 BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
		tableBackground = new Color(this.getDisplay(),255, 255, 214);
		tableForeground =  new Color(this.getDisplay(),184, 255, 148);
		lightBlue = new Color(this.getDisplay(),215,242,251);

		globals.setThemeColor(this, Background, Foreground);
		tbLiaBalanceSheet.getControl().setBackground(lightBlue);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
       //globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		

	this.setReport(sofbal);
    this.setEvents(sofbal);
    try {
    	 sheetstream= ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/SourcesAndApplicationOfFunds.ots"),"SourcesAndApplicationOfFunds");
		
	} catch (IOException e) {
		// TODO: handle exception
		e.printStackTrace();
	}
    
	}
	private void setReport(ArrayList<sourcesandapplicationoffundsbalancesheet> sofbal) 
	{
		// TODO Auto-generated method stub
		
		colaccname = new TableViewerColumn(tbLiaBalanceSheet,SWT.None);
		colaccname.getColumn().setText(" ");
		colaccname.getColumn().setAlignment(SWT.LEFT);
		if(globals.session[8].toString().equals("gnome"))
		{
			colaccname.getColumn().setWidth(56 * tblia_width /100);
		}
		if(globals.session[8].toString().equals("ubuntu"))
		{
			colaccname.getColumn().setWidth(54 * tblia_width /100);
		}
		colaccname.setLabelProvider(new ColumnLabelProvider()
		{
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
		gnukhata.controllers.reportmodels.sourcesandapplicationoffundsbalancesheet sof = (sourcesandapplicationoffundsbalancesheet) element;
		try {
			if(sof.getGroupName().trim().equals("SOURCES"))
			{
				String amount1="                                    SOURCES";
				return amount1;
			}
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//return "";
		try {
			if(sof.getGroupName().trim().equals("APPLICATIONS"))
			{
				String amount1="                                    APPLICATIONS";
				return amount1;
			}
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return sof.getGroupName();
		}	
		});
		
		colamount1 = new TableViewerColumn(tbLiaBalanceSheet,SWT.None);
		colamount1.getColumn().setText("");
		colamount1.getColumn().setAlignment(SWT.RIGHT);
		if(globals.session[8].toString().equals("gnome"))
		{
			colamount1.getColumn().setWidth(20 * tblia_width/100);
		}
		if(globals.session[8].toString().equals("ubuntu"))
		{
			colamount1.getColumn().setWidth(20 * tblia_width/100);	
		}
		
		colamount1.setLabelProvider(new ColumnLabelProvider()
		{
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
		gnukhata.controllers.reportmodels.sourcesandapplicationoffundsbalancesheet amt1 = (sourcesandapplicationoffundsbalancesheet) element;
		try {
			if(amt1.getGroupName().trim().equals("SOURCES") || amt1.getGroupName().trim().equals("APPLICATIONS"))
			{
				String amount1="Amount                                ";
				return amount1;
			}
			else
			{
			Double amount1 = Double.parseDouble(amt1.getAmount1());
			nf = NumberFormat.getInstance();
			nf.setGroupingUsed(false);
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			return nf.format(amount1);
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
		
		}	
		});
		
		
		colamount2 = new TableViewerColumn(tbLiaBalanceSheet,SWT.None);
		colamount2.getColumn().setText("");
		colamount2.getColumn().setAlignment(SWT.RIGHT);
		if(globals.session[8].toString().equals("gnome"))
		{
			colamount2.getColumn().setWidth(20 * tblia_width/100);
		}
		if(globals.session[8].toString().equals("ubuntu"))
		{
			colamount2.getColumn().setWidth(20 * tblia_width/100);	
		}
		colamount2.setLabelProvider(new ColumnLabelProvider()
		{
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
		gnukhata.controllers.reportmodels.sourcesandapplicationoffundsbalancesheet amt2 = (sourcesandapplicationoffundsbalancesheet) element;
		try {
			if(amt2.getGroupName().trim().equals("SOURCES") ||amt2.getGroupName().trim().equals("APPLICATIONS"))
			{
				String amount2="Amount                                        ";
				return amount2;
			}
			{
			Double amount2 = Double.parseDouble(amt2.getAmount2());
			nf = NumberFormat.getInstance();
			nf.setGroupingUsed(false);
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			return nf.format(amount2);
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
		
		}	
		});
		tbLiaBalanceSheet.setContentProvider(new ArrayContentProvider());
		tbLiaBalanceSheet.setInput(sofbal);
		TableItem[] items = tbLiaBalanceSheet.getTable().getItems();
		for (int rowid=0; rowid<items.length; rowid++){
		    if (rowid%2==0) 
		    {
		    	items[rowid].setBackground(tableBackground);
		    }
		    else {
		    	items[rowid].setBackground(tableForeground);
		    }
		}
		tbLiaBalanceSheet.getTable().setSelection(0);
		tbLiaBalanceSheet.getTable().setFocus();
		
	}
	
	private void setEvents(final ArrayList<sourcesandapplicationoffundsbalancesheet> sofbal)
	{
		tbLiaBalanceSheet.getControl().addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				tblIndex=tbLiaBalanceSheet.getTable().getSelectionIndex();
				tbLiaBalanceSheet.getTable().setSelection(-1);
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				tbLiaBalanceSheet.getTable().setSelection(tblIndex);
			}
		});
		
		btnBacktoBalanceSheet.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnBacktoBalanceSheet.getParent().getParent();
				btnBacktoBalanceSheet.getParent().dispose();
					
				viewBalanceSheet bs = new viewBalanceSheet(grandParent,SWT.NONE);
				bs.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
				
		
				btnPrint.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						// TODO Auto-generated method stub
						//super.widgetSelected(arg0);
						btnBacktoBalanceSheet.setFocus();
								try {
								final File sources = new File("/tmp/gnukhata/Report_Output/SourcseAndApplicationOfFunds" );
								final Sheet sourcebalSheet = sheetstream.getSpreadSheet().getFirstSheet();
								sourcebalSheet.ensureRowCount(100000);
								sourcebalSheet.getCellAt(0,0).setValue(globals.session[1].toString());
								sourcebalSheet.getCellAt(0,1).setValue("Statement Of Sources And Application Of Funds");
								sourcebalSheet.getCellAt(0,2).setValue(globals.session[2]+" To "+globals.session[2]);
								System.out.println(printsources.size());
								for(int rowcounter=0; rowcounter< sofbal.size(); rowcounter++)
								{
									sourcebalSheet.getCellAt(0,rowcounter+4).setValue(sofbal.get(rowcounter).getGroupName());
									sourcebalSheet.getCellAt(1,rowcounter+4).setValue(sofbal.get(rowcounter).getAmount1());
									sourcebalSheet.getCellAt(2,rowcounter+4).setValue(sofbal.get(rowcounter).getAmount2());
									
								}
									OOUtils.open(sourcebalSheet.getSpreadSheet().saveAs(sources));

								//OOUtils.open(sources);
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					}
				});
	
				
	}
public void makeaccessible(Control c) {
	/*
	 * getAccessible() method is the method of class Control which is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
	c.getAccessible();
}

protected void checkSubclass() {
	// this is blank method so will disable the check that prevents
	// subclassing of shells.
}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}