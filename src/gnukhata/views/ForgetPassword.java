package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.StartupController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ForgetPassword extends Shell {
	
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;

	
	
	
	
	static Display display;
	String strToYear;
	String strOrgName; 
	String strFromYear;
	
	Label lblUserName;
	Label txtUserName;
	Button btnClickHere;
	Label lblSecurityQuestion;
	Label lblAnswer;
	Text txtAnswer;
	Label lblPassword;
	Text txtPassword;
	Label lblConfirmedPassword;
	Text txtConfirmedPassword;
	Button btnUpdatePassword;
	//Button btnLogin;
	Button btnback;
	Button btnNext;
	Group grpEntry;
	int role;
	String question;
	String answer;
	String[] result;
	Boolean update;
	String res;
	String name;
	
	public ForgetPassword(String Username)
	{
		super(Display.getDefault());
		 strOrgName = globals.session[1].toString();
		 strFromYear = globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		
		//this.setFullScreen(true);
		
		FormLayout formlayout =new FormLayout();
		this.setLayout(formlayout);
		this.setText("Forgot Password");
		FormData layout = new FormData();
		Rectangle bounds = this.getDisplay().getPrimaryMonitor().getBounds();
		this.setBounds(bounds);
		name=Username;
		//this.setText(Forgot Password);
		
		
		//strToYear =  globals.session[3].toString();
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New romen", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&")+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(6);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(15);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display, "finallogo.png");
		lblLogo.setImage(globals.logo);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New romen", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New romen",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblLogo , 2);
		layout.left = new FormAttachment(2);
		lblLine.setLayoutData(layout);
		
		lblUserName = new Label(this, SWT.NONE);
		lblUserName.setText("&User Name :");
		lblUserName.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,30);
		layout.left = new FormAttachment(35);
		lblUserName.setLayoutData(layout);
		
		txtUserName = new Label(this, SWT.NONE);
		txtUserName.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		txtUserName.setText(name);
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,30);
		layout.left = new FormAttachment(52);
		txtUserName.setLayoutData(layout);
	//	txtUserName.setEditable(false);
		//txtUserName.setEnabled(false);
		
		
		lblSecurityQuestion = new Label(this, SWT.NONE);
		lblSecurityQuestion.setText("&Security Question :");
		lblSecurityQuestion.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblUserName,30);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(70); 
		lblSecurityQuestion.setLayoutData(layout);
		//lblSecurityQuestion.setVisible(false);
		
		lblAnswer = new Label(this,SWT.PUSH);
		lblAnswer.setText("Answer :");
		lblAnswer.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblSecurityQuestion,30);
		layout.left = new FormAttachment(35);
		lblAnswer.setLayoutData(layout);
		//lblAnswer.setVisible(false);
		
		txtAnswer= new Text(this,SWT.BORDER);
		txtAnswer.setFont(new Font(display, "Times New Romen", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblSecurityQuestion,30);
		layout.left = new FormAttachment(52);
		txtAnswer.setLayoutData(layout);
		txtAnswer.setFocus();
		//txtAnswer.setVisible(false);
		
		btnback = new Button(this,SWT.PUSH);
		btnback.setText("Back");
		btnback.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblAnswer,20);
		layout.left = new FormAttachment(35);
		//layout.right = new FormAttachment(45);
		btnback.setLayoutData(layout);
		
		btnNext = new Button(this,SWT.PUSH);
		btnNext.setText("Next");
		btnNext.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblAnswer,20);
		layout.left = new FormAttachment(45);
		//layout.right = new FormAttachment(45);
		btnNext.setLayoutData(layout);
		

		grpEntry = new Group(this, SWT.BORDER);
		layout = new FormData();
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(70);
		layout.top =  new FormAttachment(btnback, 5);
		layout.bottom = new FormAttachment(90);
		grpEntry.setLayoutData(layout);
		grpEntry.setLayout(new FormLayout());
		
		
		
		
		
		
		lblPassword = new Label(grpEntry, SWT.NONE);
		lblPassword.setText("&Password :");
		lblPassword.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(btnback,30);
		layout.left = new FormAttachment(25);
		lblPassword.setLayoutData(layout);
		
		txtPassword = new Text(grpEntry, SWT.BORDER);
		txtPassword.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		txtPassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(btnback,30);
		layout.left = new FormAttachment(60);
		txtPassword.setLayoutData(layout);
		
		lblConfirmedPassword = new Label(grpEntry, SWT.NONE);
		lblConfirmedPassword.setText("C&onfirm Password :");
		lblConfirmedPassword.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblPassword,30);
		layout.left = new FormAttachment(25);
		lblConfirmedPassword.setLayoutData(layout);
		
		txtConfirmedPassword = new Text(grpEntry, SWT.BORDER);
		txtConfirmedPassword.setFont(new Font(display,"Times New Romen",10,SWT.NORMAL));
		txtConfirmedPassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lblPassword,30);
		layout.left = new FormAttachment(60);
		txtConfirmedPassword.setLayoutData(layout);
		
		btnUpdatePassword = new Button(grpEntry,SWT.PUSH);
		btnUpdatePassword.setText("Update Password/&Login ");
		btnUpdatePassword.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblConfirmedPassword,30);
		layout.left = new FormAttachment(25);
		//layout.right = new FormAttachment(45);
		btnUpdatePassword.setLayoutData(layout);
		
		/*btnLogin = new Button(grpEntry,SWT.PUSH);
		btnLogin.setText("&Login");
		btnLogin.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblConfirmedPassword,30);
		layout.left = new FormAttachment(60);
		layout.right = new FormAttachment(80);
		btnLogin.setLayoutData(layout);
		*/grpEntry.pack();
		grpEntry.setVisible(false);
		lblPassword.setVisible(false);
		txtPassword.setVisible(false);
		lblConfirmedPassword.setVisible(false);
		txtConfirmedPassword.setVisible(false);
		btnUpdatePassword.setVisible(false);
	//	btnLogin.setVisible(false);
		
		this.getAccessible();
		this.setEvents();
		this.pack();
		this.open();
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);

		globals.setThemeColor(this, Background, Foreground);
	    globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);


		this.showView();
		
		
		
	}
	
	private void setEvents()
	{
		/*txtUserName.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			try
			{
			if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
			{
				result = StartupController.getQuestion(txtUserName.getText());
				String res1 = result[0];
				//System.out.print(res1);
				lblSecurityQuestion.setText( res1);
				 res = result[1];
				//txtAnswer.setFocus();
				return;
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
				alert.setText("Error!");
				alert.setMessage("nothing");
				//alert.open();
				
				
			}
		}
		});
		*/
		/*txtPassword.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			
			
		}
		
		});*/
		txtPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
				{
					if(txtPassword.getText().trim().equals(""))
					{
						MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
						msg1.setText("Error!");
						msg1.setMessage("Please enter password");
						msg1.open();
						txtPassword.setFocus();
					}
					else
					{

					txtConfirmedPassword.setFocus();
					}
					}
					
				if(e.keyCode==SWT.ARROW_UP)
				{
					txtAnswer.setEnabled(true);
					grpEntry.setVisible(false);
					lblPassword.setVisible(false);
					txtPassword.setVisible(false);
					lblConfirmedPassword.setVisible(false);
					txtConfirmedPassword.setVisible(false);
					btnUpdatePassword.setVisible(false);
					//btnLogin.setVisible(false);
					
					
					txtAnswer.setFocus();					
				}
			}
		});
		
		txtConfirmedPassword.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
			//	super.focusGained(arg0);
				if (txtPassword.getText().equals(""))
				{
				display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtPassword.setFocus();
						
						return;
				
					}
				});	
					
				}
			}
		});
		txtAnswer.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				if(txtAnswer.getText().trim().equals(""))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
					msg1.setText("Error!");
					msg1.setMessage("Please enter Answer");
					msg1.open();
					txtAnswer.setFocus();
				}
				return;
			}
		});
		
		txtAnswer.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
		if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
		{
			
			
			btnNext.notifyListeners(SWT.Selection, new Event());
			
	
		}
			
		}
		
						

		});

		btnNext.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				if(txtAnswer.getText().equals(""))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
					msg1.setText("Error!");
					msg1.setMessage("Please Enter Answer");
					msg1.open();
					
					txtAnswer.setFocus();
					return;
				
					
				}
				
				if( txtAnswer.getText().equals(res))
				{
					grpEntry.setVisible(true);
					lblPassword.setVisible(true);
					txtPassword.setVisible(true);
					lblConfirmedPassword.setVisible(true);
					txtConfirmedPassword.setVisible(true);
					btnUpdatePassword.setVisible(true);
					//btnLogin.setVisible(true);
					btnNext.setVisible(false);
					txtAnswer.setEnabled(false);
					txtPassword.setFocus();
					return;
				

				}
				
				else
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK);
					msg1.setText("Error!");
					msg1.setMessage("Answer does not match");
					msg1.open();
					
					txtAnswer.setFocus();
					txtAnswer.selectAll();
					grpEntry.setVisible(false);
					lblPassword.setVisible(false);
					txtPassword.setVisible(false);
					lblConfirmedPassword.setVisible(false);
					txtConfirmedPassword.setVisible(false);
					btnUpdatePassword.setVisible(false);
				//	btnLogin.setVisible(false);
					
					return;
				

				}

			
			}
			
		});
		txtConfirmedPassword.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			if(txtConfirmedPassword.getText().trim().equals(""))
			{
				MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
				alert.setText("Error!");
				alert.setMessage("Please Confirm your Password");
				txtConfirmedPassword.setFocus();
				//alert.open();
				return;
				
			}
			
			if(!txtPassword.getText().equals(txtConfirmedPassword.getText()))
				{
				
				MessageBox msg1 = new MessageBox(new Shell(),SWT.OK |SWT.ICON_INFORMATION);
				msg1.setText("Information!");
				msg1.setMessage("Password does not match");
				msg1.open();
			//	txtConfirmedPassword.selectAll();
				txtConfirmedPassword.setFocus();
				return;
			}

			//return;
		}
		
		});
		
		txtConfirmedPassword.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(txtPassword.getText().trim().equals(""))
				{
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtPassword.setFocus();
							return;
						}
					});
				}
				
			}
		});
		txtConfirmedPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
				{
					if(txtConfirmedPassword.getText().trim().equals(""))
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("Please Confirm your Password");
						//alert.open();
						txtConfirmedPassword.setFocus();
						return;
						
					}
							}
				if(e.keyCode==SWT.ARROW_UP)
				{
					txtPassword.setFocus();					
				}
			}
		});
		txtAnswer.addFocusListener(new FocusAdapter() {
			@Override
						public void focusGained(FocusEvent arg0) {
							// TODO Auto-generated method stub
							//super.focusLost(arg0);
				result = StartupController.getQuestion(txtUserName.getText());
				String res1 = result[0];
				//System.out.print(res1);
				lblSecurityQuestion.setText( res1);
				 res = result[1];
				txtAnswer.setFocus();
				return;
		
				
						}			
		});
		
		
		btnUpdatePassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e)
			{
		/*		if(e.keyCode==SWT.CR || e.keyCode==SWT.KEYPAD_CR)
				{
					if(txtPassword.getText().trim().equals(""))
					{
						MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg1.setText("Information!");
						msg1.setMessage("Please enter password");
						msg1.open();
						txtPassword.selectAll();
						//txtConfirmedPassword.setFocus();
					
						display.getCurrent().asyncExec(new Runnable() {
							
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtPassword.setFocus();
								return;
							}
						});
					}
				
					if(txtConfirmedPassword.getText().trim().equals(""))
					{
						MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg1.setText("Information!");
						msg1.setMessage("Please confirm password");
						msg1.open();
						txtConfirmedPassword.selectAll();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtConfirmedPassword.setFocus();
								return;
							}
						});
					}
					if(txtPassword.getText().equals(txtConfirmedPassword.getText()))
					{
						update = StartupController.updatePassword(txtUserName.getText(),txtConfirmedPassword.getText());
						MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg1.setText("Information");
							msg1.setMessage("Password Updated");
							msg1.open();
							//btnUpdatePassword.setEnabled(false);
							//btnUpdatePassword.setFocus();
							if (StartupController.login(txtUserName.getText(),txtPassword.getText()))
							{
								MessageBox msg = new MessageBox(new Shell(),SWT.OK);
								msg.setMessage("Login Successful");
								msg.open();
								dispose();
								StartupController.showMainShell(display, 2);
							}
							
							}
					else
						{
						
						MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg1.setText("Information!");
						msg1.setMessage("Error changing password");
						msg1.open();
						txtConfirmedPassword.selectAll();
						txtConfirmedPassword.setFocus();
						return;
					}
				
				}
		*/		if(e.keyCode==SWT.ARROW_UP)
				{
					txtConfirmedPassword.setFocus();
				}
			}
		});
		
	/*	btnLogin.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e)
			{
						if(e.keyCode==SWT.ARROW_UP)
				{
					btnUpdatePassword.setEnabled(true);
					btnUpdatePassword.setFocus();
				}
			}
		});
*/
		btnUpdatePassword.addSelectionListener(new SelectionAdapter() {
		
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				if(txtPassword.getText().trim().equals(""))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg1.setText("Alert!!");
					msg1.setMessage("Please enter password");
					msg1.open();
					
					txtPassword.setFocus();
					return;
				}
				if(txtConfirmedPassword.getText().trim().equals(""))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg1.setText("Alert!!");
					msg1.setMessage("Please Confirm the Password");
					msg1.open();
					
					
					txtConfirmedPassword.setFocus();
					return;
			
					
				}
				if(txtPassword.getText().equals(txtConfirmedPassword.getText()))
				{
					update = StartupController.updatePassword(txtUserName.getText(),txtConfirmedPassword.getText());
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg1.setText("Information");
						msg1.setMessage("Password Updated");
						msg1.open();
						//btnUpdatePassword.setEnabled(false);
						//btnUpdatePassword.setFocus();
						if (StartupController.login(txtUserName.getText(),txtPassword.getText()))
						{
							/*MessageBox msg = new MessageBox(new Shell(),SWT.OK);
							msg.setMessage("Login Successful");
							msg.open();
							*/dispose();
							StartupController.showMainShell(display, 2);
						}
						
						}
				else
					{
					
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg1.setText("Information!");
					msg1.setMessage("Password and Confirm Password does not match ");
					msg1.open();
					txtConfirmedPassword.selectAll();
					txtConfirmedPassword.setFocus();
					return;
				}
				
			}
		});
	
	/*	btnLogin.addSelectionListener(new SelectionAdapter(){
			
			@Override
		public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		//super.widgetSelected(arg0);
				if(txtPassword.getText().trim().equals(""))
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.OK);
					msg.setMessage("Please Enter Password");
					msg.open();
				
					txtPassword.setFocus();
					return;
				}
				if(txtConfirmedPassword.getText().trim().equals(""))
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.OK);
					msg.setMessage("Please Confirm the Password");
					msg.open();
				
					txtConfirmedPassword.setFocus();
					return;
				}
				
				if (StartupController.login(txtUserName.getText(),txtPassword.getText()))
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.OK);
					msg.setMessage("Login Successful");
					msg.open();
					dispose();
					StartupController.showMainShell(display, 2);
				}
				else
				{
					btnUpdatePassword.setEnabled(true);
					btnUpdatePassword.setFocus();
					return;
					
					
				}

			
			}			
			
		});
		
*/		txtConfirmedPassword.addKeyListener(new KeyAdapter(){
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
				{
					if(txtPassword.getText().trim().equals(""))
					{
						MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg1.setText("Alert!!");
						msg1.setMessage("Enter password");
						msg1.open();
						
						txtPassword.setFocus();
						return;
					}
					if(txtConfirmedPassword.getText().trim().equals(""))
					{
						MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg1.setText("Alert!!");
						msg1.setMessage("Please Confirm Password");
						msg1.open();
						
						txtConfirmedPassword.setFocus();
						return;
					}
					
					btnUpdatePassword.setFocus();
					
					
				}
				
			}
		});
		
		btnback.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				btnback.getParent().dispose();
				LoginForm lf = new LoginForm();
			}
		});
		
		btnback.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtAnswer.setFocus();
				}
				
			}
		});
	
	
	
	}
	
	public void makeaccessible(Control c)
	{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}
	
	protected void checkSubclass() {
		// this is blank method so will disable the check that prevents
		// subclassing of shells.
	}

	private void showView() {
		while (!this.isDisposed()) {
			if (!this.getDisplay().readAndDispatch()) {
				this.getDisplay().sleep();
				if (!this.getMaximized()) {
					this.setMaximized(true);
				}
			}

		}
		this.dispose();

	}


}
