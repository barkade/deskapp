package gnukhata.views;

import gnukhata.globals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class viewCashflow extends Composite{
	
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	
	static String strOrgName;
	static String strFromYear;
	static String strToYear;
	public static String typeFlag;
	static Display display;
	
	Text txtToDtMonth;
	Text txtToDtDay;
	Text txtToDtYear;
	long wait=0;
	Text txtFromDtDay;
	Text txtFromDtMonth;
	Text txtFromDtYear;
	
	Label lblFromDash1;
	Label lblFromDash2;
	Label dash1;
	Label dash2;
	Button btnView;
	ProgressBar selectbar;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	

	public viewCashflow(Composite parent,int style) {
	
		super(parent,style);
		//strOrgName = globals.session[1].toString();
		sdf.setLenient(false);
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		
		 MainShell.lblLogo.setVisible(false);
			MainShell.lblLine.setVisible(false);
			MainShell.lblOrgDetails.setVisible(false);
			    
		
			Label lblLogo = new Label(this, SWT.None);
			layout = new FormData();
			layout.top = new FormAttachment(1);
			layout.left = new FormAttachment(63);
			layout.right = new FormAttachment(87);
			layout.bottom = new FormAttachment(9);
			//layout.right = new FormAttachment(95);
			//layout.bottom = new FormAttachment(18);
			//lblLogo.setSize(getClientArea().width, getClientArea().height);
			lblLogo.setLocation(getClientArea().width, getClientArea().height);
			lblLogo.setLayoutData(layout);
			//Image img = new Image(display,"finallogo1.png");
			lblLogo.setImage(globals.logo);
			
			Label lblOrgDetails = new Label(this,SWT.NONE);
			lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
			lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&")+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
			layout = new FormData();
			layout.top = new FormAttachment(2);
			layout.left = new FormAttachment(2);
			//layout.right = new FormAttachment(53);
			//layout.bottom = new FormAttachment(18);
			lblOrgDetails.setLayoutData(layout);

			/*Label lblLink = new Label(this,SWT.None);
			lblLink.setText("www.gnukhata.org");
			lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
			layout = new FormData();
			layout.top = new FormAttachment(lblLogo,0);
			layout.left = new FormAttachment(65);
			//layout.right = new FormAttachment(33);
			//layout.bottom = new FormAttachment(19);
			lblLink.setLayoutData(layout);*/
			 
			Label lblLine = new Label(this,SWT.NONE);
			lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
			layout = new FormData();
			layout.top = new FormAttachment( lblLogo , 2);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(99);
			layout.bottom = new FormAttachment(22);
			lblLine.setLayoutData(layout);
			

		Label lblviewCashFlow = new Label(this, SWT.NONE);
		
		if(globals.session[4].equals("Profit Making"))
		{
			lblviewCashFlow.setText("View Cash Flow");
		}
		if(globals.session[4].equals("NGO"))
		{
			lblviewCashFlow.setText("View Receipt && Payment");
		}
		lblviewCashFlow.setFont(new Font(display, "Times New Roman", 16, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,25);
		layout.left = new FormAttachment(38);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblviewCashFlow.setLayoutData(layout);
		
		Label lblPeriod=new Label(this, SWT.NONE);
		lblPeriod.setText("Period:");
		lblPeriod.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		layout=new FormData();
		layout.top=new FormAttachment(lblviewCashFlow,35);
		layout.left=new FormAttachment(32);
		lblPeriod.setLayoutData(layout);
	
		Label lblFromDt=new Label(this, SWT.NONE);
		lblFromDt=new Label(this, SWT.NONE);
		lblFromDt.setText("&From :");
		lblFromDt.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		layout=new FormData();
		layout.top =new FormAttachment(lblviewCashFlow,35);
		layout.left=new FormAttachment(lblPeriod,10);		
		lblFromDt.setLayoutData(layout);
	    
		txtFromDtDay=new Text(this, SWT.BORDER);
		txtFromDtDay.setMessage("dd");
		txtFromDtDay.setText(strFromYear.substring(0,2));	
		txtFromDtDay.setTextLimit(2);
		layout = new FormData();
		layout.top=new FormAttachment(lblviewCashFlow,35);
		layout.left = new FormAttachment(lblFromDt, 3);
		txtFromDtDay.setLayoutData(layout);
		txtFromDtDay.setVisible(true);
				
		lblFromDash1 = new Label(this, SWT.NONE);
		lblFromDash1.setText("-");
		lblFromDash1.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD));
		layout = new FormData();
		layout.left = new FormAttachment(txtFromDtDay,2);
		layout.top = new FormAttachment(lblviewCashFlow,35);
		lblFromDash1.setLayoutData(layout);
		lblFromDash1.setVisible(true);
		
		txtFromDtMonth=new Text(this, SWT.BORDER);
		txtFromDtMonth.setMessage("mm");
		txtFromDtMonth.setText(strFromYear.substring(3,5));
		txtFromDtMonth.setTextLimit(2);
		layout = new FormData();
		layout.top=new FormAttachment(lblviewCashFlow,35);
		layout.left = new FormAttachment(lblFromDash1, 3);
		txtFromDtDay.setTabs(1);
		txtFromDtMonth.setLayoutData(layout);
		txtFromDtMonth.setVisible(true);
		
		
		lblFromDash2 = new Label(this, SWT.NONE);
		lblFromDash2.setText("-");
		lblFromDash2.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD));
		layout = new FormData();
		layout.left = new FormAttachment(txtFromDtMonth,3);
		layout.top = new FormAttachment(lblviewCashFlow,35);
		lblFromDash2.setLayoutData(layout);
		lblFromDash2.setVisible(true);
		
		
		txtFromDtYear=new Text(this, SWT.BORDER);
		txtFromDtYear.setMessage("yyyy");
		txtFromDtYear.setTextLimit(4);
		txtFromDtYear.setText(strFromYear.substring(6));
		layout = new FormData();
		layout.top=new FormAttachment(lblviewCashFlow,35);
		layout.left = new FormAttachment(lblFromDash2, 3);
		txtFromDtYear.setLayoutData(layout);
		
		Label lblToDt=new Label(this, SWT.NONE);
		lblToDt=new Label(this, SWT.NONE);
		lblToDt.setText("T&o     :");
		lblToDt.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		layout=new FormData();
		layout.top =new FormAttachment(lblFromDt, 14);
		layout.left=new FormAttachment(lblPeriod,13);
		lblToDt.setLayoutData(layout);
		
		txtToDtDay=new Text(this, SWT.BORDER);
		txtToDtDay.setMessage("dd");
		txtToDtDay.setText(strToYear.substring(0,2));
		txtToDtDay.setTextLimit(2);
		layout = new FormData();
		layout.top=new FormAttachment(lblFromDt, 14);
		layout.left = new FormAttachment(lblToDt, 4);
		txtToDtDay.setLayoutData(layout);
		txtToDtDay.setVisible(true);
		
		Label lblToDash1 = new Label(this, SWT.NONE);
		lblToDash1 = new Label(this, SWT.NONE);
		lblToDash1.setText("-");
		lblToDash1.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD));
		layout = new FormData();
		layout.left = new FormAttachment(txtToDtDay,2);
		layout.top = new FormAttachment(lblFromDt, 14);
		lblToDash1.setLayoutData(layout);
		lblToDash1.setVisible(true);
		
		txtToDtMonth=new Text(this, SWT.BORDER);
		txtToDtMonth.setMessage("mm");
		txtToDtMonth.setText(strToYear.substring(3,5));
		txtToDtMonth.setTextLimit(2);
		layout = new FormData();
		layout.top=new FormAttachment(lblFromDt, 14);
		layout.left = new FormAttachment(lblFromDash1, 3);
		txtToDtMonth.setLayoutData(layout);
		txtToDtMonth.setVisible(true);
		
		Label lblToDash2 = new Label(this, SWT.NONE);
		lblToDash2 = new Label(this, SWT.NONE);
		lblToDash2.setText("-");
		lblToDash2.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD));
		layout = new FormData();
		layout.left = new FormAttachment(txtToDtMonth,3);
		layout.top = new FormAttachment(lblFromDt, 14);
		lblToDash2.setLayoutData(layout);
		lblToDash2.setVisible(true);
		
		
		txtToDtYear=new Text(this, SWT.BORDER);
		txtToDtYear.setMessage("yyyy");
		txtToDtYear.setText(strToYear.substring(6));	
		txtToDtYear.setTextLimit(4);
		layout = new FormData();
		layout.top=new FormAttachment(lblFromDt, 14);
		layout.left = new FormAttachment(lblToDash2, 3);
		txtToDtYear.setLayoutData(layout);
		txtToDtYear.setVisible(true);

		
		btnView= new Button(this, SWT.PUSH);
		btnView.setText(" &View ");
		btnView.setFont(new Font(display, "Times New Roman", 12,SWT.BOLD));
		btnView.setEnabled(true);
		layout = new FormData();
		layout.top=new FormAttachment(txtToDtYear, 30);
		layout.left = new FormAttachment(42);
		layout.right = new FormAttachment(47);
		
		btnView.setLayoutData(layout);
		selectbar=new ProgressBar(this, SWT.SMOOTH);
		selectbar.setVisible(false);
		layout = new FormData();
		layout.top = new FormAttachment(btnView,40);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(55);
		selectbar.setLayoutData(layout);
	
		this.getAccessible();
		this.pack();
		
		this.setEvents();
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);

		globals.setThemeColor(this, Background, Foreground);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
	
		txtFromDtDay.setBackground(FocusBackground);
		txtFromDtDay.setForeground(FocusForeground);

		
	}
	 protected void showprogress() {

	    	selectbar.setVisible(true);
	    	selectbar.setMaximum(100000);
			for(int i=0;i<selectbar.getMaximum();i=i+20)
	        
	        {
				selectbar.setSelection(i);
	        }
	      }
	
	private void setEvents()
	{
		txtFromDtDay.setFocus();
		txtFromDtDay.selectAll();
		
		
		
		txtFromDtDay.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		
		txtFromDtMonth.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		
		
		txtFromDtYear.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});

		txtToDtDay.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});	
		
		
		txtToDtMonth.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		
		
		txtToDtYear.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		
		txtFromDtDay.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				
				if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
				{
					
					//txtDtDOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
					txtFromDtMonth.setFocus();
				}
				

			}
		});
		
		txtFromDtDay.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
				{
				if(!txtFromDtDay.getText().equals("") && Integer.valueOf ( txtFromDtDay.getText())<10 && txtFromDtDay.getText().length()< txtFromDtDay.getTextLimit())
				{
					txtFromDtDay.setText("0"+ txtFromDtDay.getText());
					//txtFromDtMonth.setFocus();
					txtFromDtDay.setFocus();
					return;
						}
				
				}
				if(arg0.keyCode ==SWT.TAB)
				{
					if(txtFromDtDay.getText().equals(""))
					{
						txtFromDtDay.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtFromDtDay.setFocus();
							}
						});
						return;
					}
				}
				

			}
		});
		
		
		txtFromDtMonth.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
				{
				if(!	txtFromDtMonth.getText().equals("") && Integer.valueOf ( 	txtFromDtMonth.getText())<10 && 	txtFromDtMonth.getText().length()< 	txtFromDtMonth.getTextLimit())
				{
					txtFromDtMonth.setText("0"+ txtFromDtMonth.getText());
					//txtFromDtMonth.setFocus();
					
					txtFromDtYear.setFocus();
					return;
					
					
					
				}
				else
				{
					txtFromDtYear.setFocus();
				}
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtFromDtDay.setFocus();
				}
				

			}
		});

		txtFromDtYear.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
		
			if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR||arg0.keyCode==SWT.TAB)
			{
				
					txtToDtDay.setFocus();
				
			}
				
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtFromDtMonth.setFocus();
			}
		

		}
	});
	

	
	
	txtToDtDay.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			
			if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
			{
			if(!	txtToDtDay.getText().equals("") && Integer.valueOf ( txtToDtDay.getText())<10 && 	txtToDtDay.getText().length()< 	txtToDtDay.getTextLimit())
			{
				txtToDtDay.setText("0"+ txtToDtDay.getText());
				//txtFromDtMonth.setFocus();
				txtToDtMonth.setFocus();
				return;
				
				
				
			}
			else
			{
				txtToDtMonth.setFocus();
				
			}
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtFromDtYear.setFocus();
			}
			
			
		}
	});
		
		txtToDtMonth.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				
				if(arg0.keyCode ==SWT.CR||arg0.keyCode == SWT.KEYPAD_CR)
				{
				if(!	txtToDtMonth.getText().equals("") && Integer.valueOf ( txtToDtMonth.getText())<10 && 	txtToDtMonth.getText().length()< 	txtToDtMonth.getTextLimit())
				{
					txtToDtMonth.setText("0"+ txtToDtMonth.getText());
					//txtFromDtMonth.setFocus();
					txtToDtYear.setFocus();
					return;
					
					
					
				}
				else
				{
					txtToDtYear.setFocus();
				}
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtToDtDay.setFocus();
				}

			}
		});
		
		txtToDtYear.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				
				if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR|arg0.keyCode==SWT.TAB)
				{
					if(txtToDtYear.getText().trim().equals(""))
					{
						
						txtToDtYear.setFocus();
						return;
					}
					if(!txtToDtYear.getText().trim().equals(""))
					{
						//btnView.setFocus();
						btnView.notifyListeners(SWT.Selection, new Event());
					}
					/*if(txtToDtYear.getText().trim().equals(""))
					{
						
						
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtToDtYear.setFocus();							
							}
						});
						return;
						
					}
					
				*/	
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtToDtMonth.selectAll();
					txtToDtMonth.setFocus();
				}
			

			}
		});
		
		btnView.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_UP)
				{
						txtToDtYear.setFocus();
				}
			}
		});
		


		
						btnView.addSelectionListener(new SelectionAdapter() {
						@Override
						public void widgetSelected(SelectionEvent arg0) {
							if(txtFromDtDay.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Date.");
								msgDayErr.open();
								txtFromDtDay.setFocus();
								
								return;
							}
							
							if(txtFromDtMonth.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Month.");
								msgDayErr.open();
								txtFromDtMonth.setFocus();
								
								return;
							}
							if(txtFromDtYear.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Year.");
								msgDayErr.open();
								txtFromDtYear.setFocus();
								
								return;
							}
							if(!txtFromDtDay.getText().equals("") && (Integer.valueOf(txtFromDtDay.getText())> 31 || Integer.valueOf(txtFromDtDay.getText()) <= 0) )
							{
								MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
								msgdateErr.setText("Validation Date Error!");
								msgdateErr.setMessage("You have entered an invalid date");
								msgdateErr.open();
								
								txtFromDtDay.setText("");
								Display.getCurrent().asyncExec(new Runnable() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										txtFromDtDay.setFocus();
										
									}
								});
								return;
							}
							if(!txtFromDtMonth.getText().equals("") && (Integer.valueOf(txtFromDtMonth.getText())> 12 || Integer.valueOf(txtFromDtMonth.getText()) <= 0))
							{
								MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
								msgdateErr.setText("Validation Month Error!");
								msgdateErr.setMessage("You have entered an invalid month");
								msgdateErr.open();
							
								
								Display.getCurrent().asyncExec(new Runnable() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										txtFromDtMonth.setText("");
										txtFromDtMonth.setFocus();
										
									}
								});
								return;
							}
							if(!txtFromDtYear.getText().trim().equals("") && (Integer.valueOf(txtFromDtYear.getText())> 2100 || Integer.valueOf(txtFromDtYear.getText()) < 1900) ) 
							{
						MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
						msgbox.setText("Validation Year Error!");
						msgbox.setMessage("You have entered an invalid year");
						msgbox.open();
					
						txtFromDtYear.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtFromDtYear.setFocus();
								txtFromDtYear.selectAll();
								
							}
						});
						return;
					}
							try {
								Date cashstarDate = sdf.parse(txtFromDtYear.getText()+ "-"+ txtFromDtMonth.getText()+"-"+ txtFromDtDay.getText());
								
								} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_ERROR);
								msg.setText("Error!");
								msg.setMessage("Invalid Date");
								txtFromDtDay.setFocus();
								msg.open();
								return;
							}
							if(!txtFromDtDay.getText().equals("") && Integer.valueOf ( txtFromDtDay.getText())<10 && txtFromDtDay.getText().length()< txtFromDtDay.getTextLimit())
							{
								txtFromDtDay.setText("0"+ txtFromDtDay.getText());
								//txtFromDtMonth.setFocus();
								
									}
							if(!txtFromDtMonth.getText().equals("") && Integer.valueOf ( txtFromDtMonth.getText())<10 && txtFromDtMonth.getText().length()< txtFromDtMonth.getTextLimit())
							{
								txtFromDtMonth.setText("0"+ txtFromDtMonth.getText());
								//txtFromDtMonth.setFocus();
								
									}
						
					
							
							if(txtToDtDay.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Date.");
								msgDayErr.open();
								txtToDtDay.setFocus();
								
								return;
							}
							if(txtToDtMonth.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Month.");
								msgDayErr.open();
								txtToDtMonth.setFocus();
								
								return;
							}
							if(txtToDtYear.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Year.");
								msgDayErr.open();
								txtToDtYear.setFocus();
								
								return;
							}
							if(!txtToDtDay.getText().equals("") && (Integer.valueOf(txtToDtDay.getText())> 31 || Integer.valueOf(txtToDtDay.getText()) <= 0) )
							{
								MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
								msgdateErr.setText("Validation Date Error!");
								msgdateErr.setMessage("You have entered an invalid date");
								msgdateErr.open();
								
								txtToDtDay.setText("");
								Display.getCurrent().asyncExec(new Runnable() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										txtToDtDay.setFocus();
										
									}
								});
								return;
							}
							if(!txtToDtMonth.getText().equals("") && (Integer.valueOf(txtToDtMonth.getText())> 12 || Integer.valueOf(txtToDtMonth.getText()) <= 0) )
							{
								MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
								msgdateErr.setText("Validation Month Error!");
								msgdateErr.setMessage("You have entered an invalid month");
								msgdateErr.open();
							
								txtToDtMonth.setFocus();
								Display.getCurrent().asyncExec(new Runnable() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										txtToDtMonth.setText("");
										txtToDtMonth.setFocus();
										
									}
								});
								return;
								
							}
							if(!txtToDtYear.getText().trim().equals("") && (Integer.valueOf(txtToDtYear.getText())> 2100 || Integer.valueOf(txtToDtYear.getText()) < 1900) )
							{
						MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR);
						msgbox.setText("Validation Year Error!");
						msgbox.setMessage("You have entered an invalid year");
						msgbox.open();
					
						txtToDtYear.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtToDtYear.setFocus();
								txtToDtYear.selectAll();
								
							}
						});
						return;
					}
					
					
					
					
							
							try {
								Date cashEnd = sdf.parse(txtToDtYear.getText()+ "-"+ txtToDtMonth.getText()+"-"+ txtToDtDay.getText() );
								
								} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_ERROR);
								msg.setText("Error!");
								msg.setMessage("Invalid Date");
								txtToDtDay.setFocus();
								msg.open();
								return;
							}
							
							
							
							/*if(txtFromDtDay.getText().trim().equals("")&&txtFromDtMonth.getText().trim().equals("")&&txtFromDtYear.getText().trim().equals("")&&txtToDtDay.getText().trim().equals("")&&txtToDtMonth.getText().trim().equals("")&&txtToDtYear.getText().trim().equals("")||txtFromDtDay.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Date.");
								msgDayErr.open();
								txtFromDtDay.setFocus();
								
								return;
							}*/
								
							
							if(!txtToDtDay.getText().equals("") && Integer.valueOf ( txtToDtDay.getText())<10 && txtToDtDay.getText().length()< txtToDtDay.getTextLimit())
							{
								txtToDtDay.setText("0"+ txtToDtDay.getText());
								//txtFromDtMonth.setFocus();
								
									}
							if(!txtToDtMonth.getText().equals("") && Integer.valueOf ( txtToDtMonth.getText())<10 && txtToDtMonth.getText().length()< txtToDtMonth.getTextLimit())
							{
								txtToDtMonth.setText("0"+ txtToDtMonth.getText());
								//txtFromDtMonth.setFocus();
								
									}
							
							
							
							
							
						/*	if(txtFromDtMonth.getText().trim().equals("")&&txtFromDtYear.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Date.");
								msgDayErr.open();
								txtFromDtMonth.setFocus();
								
								return;
							}*/
							
							
							
							/*if(txtFromDtYear.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Date.");
								msgDayErr.open();
								txtFromDtYear.setFocus();
								
								return;
							}
							
							if(txtFromDtMonth.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Date.");
								msgDayErr.open();
								txtFromDtMonth.setFocus();
								
								return;
							}*/
							
					
							
						/*	if(txtToDtDay.getText().trim().equals("")&&txtToDtMonth.getText().trim().equals("")&&txtToDtYear.getText().trim().equals("")||txtToDtDay.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Date.");
								msgDayErr.open();
								txtToDtDay.setFocus();
								
								return;
							}*/
							/*if(txtToDtDay.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Date.");
								msgDayErr.open();
								txtToDtDay.setFocus();
								
								return;
							}
							if(txtToDtMonth.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Date.");
								msgDayErr.open();
								txtToDtMonth.setFocus();
								
								return;
							}
							if(txtToDtYear.getText().trim().equals(""))
							{
								MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
								msgDayErr.setText("Validation Date Error!");
								msgDayErr.setMessage("Please enter a valid Date.");
								msgDayErr.open();
								txtToDtYear.setFocus();
								
								return;
							}*/
							
					
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							try {
								Date LedgerStart = sdf.parse(txtFromDtYear.getText()+ "-"+ txtFromDtMonth.getText()+"-"+ txtFromDtDay.getText() );
								Date LedgerEnd = sdf.parse(txtToDtYear.getText()+ "-"+ txtToDtMonth.getText()+"-"+ txtToDtDay.getText() );
								
								Date financialStart = sdf.parse(globals.session[2].toString().substring(6) +"-"+globals.session[2].toString().substring(3,5)+"-"+ globals.session[2].toString().substring(0,2));
								Date financialEnd = sdf.parse(globals.session[3].toString().substring(6) +"-"+globals.session[3].toString().substring(3,5)+"-"+ globals.session[3].toString().substring(0,2));
								
								if(LedgerStart.compareTo(financialStart)<0 || LedgerStart.compareTo(financialEnd) > 0 )
								{
								
									MessageBox msg = new MessageBox(new Shell(),SWT.ERROR|SWT.OK | SWT.ICON_ERROR);
									msg.setText("Validation Year Error!");
									msg.setMessage("Please enter the date range within the financial year");
									msg.open();
									txtFromDtYear.setText("");
									Display.getCurrent().asyncExec(new Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											txtFromDtYear.setFocus();
										}
									});
									
									return;
								}
								
								if( LedgerEnd .compareTo(financialStart)<0 ||  LedgerEnd .compareTo(financialEnd) > 0 )
								{
								
									MessageBox msg = new MessageBox(new Shell(),SWT.ERROR|SWT.OK | SWT.ICON_ERROR);
									msg.setText("Validation Year Error!");
									msg.setMessage("Please enter the date range within the financial year");
									msg.open();
									txtToDtYear.setText("");
									Display.getCurrent().asyncExec(new Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											txtToDtYear.setFocus();
										}
									});
									
									return;
								}
								
								if(LedgerStart.compareTo( LedgerEnd)>0)
								{
								
									MessageBox msg = new MessageBox(new Shell(),SWT.ERROR|SWT.OK | SWT.ICON_ERROR);
									msg.setText("Validation Year Error!");
									msg.setMessage("From Date is greater than To Date");
									msg.open();
									txtFromDtYear.setText("");
									Display.getCurrent().asyncExec(new Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											txtFromDtYear.setFocus();
										}
									});
									
									return;
								}
							
							
												
							
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							showprogress();
					Composite grandParent = (Composite) btnView.getParent().getParent();
					String financialFrom = globals.session[2].toString();
					String fromDate = txtFromDtYear.getText() + "-" + txtFromDtMonth.getText() + "-" + txtFromDtDay.getText();
					String toDate = txtToDtYear.getText() + "-" + txtToDtMonth.getText() + "-" + txtToDtDay.getText();
					
					
					
					
					btnView.getParent().dispose();
					//dispose();
					gnukhata.controllers.reportController.showCashFlow(grandParent , fromDate, toDate,financialFrom);
					//dispose();
					
					
				}
			});
						
						
						
							txtFromDtDay.addKeyListener(new KeyAdapter() {
								@Override
								public void keyPressed(
										KeyEvent arg0) {
									// TODO Auto-generated method stub
									//super.keyPressed(arg0);
									if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
											arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
											arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR
											||arg0.keyCode == SWT.ARROW_RIGHT||arg0.keyCode == SWT.ARROW_LEFT)
									{
										arg0.doit = true;
									}
									else
									{
										
										arg0.doit = false;
									}
								}
							});
							
							txtToDtDay.addKeyListener(new KeyAdapter() {
								@Override
								public void keyPressed(KeyEvent arg0) {
									// TODO Auto-generated method stub
									//super.keyPressed(arg0);
									if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
											arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
											arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9
											||arg0.keyCode == SWT.ARROW_RIGHT||arg0.keyCode == SWT.ARROW_LEFT)
									{
										arg0.doit = true;
										
									}
									else
									{
										
										arg0.doit = false;
									}
								}
							});
							
							txtToDtMonth.addKeyListener(new KeyAdapter() {
								@Override
								public void keyPressed(KeyEvent arg0) {
									// TODO Auto-generated method stub
									//super.keyPressed(arg0);
									if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
											arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
											arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR
											||arg0.keyCode == SWT.ARROW_RIGHT||arg0.keyCode == SWT.ARROW_LEFT)
									{
										arg0.doit = true;
									}
									else
									{
										
										arg0.doit = false;
									}
								}
							});
							
							
							txtToDtYear.addKeyListener(new KeyAdapter() {
								@Override
								public void keyPressed(KeyEvent arg0) {
									// TODO Auto-generated method stub
									//super.keyPressed(arg0);
									if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
											arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
											arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR
											||arg0.keyCode == SWT.ARROW_RIGHT||arg0.keyCode == SWT.ARROW_LEFT)
									{
										arg0.doit = true;
									}
									else
									{
										
										arg0.doit = false;
									}
								}
							});
							
							txtFromDtMonth.addKeyListener(new KeyAdapter() {
								@Override
								public void keyPressed(KeyEvent arg0) {
									// TODO Auto-generated method stub
									//super.keyPressed(arg0);
									if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
											arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
											arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR
											||arg0.keyCode == SWT.ARROW_RIGHT||arg0.keyCode == SWT.ARROW_LEFT)
									{
										arg0.doit = true;
									}
									else
									{
										
										arg0.doit = false;
									}
								}
							});
							
							txtFromDtYear.addKeyListener(new KeyAdapter() {
								@Override
								public void keyPressed(KeyEvent arg0) {
									// TODO Auto-generated method stub
									//super.keyPressed(arg0);
									if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
											arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
											arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR
											||arg0.keyCode == SWT.ARROW_RIGHT||arg0.keyCode == SWT.ARROW_LEFT)
									{
										arg0.doit = true;
									}
									else
									{
										
										arg0.doit = false;
									}
								}
							});
							
							txtFromDtMonth.addFocusListener(new FocusAdapter() {
								@Override
								
							
								public void focusLost(FocusEvent arg0) {
									
								
									
									/*if(!txtFromDtMonth.getText().equals("") && (Integer.valueOf(txtFromDtMonth.getText())> 12 || Integer.valueOf(txtFromDtMonth.getText()) <= 0))
									{
										MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
										msgdateErr.setText("Validation Month Error!");
										msgdateErr.setMessage("You have entered an Invalid Month, please enter it in mm format.");
										msgdateErr.open();
										
										
										Display.getCurrent().asyncExec(new Runnable() {
											
											@Override
											public void run() {
												// TODO Auto-generated method stub
												txtFromDtMonth.setText("");
												txtFromDtMonth.setFocus();
												
											}
										});
										return;
										
									}*/
									if(! txtFromDtMonth.getText().equals("") && Integer.valueOf ( txtFromDtMonth.getText())<10 && txtFromDtMonth.getText().length()< txtFromDtMonth.getTextLimit())
									{
										txtFromDtMonth.setText("0"+ txtFromDtMonth.getText());
										return;
									}
								
								}
							
							});
							
							txtToDtDay.addFocusListener(new FocusAdapter() {
								@Override
								public void focusLost(FocusEvent arg0) {
									// TODO Auto-generated method stub
									//super.focusLost(arg0);
									
									
									/*if(!txtToDtDay.getText().equals("") && Integer.valueOf ( txtToDtDay.getText())<10 && txtToDtDay.getText().length()< txtToDtDay.getTextLimit())
									{
										txtToDtDay.setText("0"+ txtToDtDay.getText());
									}
									
									if(!txtToDtDay.getText().equals("") && (Integer.valueOf(txtToDtDay.getText())> 31 || Integer.valueOf(txtToDtDay.getText()) <= 0) )
									{
										MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
										msgdateErr.setText("Validation Date Error!");
										msgdateErr.setMessage("You have entered an Invalid Date");
										msgdateErr.open();
										
										txtToDtDay.setText("");
										Display.getCurrent().asyncExec(new Runnable() {
											
											@Override
											public void run() {
												// TODO Auto-generated method stub
												txtToDtDay.setFocus();
												
											}
										});	
										return;
									}*/
									if(!txtToDtDay.getText().equals("") && Integer.valueOf ( txtToDtDay.getText())<10 && txtToDtDay.getText().length()< txtToDtDay.getTextLimit())
									{
										txtToDtDay.setText("0"+ txtToDtDay.getText());
										return;
									}
								}
								
							
							});
							
							txtToDtMonth.addFocusListener(new FocusAdapter() {
								@Override
								public void focusLost(FocusEvent arg0) {
									// TODO Auto-generated method stub
									//super.focusLost(arg0);
									/*if(!txtToDtMonth.getText().equals("") && Integer.valueOf ( txtToDtMonth.getText())<10 && txtToDtMonth.getText().length()< txtToDtMonth.getTextLimit())
									{
										 txtToDtMonth.setText("0"+  txtToDtMonth.getText());
									}
									if(!txtToDtMonth.getText().equals("") && (Integer.valueOf(txtToDtMonth.getText())> 12 || Integer.valueOf(txtToDtMonth.getText()) <= 0) )
									{
										MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
										msgdateErr.setText("Validation Month Error!");
										msgdateErr.setMessage("You have entered an Invalid Month");
										msgdateErr.open();
										
										txtToDtMonth.setText("");
										Display.getCurrent().asyncExec(new Runnable() {
											
											@Override
											public void run() {
												// TODO Auto-generated method stub
												txtToDtMonth.setFocus();
												
											}
										});
										return;
									}*/
									if(!txtToDtMonth.getText().equals("") && Integer.valueOf ( txtToDtMonth.getText())<10 && txtToDtMonth.getText().length()< txtToDtMonth.getTextLimit())
									{
										txtToDtMonth.setText("0"+ txtToDtMonth.getText());
										return;
									}
									
									
							}
							
							
							});
							
							
							txtFromDtDay.addFocusListener(new FocusAdapter() {
								@Override
								public void focusLost(FocusEvent arg0) {
									// TODO Auto-generated method stub
									//super.focusLost(arg0);
								/*	if(!txtFromDtDay.getText().equals("") && (Integer.valueOf(txtFromDtDay.getText())> 31 || Integer.valueOf(txtFromDtDay.getText()) <= 0) )
									{
										MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
										msgdateErr.setText("Validation Date Error!");
										msgdateErr.setMessage("You have entered an Invalid Date");
										msgdateErr.open();
										
										txtFromDtDay.setText("");
										Display.getCurrent().asyncExec(new Runnable() {
											
											@Override
											public void run() {
												// TODO Auto-generated method stub
												txtFromDtDay.setFocus();							
											}
										});
										return;
									}*/
									if(!txtFromDtDay.getText().equals("") && Integer.valueOf ( txtFromDtDay.getText())<10 && txtFromDtDay.getText().length()< txtFromDtDay.getTextLimit())
									{
										txtFromDtDay.setText("0"+ txtFromDtDay.getText());
										return;
									}
									
							}
							
							});
							
							
			

							txtFromDtYear.addFocusListener(new FocusAdapter() {

								@Override
								public void focusGained(FocusEvent arg0) {
									// TODO Auto-generated method stub
									super.focusGained(arg0);
									txtFromDtYear.clearSelection();
									txtFromDtYear.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
									txtFromDtYear.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
								
								}					
							});
							
								
/*							txtToDtYear.addFocusListener(new FocusAdapter() {
								@Override
								public void focusLost(FocusEvent arg0) {
									// TODO Auto-generated method stub
									//super.focusLost(arg0);
									
									SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
									try {
										Date voucherDate = sdf.parse(txtToDtYear.getText() + "-" + txtToDtMonth.getText() + "-" + txtToDtDay.getText());
										Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
										Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
										
										if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
										{
											MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK );
											errMsg.setMessage("please enter the date within the financial year");
											errMsg.open();
											txtToDtYear.setText("");
											Display.getCurrent().asyncExec(new Runnable() {
												
												@Override
												public void run() {
													// TODO Auto-generated method stub
													
													txtToDtYear.setFocus();
													
												}
											});
											
											return;
										}
									} 
									catch (java.text.ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
								}
								
							});
									

	*/						
	}
	
	/*public static void main(String[] args)
	{
		Display d = new Display();
		Shell s = new Shell(d);
		viewCashflow vcf = new viewCashflow(s, SWT.NONE);
		vcf.setSize(s.getClientArea().width, s.getClientArea().height);
		//viewCashflow vcf = new viewCashflow(s, SWT.NONE);
		//viewTrialBalance vtb = new viewTrialBalance(s, SWT.NONE);
		//vtb.setSize(s.getClientArea().width, s.getClientArea().height );
		

		s.pack();
		s.open();
		while (!s.isDisposed() ) {
			if (!d.readAndDispatch())
			{
				 d.sleep();
				 if(! s.getMaximized())
				 {
					 s.setMaximized(true);
				 }
			}
	}	} */
	
	public void makeaccessible(Control c) {
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}
	
	protected void checkSubclass() {
		// this is blank method so will disable the check that prevents
		// subclassing of shells.
	}


}
