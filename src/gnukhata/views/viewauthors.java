package gnukhata.views;

import gnukhata.globals;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class viewauthors extends Composite{

	static String strOrgName;
	static String strFromYear;
	static String strToYear;
	static Display display;
	
	public viewauthors(Composite parent, int style)
	{
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		MainShell.lblLogo.setVisible(false);
		 MainShell.lblLine.setVisible(false);
		 MainShell.lblOrgDetails.setVisible(false);
		    
		strToYear =  globals.session[3].toString();
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(67);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&")+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

			 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(90);
		//layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
		Label lblimg = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(5);
	//	layout.right = new FormAttachment(15);
		layout.bottom = new FormAttachment(32);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblimg.setLocation(getClientArea().width, getClientArea().height);
		lblimg.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblimg.setImage(globals.img);
		
		Text txtdata = new Text(this,SWT.WRAP|SWT.READ_ONLY);
		Color clr = Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
		txtdata.setBackground(clr);
		txtdata.setText("Domain Expert:\nCA Mr. Arun Kelkar\n<arunkelkar@rediffmail.com>");
		txtdata.setFont(new Font(display, "Times New Roman",13,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,4);
		layout.left = new FormAttachment(lblimg,5);
		//layout.right = new FormAttachment(25);
		//layout.bottom = new FormAttachment(30);
		txtdata.setLayoutData(layout);
		
		Label lblimg1 = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(txtdata,100);
		//layout.right = new FormAttachment(95);
		layout.bottom = new FormAttachment(32);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblimg1.setLocation(getClientArea().width, getClientArea().height);
		lblimg1.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblimg1.setImage(globals.img1);
		
		Text txtinfo = new Text(this,SWT.WRAP|SWT.READ_ONLY);
		Color clrb = Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
		txtinfo.setBackground(clrb);
		txtinfo.setText("Team Leader:\nKrishnakant Mane \n<kk@dff.org.in>");
		txtinfo.setFont(new Font(display, "Times New Roman",13,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,4);
		layout.left = new FormAttachment(lblimg1,5);
		layout.right = new FormAttachment(80);
		layout.bottom = new FormAttachment(30);
		txtinfo.setLayoutData(layout);
		
		Text lbldata = new Text(this,SWT.WRAP|SWT.READ_ONLY|SWT.V_SCROLL);
		Color clrblue = Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
		lbldata.setBackground(clrblue);
		lbldata.setText("ACTIVE CONTRIBUTORS:"
				+ "\n\n\tIshan Masdekar <imasdekar@gmail.com>"
				+ "\n\n\tNavin Karkera<navinkarkera@yahoo.com>"
				+ "\n\n\tSuyog Barkade"
				+ "\n\nFORMER CONTRIBUTORS:"
				+ "\n\nTo check our Former Contributors you can visit the following link"
				+ "\n\nwww.gnukhata.in"
				);
		lbldata.setFont(new Font(display, "Times New Roman",13,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(lblimg,30);
		layout.left = new FormAttachment(5);
		layout.right = new FormAttachment(90);
		layout.bottom = new FormAttachment(90);
		lbldata.setLayoutData(layout);
	}
	
}
