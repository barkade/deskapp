package gnukhata.views;

/*@ Authors
Amit Chougule <acamit333@gmail.com>,
Vinay khedekar < vinay.itengg@gmail.com>
*/

import gnukhata.globals;
import gnukhata.controllers.StartupController;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
//import com.sun.xml.internal.ws.transport.http.DeploymentDescriptorParser;

public class InitialSetupForm extends Shell  {
	
		static Display display;
		
		Color Background;
		Color Foreground;
		Color FocusBackground;
		Color FocusForeground;
		Color BtnFocusForeground;
		
		String strOrgName;//organisation name
		String strFromYear;//starting date of the financial year.
		String strToYear;//ending date of the financial year.
		String strOrgType;//organisation type "NGO" or "ProfitMaking"
		public static Label lblsavemsg;
		Label lblorginfo;
		Label lblRegiNo;
		Text txtRegiNo;
		Label lblDtOfRegiNo;
		Text txtDtDOrg;
		Label lblDtOfRegiNoDash1;
		Text txtDtMOrg;
		Label lblDtOfRegiNoDash2;
		Text txtDtYOrg;
		
		Label lblFcraRegiNo;
		Text txtFcraRegiNo;
		Label lblDtOfFcraRegiNo;
		Text txtDtDOfFcraRegiNo;
		Label lblDtOfFcraRegiNoDash1;
		Text txtDtMOfFcraRegiNo;
		Label lblDtOfFcraRegiNoDash2;
		Text txtDtYOfFcraRegiNo;
		
		Label lblAddress;
		Text txtAddress;
		Label lblCountry;
		CCombo dropdownCountry;
		long searchTexttimeout = 0;
		String searchText = "";
		
		Label lblState;
		CCombo dropdownState;
		Label lblCity;
		CCombo dropdownCity;
		Label lblPostalCode;
		Text txtPostalCode;
		Label lblVatNo;
		Text txtVatNo ;
		Label lblDtFormat;
		Label lblSkip;
		Label lblBack;
		Label lblSave;
		
		Label lblEmailId;
		Text txtEmailId ;
		Label lblTeliphoneNo;
		Text txtTeliphoneNo;  
		Label lblFaxNo;
		Text txtFaxNo;
		Label lblWebsite;
		Text txtWebsite;
		Label lblPAN ;
		Text txtPAN;
		Label lblServiceTaxNo ;
		Text txtServiceTaxNo;
		Button btnSkip;
		Button btnSave;
		Button btnBack;
		ProgressBar selectbar;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Vector<Object> params;
		
		public InitialSetupForm(String[] initialParams) {
			
			super(display);
			sdf.setLenient(false);
			this.setMaximized(true);
			strOrgName = initialParams[0];
			strOrgType = initialParams[3]; 
			strFromYear = initialParams[1];
			strToYear = initialParams[2];
			FormLayout formlayout = new FormLayout();
			this.setLayout(formlayout);
			this.setText("GNUKhata Startup");
			FormData layout = new FormData();
			
			Label lblHeadline = new Label(this,SWT.None);
			lblHeadline.setFont(new Font(display, "Times New Roman", 13, SWT.BOLD));
			lblHeadline.setText("GNUKhata: A Free and Open Source Accounting Software");
			layout = new FormData();
			layout.top = new FormAttachment(1);
			layout.left = new FormAttachment(3);
			layout.right = new FormAttachment(49);
			layout.bottom = new FormAttachment(5);
			lblHeadline.setLayoutData(layout);
			
			Label lblLogo = new Label(this, SWT.None);
			//Image img = new Image(display,"finallogo1.png");
			lblLogo.setImage(globals.logo);
			layout = new FormData();
			layout.top = new FormAttachment(1);
			layout.left = new FormAttachment(70);
			layout.right = new FormAttachment(100);
			layout.bottom = new FormAttachment(15);
			lblLogo.setLayoutData(layout);
						
			Label lblOrgDetails = new Label(this,SWT.NONE);
			lblOrgDetails.setFont( new Font(display,"Times New Roman",11,SWT.BOLD ) );
			lblOrgDetails.setText(strOrgName.replace("&", "&&")+ "\n"+ "For Financial Year "+"From "+strFromYear+" To "+strToYear);
			layout = new FormData();
			layout.top = new FormAttachment(6);
			layout.left = new FormAttachment(3);
			layout.right = new FormAttachment(69);
			layout.bottom = new FormAttachment(14);
			lblOrgDetails.setLayoutData(layout);
			
			Label lblLine = new Label(this,SWT.NONE);
			lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
			layout = new FormData();
			layout.top = new FormAttachment( lblOrgDetails , 1);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(99);
		//	layout.bottom = new FormAttachment(22);
			lblLine.setLayoutData(layout);
			
			lblorginfo = new Label(this, SWT.NONE);
			lblorginfo.setText(" Organisation Particulars");
			lblorginfo.setFont(new Font(display,"Times New Romen",12,SWT.BOLD));
			layout = new FormData();
			layout.top = new FormAttachment(lblLine, 1);
			layout.left = new FormAttachment(40);
			layout.right = new FormAttachment(70);
		//	layout.bottom = new FormAttachment(25);
			lblorginfo.setLayoutData(layout);
			
			lblRegiNo = new Label(this, SWT.NONE);
			lblRegiNo.setText("&Registration Number :");
			lblRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(24);
			layout.left = new FormAttachment(1);
			layout.right = new FormAttachment(20);
			layout.bottom = new FormAttachment(28);
			lblRegiNo.setLayoutData(layout);
			
			txtRegiNo = new Text(this, SWT.BORDER);
			txtRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(24);
			layout.left = new FormAttachment(26);
			layout.right = new FormAttachment(43);
			layout.bottom = new FormAttachment(28);
			txtRegiNo.setLayoutData(layout);
			
			lblDtOfRegiNo = new Label(this, SWT.NONE);
			lblDtOfRegiNo.setText("Date &Of Registration :");
			lblDtOfRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(24);
			layout.left = new FormAttachment(50);
			layout.right = new FormAttachment(73);
			layout.bottom = new FormAttachment(28);
			lblDtOfRegiNo.setLayoutData(layout);
			
			txtDtDOrg = new Text(this, SWT.BORDER);
			txtDtDOrg.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
			txtDtDOrg.setMessage("dd");
			txtDtDOrg.setTextLimit(2);
			layout = new FormData();
			layout.top = new FormAttachment(24);
			layout.left = new FormAttachment(79);
			//layout.right = new FormAttachment(82);
			layout.bottom = new FormAttachment(28);
			txtDtDOrg.setLayoutData(layout);
			
			lblDtOfRegiNoDash1 = new Label(this, SWT.NONE);
			lblDtOfRegiNoDash1.setText("-");
			lblDtOfRegiNoDash1.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
			layout = new FormData();
			layout.top = new FormAttachment(24);
			layout.left = new FormAttachment(txtDtDOrg,2);
			layout.bottom = new FormAttachment(28);
			lblDtOfRegiNoDash1.setLayoutData(layout);
			lblDtOfRegiNoDash1.setVisible(true);
			
			txtDtMOrg = new Text(this, SWT.BORDER);
			txtDtMOrg.setMessage("mm");
			txtDtMOrg.setTextLimit(2);
			txtDtMOrg.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(24);
			layout.left = new FormAttachment(lblDtOfRegiNoDash1);
			//layout.right = new FormAttachment(87);
			layout.bottom = new FormAttachment(28);
			txtDtMOrg.setLayoutData(layout);
			
			lblDtOfRegiNoDash2 = new Label(this, SWT.NONE);
			lblDtOfRegiNoDash2.setText("-");
			lblDtOfRegiNoDash2.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
			layout = new FormData();
			layout.top = new FormAttachment(24);
			layout.left = new FormAttachment(txtDtMOrg);
			layout.bottom = new FormAttachment(28);
			lblDtOfRegiNoDash2.setLayoutData(layout);
			lblDtOfRegiNoDash2.setVisible(true);
			
			txtDtYOrg = new Text(this, SWT.BORDER);
			txtDtYOrg.setMessage("yyyy");
			txtDtYOrg.setTextLimit(4);
			txtDtYOrg.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(24);
			layout.left = new FormAttachment(lblDtOfRegiNoDash2);
			//layout.right = new FormAttachment(92);
			layout.bottom = new FormAttachment(28);
			txtDtYOrg.setLayoutData(layout);
			
			
			lblFcraRegiNo = new Label(this, SWT.NONE);
			lblFcraRegiNo.setText("&FCRA Registration Number :");
			lblFcraRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(29);
			layout.left = new FormAttachment(1);
			layout.right = new FormAttachment(25);
			layout.bottom = new FormAttachment(33);
			lblFcraRegiNo.setLayoutData(layout);
			
			txtFcraRegiNo = new Text(this, SWT.BORDER);
			txtFcraRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(29);
			layout.left = new FormAttachment(26);
			layout.right = new FormAttachment(43);
			layout.bottom = new FormAttachment(33);
			txtFcraRegiNo.setLayoutData(layout);
			
			lblDtOfFcraRegiNo = new Label(this, SWT.NONE);
			lblDtOfFcraRegiNo.setText("Date Of FCRA Registratio&n :");
			lblDtOfFcraRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(29);
			layout.left = new FormAttachment(50);
			lblDtOfFcraRegiNo.setLayoutData(layout);
			
			txtDtDOfFcraRegiNo = new Text(this, SWT.BORDER);
			txtDtDOfFcraRegiNo.setMessage("dd");
			txtDtDOfFcraRegiNo.setTextLimit(2);
			txtDtDOfFcraRegiNo.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(29);
			layout.left = new FormAttachment(79);
			layout.bottom = new FormAttachment(33);
			txtDtDOfFcraRegiNo.setLayoutData(layout);
			
			lblDtOfFcraRegiNoDash1 = new Label(this, SWT.NONE);
			lblDtOfFcraRegiNoDash1.setText("-");
			lblDtOfFcraRegiNoDash1.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
			layout = new FormData();
			layout.top = new FormAttachment(29);
			layout.left = new FormAttachment(txtDtDOfFcraRegiNo);
			lblDtOfFcraRegiNoDash1.setLayoutData(layout);
			lblDtOfFcraRegiNoDash1.setVisible(true);
			
			txtDtMOfFcraRegiNo = new Text(this, SWT.BORDER);
			txtDtMOfFcraRegiNo.setMessage("mm");
			txtDtMOfFcraRegiNo.setTextLimit(2);
			txtDtMOfFcraRegiNo.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(29);
			layout.left = new FormAttachment(lblDtOfFcraRegiNoDash1);
			layout.bottom = new FormAttachment(33);
			txtDtMOfFcraRegiNo.setLayoutData(layout);
			
			lblDtOfFcraRegiNoDash2 = new Label(this, SWT.NONE);
			lblDtOfFcraRegiNoDash2.setText("-");
			lblDtOfFcraRegiNoDash2.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
			layout = new FormData();
			layout.top = new FormAttachment(29);
			layout.left = new FormAttachment(txtDtMOfFcraRegiNo);
			lblDtOfFcraRegiNoDash2.setLayoutData(layout);
			
			
			txtDtYOfFcraRegiNo = new Text(this, SWT.BORDER);
			txtDtYOfFcraRegiNo.setMessage("yyyy");
			txtDtYOfFcraRegiNo.setTextLimit(4);
			txtDtYOfFcraRegiNo.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(29);
			layout.left = new FormAttachment(lblDtOfFcraRegiNoDash2);
			//layout.right = new FormAttachment(95);
			layout.bottom = new FormAttachment(33);
			txtDtYOfFcraRegiNo.setLayoutData(layout);
			
			if (strOrgType.equals("Profit Making")) 
			{
				lblFcraRegiNo.setVisible(false);
				txtFcraRegiNo.setVisible(false);
				lblDtOfFcraRegiNo.setVisible(false);
				lblDtOfFcraRegiNoDash1.setVisible(false);
				txtDtDOfFcraRegiNo.setVisible(false);
				txtDtMOfFcraRegiNo.setVisible(false);
				lblDtOfFcraRegiNoDash2.setVisible(false);
				txtDtYOfFcraRegiNo.setVisible(false);
				lblRegiNo.setVisible(false);
				txtRegiNo.setVisible(false);
				lblDtOfRegiNo.setVisible(false);
				lblDtOfRegiNoDash1.setVisible(false);
				lblDtOfRegiNoDash2.setVisible(false);
				txtDtDOrg.setVisible(false);
				txtDtMOrg.setVisible(false);
				txtDtYOrg.setVisible(false);
			}
			
			
			lblAddress = new Label(this, SWT.NONE);
			lblAddress.setText("A&ddress  :");
			lblAddress.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(34);
			layout.left = new FormAttachment(1);
			layout.right = new FormAttachment(11);
			layout.bottom = new FormAttachment(38);
			lblAddress.setLayoutData(layout);
			
			txtAddress = new Text(this, SWT.BORDER);
			txtAddress.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		    layout = new FormData();
			layout.top = new FormAttachment(34);
			layout.left = new FormAttachment(26);
			layout.right = new FormAttachment(43);
			layout.bottom = new FormAttachment(38);
			txtAddress.setLayoutData(layout);
			
			lblCountry = new Label(this, SWT.NONE);
			lblCountry.setText("&Country :");
			lblCountry.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(39);
			layout.left = new FormAttachment(1);
			layout.right = new FormAttachment(11);
			layout.bottom = new FormAttachment(42);
			lblCountry.setLayoutData(layout);
			
			dropdownCountry = new CCombo(this,SWT.READ_ONLY | SWT.BORDER);
			dropdownCountry.setFont(new Font(display,"Times New Romen",9,SWT.NONE));
			layout = new FormData(); 
			layout.top = new FormAttachment(39);
			layout.left = new FormAttachment(26);
			layout.right = new FormAttachment(43);
			layout.bottom = new FormAttachment(42);
			
			dropdownCountry.add("------Please Select-----");
			dropdownCountry.select(0);
			if(dropdownCountry.getSelection().equals("please select"))
			{
				dropdownCountry.add("------Please Select-----");
			}
			else
			{
				dropdownCountry.add("INDIA");
			}
			
			
			dropdownCountry.setLayoutData(layout);
			lblState = new Label(this, SWT.NONE);
			lblState.setText("S&tate :");
			lblState.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(44);
			layout.left = new FormAttachment(1);
			layout.right = new FormAttachment(9);
			layout.bottom = new FormAttachment(47);
			lblState.setLayoutData(layout);
			
			dropdownState = new CCombo(this,SWT.READ_ONLY| SWT.BORDER);
			dropdownState.setFont(new Font(display,"Times New Romen",9,SWT.NONE));
			layout = new FormData(); 
			layout.top = new FormAttachment(44);
			layout.left = new FormAttachment(26);
			layout.right = new FormAttachment(43);
			layout.bottom = new FormAttachment(47);
			dropdownState.setLayoutData(layout);
			
			dropdownState.add("------Please Select-----");
			dropdownState.setEnabled(false);
			String[] states = StartupController.getStates();
			for(int i=0; i< states.length; i++)
			{
				dropdownState.add(states[i]);
			}
			dropdownState.select(0);
			/*dropdownState.setItems(states);
			dropdownState.add("------Please Select-----");
			*/
			lblCity = new Label(this, SWT.NONE);
			lblCity.setText("Cit&y :");
			lblCity.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(49);
			layout.left = new FormAttachment(1);
			layout.right = new FormAttachment(9);
			layout.bottom = new FormAttachment(52);
			lblCity.setLayoutData(layout);
			
			dropdownCity = new CCombo(this,SWT.READ_ONLY| SWT.BORDER);
			dropdownCity.setFont(new Font(display,"Times New Romen",9,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(49);
			layout.left = new FormAttachment(26);
			layout.right = new FormAttachment(43);
			layout.bottom = new FormAttachment(52);
			dropdownCity.setLayoutData(layout);
			dropdownCity.add("------Please Select-----");
			dropdownCity.select(0);
			dropdownCity.setEnabled(false);
			
			if(dropdownCountry.getSelectionIndex()==0)
			{
				//dropdownState.removeAll();
				dropdownState.setEnabled(false);
				dropdownCity.setEnabled(false);
			}
			else //if(dropdownCountry.getSelectionIndex()>0)
			{
				dropdownState.setEnabled(true);
			}
			
			if(dropdownState.getSelectionIndex()==0 )
			{
				
				dropdownCity.setEnabled(false);
			}
			else //if(dropdownCountry.getSelectionIndex()>0)
			{
				if (dropdownState.isEnabled()) {
					dropdownCity.setEnabled(true);
				}
				
			}
		
			
			
			lblPostalCode = new Label(this, SWT.NONE);
			lblPostalCode.setText("Posta&l Code :");
			lblPostalCode.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(54);
			layout.left = new FormAttachment(1);
			layout.right = new FormAttachment(13);
			layout.bottom = new FormAttachment(58);
			lblPostalCode.setLayoutData(layout);
			
			txtPostalCode = new Text(this, SWT.BORDER);
			txtPostalCode.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(54);
			layout.left = new FormAttachment(26);
			layout.right = new FormAttachment(43);
			layout.bottom = new FormAttachment(58);
		    txtPostalCode.setLayoutData(layout);
			
		    lblVatNo = new Label(this, SWT.NONE);
			lblVatNo.setText("&VAT No :");
			lblVatNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(59);
			layout.left = new FormAttachment(1);
			layout.right = new FormAttachment(12);
			layout.bottom = new FormAttachment(63);
			lblVatNo.setLayoutData(layout);
			
			txtVatNo = new Text(this, SWT.BORDER);
			txtVatNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(59);
			layout.left = new FormAttachment(26);
			layout.right = new FormAttachment(43);
			layout.bottom = new FormAttachment(63);
			txtVatNo.setLayoutData(layout);
			
			
		/*	lblDtFormat = new Label(this, SWT.NONE);
			lblDtFormat.setText("Date Will be In This Format(DD-MM-YYYY)");
			lblDtFormat.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(92);
			layout.left = new FormAttachment(15);
			layout.right = new FormAttachment(55);
			layout.bottom = new FormAttachment(95);
			lblDtFormat.setLayoutData(layout);*/
			
			
			lblEmailId = new Label(this, SWT.NONE);
			lblEmailId.setText("&Email-Id :");
			lblEmailId.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(34);
			layout.left = new FormAttachment(50);
			layout.right = new FormAttachment(62);
			layout.bottom = new FormAttachment(38);
			lblEmailId.setLayoutData(layout);
			
			txtEmailId = new Text(this, SWT.BORDER);
			txtEmailId.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(34);
			layout.left = new FormAttachment(79);
			layout.right = new FormAttachment(96);
			layout.bottom = new FormAttachment(38);
			txtEmailId.setLayoutData(layout);
			
			lblTeliphoneNo = new Label(this, SWT.NONE);
			lblTeliphoneNo.setText("Telephone N&umber :");
			lblTeliphoneNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(39);
			layout.left = new FormAttachment(50);
			layout.right = new FormAttachment(74);
			layout.bottom = new FormAttachment(43);
			lblTeliphoneNo.setLayoutData(layout);
			
			txtTeliphoneNo = new Text(this, SWT.BORDER);
			txtTeliphoneNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(39);
			layout.left = new FormAttachment(79);
			layout.right = new FormAttachment(96);
			layout.bottom = new FormAttachment(43);
			txtTeliphoneNo.setLayoutData(layout);
			
			lblFaxNo = new Label(this, SWT.NONE);
			lblFaxNo.setText("Fa&x Number :");
			lblFaxNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(44);
			layout.left = new FormAttachment(50);
			layout.right = new FormAttachment(65);
			layout.bottom = new FormAttachment(48);
			lblFaxNo.setLayoutData(layout);
			
			txtFaxNo = new Text(this, SWT.BORDER);
			txtFaxNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(44);
			layout.left = new FormAttachment(79);
			layout.right = new FormAttachment(96);
			layout.bottom = new FormAttachment(48);
			txtFaxNo.setLayoutData(layout);
			
			lblWebsite = new Label(this, SWT.NONE);
			lblWebsite.setText("&Website :");
			lblWebsite.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(49);
			layout.left = new FormAttachment(50);
			layout.right = new FormAttachment(64);
			layout.bottom = new FormAttachment(53);
			lblWebsite.setLayoutData(layout);
			
			txtWebsite = new Text(this, SWT.BORDER);
			txtWebsite.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(49);
			layout.left = new FormAttachment(79);
			layout.right = new FormAttachment(96);
			layout.bottom = new FormAttachment(53);
			txtWebsite.setLayoutData(layout);

			lblPAN = new Label(this, SWT.NONE);
			lblPAN.setText("&Permanent Account Number :");
			lblPAN.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(54);
			layout.left = new FormAttachment(50);
			layout.right = new FormAttachment(77);
			layout.bottom = new FormAttachment(58);
			lblPAN.setLayoutData(layout);
			
			txtPAN = new Text(this, SWT.BORDER);
			txtPAN.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(54);
			layout.left = new FormAttachment(79);
			layout.right = new FormAttachment(96);
			layout.bottom = new FormAttachment(58);
			txtPAN.setLayoutData(layout);
			
			lblServiceTaxNo = new Label(this, SWT.NONE);
			lblServiceTaxNo.setText("Serv&ice Tax Number :");
			lblServiceTaxNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(59);
			layout.left = new FormAttachment(50);
			layout.right = new FormAttachment(76);
			layout.bottom = new FormAttachment(63);
			lblServiceTaxNo.setLayoutData(layout);
			
			txtServiceTaxNo = new Text(this, SWT.BORDER);
			txtServiceTaxNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(59);
			layout.left = new FormAttachment(79);
			layout.right = new FormAttachment(96);
			layout.bottom = new FormAttachment(63);
			txtServiceTaxNo.setLayoutData(layout);
			
			/*lblsavemsg = new Label(this, SWT.NONE);
			lblsavemsg.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD|SWT.ITALIC));
			layout = new FormData();
			layout.top = new FormAttachment(70);
			layout.left = new FormAttachment(25);
			layout.right = new FormAttachment(90);
			//layout.bottom = new FormAttachment(txtMvatNo,20);
			lblsavemsg.setLayoutData(layout);
			lblsavemsg.setVisible(false);*/
			
			btnSave = new Button(this,SWT.PUSH);
			btnSave.setText("&Save");
			btnSave.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
			btnSave.setToolTipText("Click here to save all the details and create the database.");
			layout = new FormData();
			layout.top = new FormAttachment(85);
			layout.left = new FormAttachment(40);
			layout.right = new FormAttachment(55);
			//layout.bottom = new FormAttachment(90);
			btnSave.setLayoutData(layout);
			
			
			btnSkip = new Button(this,SWT.PUSH);
			btnSkip.setText("S&kip");
			btnSkip.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
			btnSkip.setToolTipText("Click here if you don't want to save details and just create the database");
			layout = new FormData();
			layout.top = new FormAttachment(85);
			layout.left = new FormAttachment(70);
			layout.right = new FormAttachment(85);
			//layout.bottom = new FormAttachment(90);
			btnSkip.setLayoutData(layout);
			
			
			btnBack = new Button(this,SWT.PUSH);
			btnBack.setText("&Back");
			btnBack.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
			layout = new FormData();
			layout.top = new FormAttachment(85);
			layout.left = new FormAttachment(10);
			layout.right = new FormAttachment(24);
			//layout.bottom = new FormAttachment(90);
			btnBack.setLayoutData(layout);
			
			selectbar=new ProgressBar(this, SWT.SMOOTH);
			selectbar.setVisible(false);
			layout = new FormData();
			layout.top = new FormAttachment(txtServiceTaxNo,40);
			layout.left = new FormAttachment(45);
			layout.right = new FormAttachment(65);
			selectbar.setLayoutData(layout);
			this.setBackgroundMode(SWT.INHERIT_FORCE);
			this.setBackgroundImage(globals.backImg);
			this.getAccessible();
			this.setEvents();
			this.pack();
			this.open();
			BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
			Background =  new Color(this.getDisplay() ,220 , 224, 227);
			Foreground = new Color(this.getDisplay() ,0, 0,0 );
			FocusBackground  = new Color(this.getDisplay(),78,97,114 );
			FocusForeground = new Color(this.getDisplay(),255,255,255);

			globals.setThemeColor(this, Background, Foreground);
			globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
			globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		    globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
	    	globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		        

			this.showView();
			
		}
		//int i=100;
	    protected void showprogress() {

	    	selectbar.setVisible(true);
	    	selectbar.setMaximum(100000);
			for(int i=0;i<selectbar.getMaximum();i=i+20)
	        
	        {
				selectbar.setSelection(i);
	        }
	      }

	    
	
		private void setEvents()
		{
			//the selection listenner is click event.
			//We are going to use adapters instead of listenners.
			//adapters are abstract classes so Eclipse allows us to override the methods.
			//System.out.println("inside set events");
			
			
			dropdownCountry.addFocusListener(new FocusAdapter() {
				public void focusGained(FocusEvent arg0) {
					dropdownCountry.setListVisible(true);
				};
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					dropdownCountry.setListVisible(false);	
				}
			});
			
			dropdownState.addFocusListener(new FocusAdapter() {
				public void focusGained(FocusEvent arg0) {
					dropdownState.setListVisible(true);
				};
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
				dropdownState.setListVisible(false);
				}
			});
			
			dropdownCity.addFocusListener(new FocusAdapter() {
				public void focusGained(FocusEvent arg0) {
					dropdownCity.setListVisible(true);
				};
				
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					dropdownCity.setListVisible(false);
				}
			});
			txtRegiNo.addFocusListener(new FocusAdapter() {
				public void focusGained(FocusEvent arg0) {
					if(dropdownCountry.getSelectionIndex()==0)
					{
						 //dropdownState.removeAll();
						dropdownState.setEnabled(false);
						dropdownCity.setEnabled(false);
					}
					else //if(dropdownCountry.getSelectionIndex()>0)
					{
						dropdownState.setEnabled(true);
					}
					
					if(dropdownState.getSelectionIndex()==0 )
					{
						
						dropdownCity.setEnabled(false);
					}
					else //if(dropdownCountry.getSelectionIndex()>0)
					{
						if (dropdownState.isEnabled()) {
							dropdownCity.setEnabled(true);
						}
						
					}
				};
			});
			dropdownCountry.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					//dropdownCity.removeAll();
					if(dropdownCountry.getSelectionIndex()==0)
					{
						dropdownState.removeAll();
						dropdownCity.removeAll();
						dropdownState.setEnabled(false);
						dropdownCity.setEnabled(false);
						//dropdownState.removeAll();
						//dropdownCity.removeAll();
						
					}
					else //if(dropdownCountry.getSelectionIndex()>0)
					{
						//dropdownState.add("----Please Select----",0);
						String[] states = StartupController.getStates();
					   // dropdownState.setItems(states);
						dropdownState.add("----Please Select----");
						for (int i = 0; i < states.length; i++ )
						{
							dropdownState.add(states[i]);
							
						}
						dropdownState.setEnabled(true);
						
						//dropdownState.setItems(states);
					}
					
					/*if(dropdownCountry.getSelectionIndex()> 0)
					{
						dropdownState.setEnabled(true);
					String[] states = StartupController.getStates();
					//dropdownState.setItems(states);
					dropdownState.add("----Please Select----");
					for (int i = 0; i < states.length; i++ )
					{
						dropdownState.add(states[i]);
						
					}
					 //dropdownState.add("please select,0");
					//dropdownState.select(0);
				
					//dropdownState.setItems(states);
					
				}
					else 
					{
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					//dropdownCity.removeAll();
						 if (dropdownCountry.getSelectionIndex()== 0) 
							{
							 //dropdownState.setItems(null);
							 dropdownState.add("");
							 dropdownCity.add(" ");
								//dropdownState.setEnabled(false);
							}
						 dropdownState.removeAll();
						 dropdownState.setEnabled(false);
						 dropdownCity.removeAll();
						 dropdownCity.setEnabled(false);
						
					
					}*/
				}});
			
			dropdownState.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					//dropdownCity.removeAll();
					if(dropdownState.getSelectionIndex()> 0)
					{
						dropdownCity.setEnabled(true);
					String[] cities = StartupController.getCities(dropdownState.getItem(dropdownState.getSelectionIndex()));
					dropdownCity.setItems(cities);
				}
					else 
					{
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					//dropdownCity.removeAll();
						 if (dropdownState.getSelectionIndex()== 0) 
							{
							 dropdownCity.add(" ");
								//dropdownState.setEnabled(false);
							}
						 dropdownCity.removeAll();
						dropdownCity.setEnabled(false);
					
					}
				}});	
			
		
		/*	txtRegiNo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					txtRegiNo.setBackground(FocusBackground);
					txtRegiNo.setForeground(FocusForeground);
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					
					txtRegiNo.setBackground(Background);
					txtRegiNo.setForeground(Foreground);
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
				}
			});
			txtFcraRegiNo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					txtFcraRegiNo.setBackground(FocusBackground);
					txtFcraRegiNo.setForeground(FocusForeground);
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					
					txtFcraRegiNo.setBackground(Background);
					txtFcraRegiNo.setForeground(Foreground);
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
				}
			});
			
			
			txtAddress.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					txtAddress.setBackground(FocusBackground);
					txtAddress.setForeground(FocusForeground);
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					
					txtAddress.setBackground(Background);
					txtAddress.setForeground(Foreground);
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
				}
			});
			dropdownCountry.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					dropdownCountry.setBackground(FocusBackground);
					dropdownCountry.setForeground(FocusForeground);
					
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
				 @Override
				public void focusLost(FocusEvent arg0) {

					     dropdownCountry.setBackground(Background);
						dropdownCountry.setForeground(Foreground);
					 // TODO Auto-generated method stub
				//	super.focusLost(arg0);
				}
			});
			dropdownState.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					dropdownState.setBackground(FocusBackground);
					dropdownState.setForeground(FocusForeground);
					
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
				@Override
				public void focusLost(FocusEvent arg0) {
				     dropdownState.setBackground(Background);
				     dropdownState.setForeground(Foreground);
					
					// TODO Auto-generated method stub
				//	super.focusLost(arg0);
				}
			});

              txtPostalCode.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					txtPostalCode.setBackground(FocusBackground);
					txtPostalCode.setForeground(FocusForeground);
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					
					txtPostalCode.setBackground(Background);
					txtPostalCode.setForeground(Foreground);
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
				}
			});
              txtMvatNo.addFocusListener(new FocusAdapter() {
  				@Override
  				public void focusGained(FocusEvent arg0) {
  					txtMvatNo.setBackground(FocusBackground);
  					txtMvatNo.setForeground(FocusForeground);
  					// TODO Auto-generated method stub
  				//	super.focusGained(arg0);
  				}
  				@Override
  				public void focusLost(FocusEvent arg0) {
  					
  					txtMvatNo.setBackground(Background);
  					txtMvatNo.setForeground(Foreground);
  					// TODO Auto-generated method stub
  					//super.focusLost(arg0);
  				}
  			});
              txtEmailId.addFocusListener(new FocusAdapter() {
  				@Override
  				public void focusGained(FocusEvent arg0) {
  					txtEmailId.setBackground(FocusBackground);
  					txtEmailId.setForeground(FocusForeground);
  					// TODO Auto-generated method stub
  				//	super.focusGained(arg0);
  				}
  				@Override
  				public void focusLost(FocusEvent arg0) {
  					
  					txtEmailId.setBackground(Background);
  					txtEmailId.setForeground(Foreground);
  					// TODO Auto-generated method stub
  					//super.focusLost(arg0);
  				}
  			});
              txtTeliphoneNo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					txtTeliphoneNo.setBackground(FocusBackground);
					txtTeliphoneNo.setForeground(FocusForeground);
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					
					txtTeliphoneNo.setBackground(Background);
					txtTeliphoneNo.setForeground(Foreground);
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
				}
			});
              txtFaxNo.addFocusListener(new FocusAdapter() {
  				@Override
  				public void focusGained(FocusEvent arg0) {
  					txtFaxNo.setBackground(FocusBackground);
  					txtFaxNo.setForeground(FocusForeground);
  					// TODO Auto-generated method stub
  				//	super.focusGained(arg0);
  				}
  				@Override
  				public void focusLost(FocusEvent arg0) {
  					
  					txtFaxNo.setBackground(Background);
  					txtFaxNo.setForeground(Foreground);
  					// TODO Auto-generated method stub
  					//super.focusLost(arg0);
  				}
  			});
  		
              txtWebsite.addFocusListener(new FocusAdapter() {
    				@Override
    				public void focusGained(FocusEvent arg0) {
    					txtWebsite.setBackground(FocusBackground);
    					txtWebsite.setForeground(FocusForeground);
    					// TODO Auto-generated method stub
    				//	super.focusGained(arg0);
    				}
    				@Override
    				public void focusLost(FocusEvent arg0) {
    					
    					txtWebsite.setBackground(Background);
    					txtWebsite.setForeground(Foreground);
    					// TODO Auto-generated method stub
    					//super.focusLost(arg0);
    				}
    			});
              txtPAN.addFocusListener(new FocusAdapter() {
  				@Override
  				public void focusGained(FocusEvent arg0) {
  					txtPAN.setBackground(FocusBackground);
  					txtPAN.setForeground(FocusForeground);
  					// TODO Auto-generated method stub
  				//	super.focusGained(arg0);
  				}
  				@Override
  				public void focusLost(FocusEvent arg0) {
  					
  					txtPAN.setBackground(Background);
  					txtPAN.setForeground(Foreground);
  					// TODO Auto-generated method stub
  					//super.focusLost(arg0);
  				}
  			});
            txtServiceTaxNo.addFocusListener(new FocusAdapter() {
    				@Override
    				public void focusGained(FocusEvent arg0) {
    					txtServiceTaxNo.setBackground(FocusBackground);
    					txtServiceTaxNo.setForeground(FocusForeground);
    					// TODO Auto-generated method stub
    				//	super.focusGained(arg0);
    				}
    				@Override
    				public void focusLost(FocusEvent arg0) {
    					
    					txtServiceTaxNo.setBackground(Background);
    					txtServiceTaxNo.setForeground(Foreground);
    					// TODO Auto-generated method stub
    					//super.focusLost(arg0);
    				}
    			});
              
            btnBack.addFocusListener(new FocusAdapter() {
  				@Override
  				public void focusGained(FocusEvent arg0) {
  					btnBack.setBackground(FocusBackground);
  					btnBack.setForeground(FocusForeground);
  					// TODO Auto-generated method stub
  				//	super.focusGained(arg0);
  				}
  				@Override
  				public void focusLost(FocusEvent arg0) {
  					
  					btnBack.setBackground(Background);
  					btnBack.setForeground(Foreground);
  					// TODO Auto-generated method stub
  					//super.focusLost(arg0);
  				}
  			});
            btnSave.addFocusListener(new FocusAdapter() {
  				@Override
  				public void focusGained(FocusEvent arg0) {
  					btnSave.setBackground(FocusBackground);
  					btnSave.setForeground(FocusForeground);
  					// TODO Auto-generated method stub
  				//	super.focusGained(arg0);
  				}
  				@Override
  				public void focusLost(FocusEvent arg0) {
  					
  					btnSave.setBackground(Background);
  					btnSave.setForeground(Foreground);
  					// TODO Auto-generated method stub
  					//super.focusLost(arg0);
  				}
  			});
            btnSkip.addFocusListener(new FocusAdapter() {
  				@Override
  				public void focusGained(FocusEvent arg0) {
  					btnSkip.setBackground(FocusBackground);
  					btnSkip.setForeground(FocusForeground);
  					// TODO Auto-generated method stub
  				//	super.focusGained(arg0);
  				}
  				@Override
  				public void focusLost(FocusEvent arg0) {
  					
  					btnSkip.setBackground(Background);
  					btnSkip.setForeground(Foreground);
  					// TODO Auto-generated method stub
  					//super.focusLost(arg0);
  				}
  			});
			
*/			try
			{
				
				
				/*dropdownState.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						
						if(dropdownState.getSelectionIndex()> 0)
						{
							dropdownCity.setEnabled(true);
							String[] cities = StartupController.getCities(dropdownState.getItem(dropdownState.getSelectionIndex()));
							dropdownCity.setItems(cities);
						}
						else
						{
						// TODO Auto-generated method stub
						//super.widgetSelected(arg0);
						//dropdownCity.removeAll();
							dropdownCity.setEnabled(true);
						
						}
					}
				});
				*/
				txtAddress.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent arg0) {
						
						// TODO Auto-generated method stub
					
					}
					@Override
					public void focusLost(FocusEvent arg0) {
						
						if(!txtAddress.getText().trim().equals(""))
						{
							String orgname1 = txtAddress.getText();
							String titlecasename = toTitleCase(orgname1);
							txtAddress.setText(titlecasename);
						}// TODO Auto-generated method stub
					
					//super.focusLost(arg0);
					
					}
					
				});
		btnSkip.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				// TODO Auto-generated method stub
				Vector<String > deployParams = new Vector<String>(4);
				deployParams.add(strOrgName);
				deployParams.add(strFromYear);
				deployParams.add(strToYear);
				deployParams.add(strOrgType);
				showprogress();
				gnukhata.controllers.StartupController.deploy(deployParams);
				btnSkip.getShell().getDisplay().dispose();
				gnukhata.controllers.StartupController.showLoginForm();
				
				
			}
			
		});
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				if((txtDtDOrg.getText().equals("") && txtDtMOrg.getText().equals("")&& txtDtYOrg.getText().equals("")) || (!(txtDtDOrg.getText().equals("")) && !(txtDtMOrg.getText().equals("")) && !(txtDtYOrg.getText().equals("")) && (txtDtDOfFcraRegiNo.getText().equals("") && txtDtMOfFcraRegiNo.getText().equals("")&& txtDtYOfFcraRegiNo.getText().equals("")) || (!(txtDtDOfFcraRegiNo.getText().equals("")) && !(txtDtMOfFcraRegiNo.getText().equals(""))&& !(txtDtYOfFcraRegiNo.getText().equals("")))))
				{
						
					if(!txtDtDOrg.getText().equals("") && (Integer.valueOf(txtDtDOrg.getText())> 31 || Integer.valueOf(txtDtDOrg.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid date");
						msgdateErr.open();
						
						txtDtDOrg.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOrg.setFocus();
								return;
							}
						});
						return;
					}
					
					if(!txtDtDOrg.getText().equals("") && Integer.valueOf ( txtDtDOrg.getText())<10 && txtDtDOrg.getText().length()< txtDtMOrg.getTextLimit())
					{	
						String dt = txtDtDOrg.getText();
						txtDtDOrg.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						
						//txtDtYOrg.setFocus();
						return;
					}	
					
					if(!txtDtMOrg.getText().equals("") && (Integer.valueOf(txtDtMOrg.getText())> 12 || Integer.valueOf(txtDtMOrg.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid Month");
						msgdateErr.open();
						
						txtDtMOrg.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOrg.setFocus();
								return;
							}
							
						});
						return;
					}
					
					if(!txtDtMOrg.getText().equals("") && Integer.valueOf ( txtDtMOrg.getText())<10 && txtDtMOrg.getText().length()< txtDtMOrg.getTextLimit())
					{	
						String dt = txtDtMOrg.getText();
						txtDtMOrg.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						
						//txtDtYOrg.setFocus();
						return;
					}	
				
					if(!txtDtYOrg.getText().trim().equals("")&& Integer.valueOf(txtDtYOrg.getText())<1990)
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Error!");
						msgDayErr.setMessage("You have entered an Invalid Year.");
					    msgDayErr.open();
						txtDtYOrg.setText("");
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOrg.setFocus();
								txtDtYOrg.selectAll();
								
							}
						});
						return;
					}

					
					if((!(txtDtDOrg.getText().equals("")) && !(txtDtMOrg.getText().equals("")) && !(txtDtYOrg.getText().equals(""))))
					{
				DateValidate dv = new DateValidate(Integer.valueOf(txtDtMOrg.getText()) ,Integer.valueOf(txtDtDOrg.getText()) ,Integer.valueOf(txtDtYOrg.getText()));
				String validationResult = dv.toString();
				if(validationResult.substring(2,3).equals("0"))
				{
					MessageBox msgErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgErr.setText("Validation Date Error!");
					msgErr.setMessage("You have entered invalid Date");
					msgErr.open();
				
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtDOrg.setFocus();
							txtDtDOrg.selectAll();
							
						}
				});
				return;
				}
				
					}
		
					
					if(!txtDtDOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtDOfFcraRegiNo.getText())> 31 || Integer.valueOf(txtDtDOfFcraRegiNo.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid date");
						msgdateErr.open();
						
						txtDtDOfFcraRegiNo.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOfFcraRegiNo.setFocus();
								return;
							}
						});
						return;
					}
			
					if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
					{	
						String dt = txtDtDOfFcraRegiNo.getText();
						txtDtDOfFcraRegiNo.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						
						//txtDtYOrg.setFocus();
						return;
					}	
					
					
					if(!txtDtMOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtMOfFcraRegiNo.getText())> 12 || Integer.valueOf(txtDtMOfFcraRegiNo.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid Month");
						msgdateErr.open();
						
						txtDtMOfFcraRegiNo.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOfFcraRegiNo.setFocus();
								return;
							}
							
						});
						return;
					}
					if(!txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtMOfFcraRegiNo.getTextLimit())
					{	
						String dt = txtDtMOfFcraRegiNo.getText();
						txtDtMOfFcraRegiNo.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						
						//txtDtYOrg.setFocus();
						return;
					}

					if(!txtDtYOfFcraRegiNo.getText().trim().equals("")&& Integer.valueOf(txtDtYOfFcraRegiNo.getText())<1990)
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Error!");
						msgDayErr.setMessage("You have entered an Invalid Year.");
					    msgDayErr.open();
						txtDtYOfFcraRegiNo.setText("");
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
								txtDtYOfFcraRegiNo.selectAll();
								
							}
						});
						return;
					}
					
					
				
					if((!(txtDtDOfFcraRegiNo.getText().equals("")) && !(txtDtMOfFcraRegiNo.getText().equals("")) && !(txtDtYOfFcraRegiNo.getText().equals(""))))
					{
				DateValidate dv = new DateValidate(Integer.valueOf(txtDtMOfFcraRegiNo.getText()) ,Integer.valueOf(txtDtDOfFcraRegiNo.getText()) ,Integer.valueOf(txtDtYOfFcraRegiNo.getText()));
				String validationResult = dv.toString();
				if(validationResult.substring(2,3).equals("0"))
				{
					MessageBox msgErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgErr.setText("Validation Date Error!");
					msgErr.setMessage("You have entered invalid Date");
					msgErr.open();
				
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtDOfFcraRegiNo.setFocus();
							txtDtDOfFcraRegiNo.selectAll();
							
						}
				});
				return;
				}
				
					}
			
		

					
					/*	try {
						Date fcradate = sdf.parse(txtDtDOrg.getText()+ "-" + txtDtMOrg.getText() + "-" + txtDtYOrg.getText());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_ERROR);
						msg.setText("Error!");
						msg.setMessage("Invalid Date");
						txtDtDOrg.setFocus();
						msg.open();
						return;
					}
					
					
					try {
						Date fcradate = sdf.parse(txtDtDOfFcraRegiNo.getText()+ "-" + txtDtMOfFcraRegiNo.getText() + "-" + txtDtYOfFcraRegiNo.getText());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_ERROR);
						msg.setText("Error!");
						msg.setMessage("Invalid Date");
						txtDtDOfFcraRegiNo.setFocus();
						msg.open();
						return;
					}
				*/	
					
					
					Vector<String > deployParams = new Vector<String>(4);
					deployParams.add(strOrgName);
					deployParams.add(strFromYear);
					deployParams.add(strToYear);
					deployParams.add(strOrgType);	
					Vector<String> orgParams = new Vector<String>();
					orgParams.add(strOrgType);
					orgParams.add(strOrgName);
					orgParams.add(txtAddress.getText());
					//orgParams.add(dropdownCity.getItem(dropdownCity.getSelectionIndex()));
					if(dropdownCity.getItemCount() > 0 && dropdownCity.getSelectionIndex() > 0)
					{
						orgParams.add(dropdownCity.getItem(dropdownCity.getSelectionIndex()));
					}
					else
					{
						orgParams.add("No city");
					}

					orgParams.add(txtPostalCode.getText() );
					//orgParams.add(dropdownState.getItem(dropdownState.getSelectionIndex()));
					if(dropdownState.getItemCount() > 0 && dropdownState.getSelectionIndex() > 0)
					{
						orgParams.add(dropdownState.getItem(dropdownState.getSelectionIndex()));
					}
					else
					{
						orgParams.add("No state");
					}
					
					//orgParams.add(dropdownCountry.getItem(dropdownCountry.getSelectionIndex()));
					if(dropdownCountry.getItemCount() > 0 && dropdownCountry.getSelectionIndex() > 0)
					{
						orgParams.add(dropdownCountry.getItem(dropdownCountry.getSelectionIndex()));
					}
					else
					{
						orgParams.add("no country");
					}
					
					orgParams.add(txtTeliphoneNo.getText());
					orgParams.add(txtFaxNo.getText());
					orgParams.add(txtWebsite.getText());
					orgParams.add(txtEmailId.getText());
					orgParams.add(txtPAN.getText());
					orgParams.add(txtVatNo.getText());
					orgParams.add(txtServiceTaxNo.getText());
					orgParams.add(txtRegiNo.getText());
					//orgParams.add(txtDtDOrg.getText() + "-" + txtDtMOrg.getText() + "-" + txtDtYOrg.getText());
					if(txtDtDOrg.getText().trim().equals("")&&txtDtMOrg.getText().trim().equals("")&&txtDtYOrg.getText().trim().equals(""))
					{
						orgParams.add(""+""+""+""+"");
					}
					else
					{
						orgParams.add(txtDtDOrg.getText() + "-" + txtDtMOrg.getText() + "-" + txtDtYOrg.getText());

					}
					System.out.println(txtDtDOrg.getText() + "-" + txtDtMOrg.getText() + "-" + txtDtYOrg.getText());	
					

					orgParams.add(txtFcraRegiNo.getText());
					if(txtDtDOfFcraRegiNo.getText().trim().equals("")&&txtDtMOfFcraRegiNo.getText().trim().equals("")&&txtDtYOfFcraRegiNo.getText().trim().equals(""))
					{
						orgParams.add(""+""+""+""+"");
					}
					else
					{
						orgParams.add(txtDtDOfFcraRegiNo.getText()+ "-" + txtDtMOfFcraRegiNo.getText() + "-" + txtDtYOfFcraRegiNo.getText());
					}	
					System.out.println("fcra"+txtDtDOfFcraRegiNo.getText()+ "-" + txtDtMOfFcraRegiNo.getText() + "-" + txtDtYOfFcraRegiNo.getText());
				
					//orgParams.add(txtDtDOfFcraRegiNo.getText()+ "-" + txtDtMOfFcraRegiNo.getText() + "-" + txtDtYOfFcraRegiNo.getText());
					orgParams.add("0");
					showprogress();
					//dispose();
					
					if(StartupController.deploy(deployParams, orgParams))
					{
						MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg.setText("Information!");
						msg.setMessage("Organisation added Successfully");
						msg.open();
						
						btnSave.getShell().getDisplay().dispose();
						gnukhata.controllers.StartupController.showLoginForm();
					}
					else
					{
						MessageBox msgfail = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgfail.open();
						
					}

				}
				else
				{
					if(txtDtDOrg.getText().trim().equals("") )
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a date in DD format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOrg.setFocus();
							return;	
							}
						});
						return;
					}
					
					
					if(txtDtMOrg.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Month in MM format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOrg.setFocus();
								return;
							}
						});
						return;
					}
					
				
					if(txtDtYOrg.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOrg.setFocus();
								return;
							}
						});
						return;
					}
					

					if(txtDtDOfFcraRegiNo.getText().trim().equals("") )
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a date in DD format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOfFcraRegiNo.setFocus();
								return;
							}
						});
						return;
					}
					
					
					if(txtDtMOfFcraRegiNo.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Month in MM format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOfFcraRegiNo.setFocus();
								return;
							}
						});
					return;
					}
					
				
					if(txtDtYOfFcraRegiNo.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
								
							}
						});
						return;
					}
				
					
				}
				
				
				
						/*	try {
					Date orgdate = sdf.parse(txtDtDOrg.getText() + "-" + txtDtMOrg.getText() + "-" + txtDtYOrg.getText());
					
					} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_ERROR);
					msg.setText("Error!");
					msg.setMessage("Invalid Date");
					txtDtDOrg.setFocus();
					msg.open();
					return;
				}
				
				try {
					Date fcradate = sdf.parse(txtDtDOfFcraRegiNo.getText()+ "-" + txtDtMOfFcraRegiNo.getText() + "-" + txtDtYOfFcraRegiNo.getText());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_ERROR);
					msg.setText("Error!");
					msg.setMessage("Invalid Date");
					txtDtDOfFcraRegiNo.setFocus();
					msg.open();
					return;
				}
				
				if(txtAddress.getText().trim().equals(""))
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg.setText("Information!");
					msg.setMessage("Please enter address");
					msg.open();
					txtAddress.setFocus();
					return;
				}
				if(dropdownCountry.getSelectionIndex()<= 0 )
				
				{
					MessageBox msgvalidate = new MessageBox(new Shell(), SWT.OK | SWT.ICON_INFORMATION);
					msgvalidate.setText("Information!");
					msgvalidate.setMessage("Please select a valid country");
					msgvalidate.open();
					dropdownCountry.setFocus();
					return;
				}
				if(dropdownState.getSelectionIndex()<= 0 )
					
				{
					MessageBox msgvalidate = new MessageBox(new Shell(), SWT.OK | SWT.ICON_INFORMATION);
					msgvalidate.setText("Information!");
					msgvalidate.setMessage("Please select a valid state");
					msgvalidate.open();
					dropdownState.setFocus();
					return;
				}
				if(dropdownCity.getSelectionIndex()< 0 )
					
				{
					MessageBox msgvalidate = new MessageBox(new Shell(), SWT.OK | SWT.ICON_INFORMATION);
					msgvalidate.setText("Information!");
					msgvalidate.setMessage("Please select a valid city");
					msgvalidate.open();
					dropdownCity.setFocus();
					return;
				}
				if(txtPostalCode.getText().trim().equals(""))
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg.setText("Information!");
					msg.setMessage("Please enter postalcode");
					msg.open();
					txtPostalCode.setFocus();
					return;
				}
			*/	
				/*Vector<String > deployParams = new Vector<String>(4);
				deployParams.add(strOrgName);
				deployParams.add(strFromYear);
				deployParams.add(strToYear);
				deployParams.add(strOrgType);	
				Vector<String> orgParams = new Vector<String>();
				orgParams.add(strOrgType);
				orgParams.add(strOrgName);
				orgParams.add(txtAddress.getText());
				//orgParams.add(dropdownCity.getItem(dropdownCity.getSelectionIndex()));
				if(dropdownCity.getItemCount() > 0 && dropdownCity.getSelectionIndex() > 0)
				{
					orgParams.add(dropdownCity.getItem(dropdownCity.getSelectionIndex()));
				}
				else
				{
					orgParams.add("No city");
				}

				orgParams.add(txtPostalCode.getText() );
				//orgParams.add(dropdownState.getItem(dropdownState.getSelectionIndex()));
				if(dropdownState.getItemCount() > 0 && dropdownState.getSelectionIndex() > 0)
				{
					orgParams.add(dropdownState.getItem(dropdownState.getSelectionIndex()));
				}
				else
				{
					orgParams.add("No state");
				}
				
				//orgParams.add(dropdownCountry.getItem(dropdownCountry.getSelectionIndex()));
				if(dropdownCountry.getItemCount() > 0 && dropdownCountry.getSelectionIndex() > 0)
				{
					orgParams.add(dropdownCountry.getItem(dropdownCountry.getSelectionIndex()));
				}
				else
				{
					orgParams.add("no country");
				}
				
				orgParams.add(txtTeliphoneNo.getText());
				orgParams.add(txtFaxNo.getText());
				orgParams.add(txtWebsite.getText());
				orgParams.add(txtEmailId.getText());
				orgParams.add(txtPAN.getText());
				orgParams.add(txtVatNo.getText());
				orgParams.add(txtServiceTaxNo.getText());
				orgParams.add(txtRegiNo.getText());
				orgParams.add(txtDtDOrg.getText() + "-" + txtDtMOrg.getText() + "-" + txtDtYOrg.getText());
				orgParams.add(txtFcraRegiNo.getText());
				orgParams.add(txtDtDOfFcraRegiNo.getText()+ "-" + txtDtMOfFcraRegiNo.getText() + "-" + txtDtYOfFcraRegiNo.getText());
				orgParams.add("0");
				showprogress();
				//dispose();
				
				if(StartupController.deploy(deployParams, orgParams))
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg.setText("Information!");
					msg.setMessage("Organisation added Successfully");
					msg.open();
					
					btnSave.getShell().getDisplay().dispose();
					gnukhata.controllers.StartupController.showLoginForm();
				}
				else
				{
					MessageBox msgfail = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					msgfail.open();
					
				}
*/				
								//.views.PreferencesForm preferences = new PreferencesForm();
			}
		});
	
		btnBack.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				btnBack.getParent().dispose();
				startupForm sf = new startupForm();
			}
		});
		
		
		
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			txtRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
					{
						txtDtDOrg.setFocus();
						arg0.doit = false;
						return;
					}

				}
			});
			
			txtFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtDtDOfFcraRegiNo.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtDtYOrg.setFocus();
					}

				}
			});
			
			txtAddress.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.TRAVERSE_TAB_NEXT || arg0.keyCode==SWT.KEYPAD_CR)
					{
						dropdownCountry.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtDtYOfFcraRegiNo.setFocus();
					}

				}
			});
			
			dropdownCountry.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR && dropdownCountry.getSelectionIndex()==0)
					{
						txtPostalCode.setFocus();
					}
				
					if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.TRAVERSE_TAB_NEXT || arg0.keyCode==SWT.KEYPAD_CR)
					{
		
						dropdownState.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP )
					{
						txtAddress.setFocus();
					}

				}
			});
			
			dropdownState.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR && dropdownState.getSelectionIndex()==0)
					{
						txtPostalCode.setFocus();
					}
					if(arg0.keyCode==SWT.CR)
					{
						dropdownCity.setFocus();
						dropdownCity.getItems();
					}
					if(arg0.keyCode==SWT.ARROW_UP && dropdownState.getSelectionIndex()==0)
					{
						dropdownCountry.setFocus();
					}
			
					/*if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR || arg0.keyCode==SWT.TRAVERSE_TAB_NEXT)
					{
						dropdownCity.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP && dropdownState.getSelectionIndex()==0 )
					{
						dropdownCountry.setFocus();
					}
			*/	}
			});
			
			/*dropdownCity.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode == SWT.KEYPAD_CR || arg0.keyCode == SWT.CR)
					{
						txtPostalCode.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						dropdownState.setFocus();
					}
				}
			});*/
			

			dropdownCity.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.KEYPAD_CR || arg0.keyCode==SWT.TRAVERSE_TAB_NEXT)
					{
						//txtDtYOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
						txtPostalCode.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						dropdownState.setFocus();
					}
				}
			});
			
			txtPostalCode.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
							arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
					}
					else
					{						
						arg0.doit = false;
					}
					
					if(arg0.keyCode==SWT.CR)
					{
						
						txtVatNo.setFocus();
					}			
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						dropdownCity.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP && dropdownState.getSelectionIndex()==0)
					{
						dropdownState.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP && dropdownCountry.getSelectionIndex()==0)
					{
						dropdownCountry.setFocus();
					}
				
				/*	if(arg0.keyCode==SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
					{
						
						txtVatNo.setFocus();
					}			
					
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						dropdownCity.setFocus();
					}
				*/
					
				}
				
			});
			
			
			/*txtPostalCode.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
					{
						
						txtMvatNo.setFocus();
					}			
					else
					{						
						arg0.doit = false;
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						dropdownCity.setFocus();
					}
				}
			});*/
			
			txtVatNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtEmailId.setFocus();
						arg0.doit = false;
						return;
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtPostalCode.setFocus();
					}

				}
			});
			
			txtDtDOrg.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					if(arg0.keyCode==SWT.CR || arg0.keyCode== SWT.KEYPAD_CR)
					{
						if(!txtDtDOrg.getText().equals("") && Integer.valueOf ( txtDtDOrg.getText())<10 && txtDtDOrg.getText().length()< txtDtDOrg.getTextLimit())
						{
							txtDtDOrg.setText("0"+ txtDtDOrg.getText());
							txtDtMOrg.setFocus();
							arg0.doit = false;
							//txtFromDtMonth.setFocus();
							return;
						}
						else
						{
							txtDtMOrg.setFocus();
						}
					
						
					}
					/*if(txtDtDOrg.getText().length()==txtDtDOrg.getTextLimit())
					{
						//txtDtDOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
						txtDtMOrg.setFocus();
					}
					*/if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtRegiNo.setFocus();
					}
					
					if(arg0.keyCode == SWT.TAB)
					{
						if(txtDtDOrg.getText().trim().equals(""))
						{
							txtDtDOrg.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOrg.setFocus();
								}
							});
							return;
						}
					}

				}
			});
			
			txtDtMOrg.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					
					if(arg0.keyCode==SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
					{
						
						if(!txtDtMOrg.getText().equals("") && Integer.valueOf ( txtDtMOrg.getText())<10 && txtDtMOrg.getText().length()< txtDtMOrg.getTextLimit())
						{	
							String dt = txtDtMOrg.getText();
							txtDtMOrg.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							txtDtYOrg.setFocus();
							return;
						}	
						else
						{
							txtDtYOrg.setFocus();
							
						}
			
					}
					/*if(txtDtMOrg.getText().length()==txtDtMOrg.getTextLimit())
					{
						//txtDtMOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
						txtDtYOrg.setFocus();
					}
					*/if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtDtDOrg.setFocus();
					}
					
					if(arg0.keyCode == SWT.TAB)
					{
						if(txtDtMOrg.getText().trim().equals(""))
						{
							txtDtMOrg.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOrg.setFocus();
								}
							});
							return;
						}
					}
				}
			});
			
			txtDtYOrg.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					
					if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
					{
						if(txtDtDOrg.getText().equals("")&& txtDtMOrg.getText().equals("")&& txtDtYOrg.getText().equals(""))
						{
							txtFcraRegiNo.setFocus();
							return;
						}
					if( ( !txtDtDOrg.getText().equals("")&& !txtDtMOrg.getText().equals("")&& !txtDtYOrg.getText().equals("")))
					{
						if(!txtDtDOrg.getText().equals("") && (Integer.valueOf(txtDtDOrg.getText())> 31 || Integer.valueOf(txtDtDOrg.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid date");
							msgdateErr.open();
							
							txtDtDOrg.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOrg.setFocus();
									return;
								}
							});
							return;
						}
				
						if(!txtDtDOrg.getText().equals("") && Integer.valueOf ( txtDtDOrg.getText())<10 && txtDtDOrg.getText().length()< txtDtDOrg.getTextLimit())
							
						{	
							String dt = txtDtDOrg.getText();
							txtDtDOrg.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}	
						
						
						if(!txtDtMOrg.getText().equals("") && (Integer.valueOf(txtDtMOrg.getText())> 12 || Integer.valueOf(txtDtMOrg.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid Month");
							msgdateErr.open();
							
							txtDtMOrg.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOrg.setFocus();
									return;
								}
								
							});
							return;
						}
						if(!txtDtMOrg.getText().equals("") && Integer.valueOf ( txtDtMOrg.getText())<10 && txtDtMOrg.getText().length()< txtDtMOrg.getTextLimit())
						{	
							String dt = txtDtMOrg.getText();
							txtDtMOrg.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}

						
						if(!txtDtYOrg.getText().trim().equals("")&& Integer.valueOf(txtDtYOrg.getText())<1990)
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
						    msgDayErr.open();
							txtDtYOrg.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOrg.setFocus();
									txtDtYOrg.selectAll();
									
								}
							});
							return;
						}
						else
						{
							txtFcraRegiNo.setFocus();
						}

					/*	if(!txtDtYOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtYOfFcraRegiNo.getText())<10 && txtDtYOfFcraRegiNo.getText().length()< txtDtYOfFcraRegiNo.getTextLimit())
						{
							
								
								

							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
							//msgDayErr.open();
							//txtDtYOfFcraRegiNo.setText("");
							txtAddress.setFocus();
								//txtFromDtMonth.setFocus();
							//	return;
								
				
							return;
						}
						
						
						else
						{
							txtAddress.setFocus();
						}
*/						//	return;
						
					//	txtAddress.setFocus();
						
						
						/*if(!txtDtYOfFcraRegiNo.getText().trim().equals("")&& Integer.valueOf(txtDtYOfFcraRegiNo.getText())<1990)
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
						    msgDayErr.open();
							txtDtYOfFcraRegiNo.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									txtDtYOfFcraRegiNo.selectAll();
									
								}
							});
							return;
						}
						
					*/	
					
						if((!(txtDtDOrg.getText().equals("")) && !(txtDtMOrg.getText().equals("")) && !(txtDtYOrg.getText().equals(""))))
						{
					DateValidate dv = new DateValidate(Integer.valueOf(txtDtMOrg.getText()) ,Integer.valueOf(txtDtDOrg.getText()) ,Integer.valueOf(txtDtYOrg.getText()));
					String validationResult = dv.toString();
					if(validationResult.substring(2,3).equals("0"))
					{
						MessageBox msgErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgErr.setText("Validation Date Error!");
						msgErr.setMessage("You have entered invalid Date");
						msgErr.open();
					
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOrg.setFocus();
								txtDtDOrg.selectAll();
								
							}
					});
					return;
					}
					
						}

						//txtAddress.setFocus();
		
					}
					else
					{
						if(txtDtDOrg.getText().trim().equals("") )
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a date in DD format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOrg.setFocus();
								return;	
								}
							});
							return;
						}
						
						
						if(txtDtMOrg.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Month in MM format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOrg.setFocus();
									return;
								}
							});
							return;
						}
						
					
						if(txtDtYOrg.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOrg.setFocus();
									return;
								}
							});
							return;
						}

					}
					txtFcraRegiNo.setFocus();
					}
					/*if(txtDtYOrg.getText().length()==txtDtYOrg.getTextLimit())
					{
						//txtDtYOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
						txtFcraRegiNo.setFocus();
					}
					*/if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtDtMOrg.setFocus();
						return;
					}
					if(arg0.keyCode == SWT.TAB)
					{
						if(txtDtYOrg.getText().trim().equals(""))
						{
							txtDtYOrg.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtFcraRegiNo.setFocus();
								}
							});
							return;
						}
					}

				}
				
			
			});
			txtDtDOfFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					
					if(arg0.keyCode == SWT.CR|| arg0.keyCode == SWT.KEYPAD_CR)
					{
						if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
						{
							txtDtDOfFcraRegiNo.setText("0"+ txtDtDOfFcraRegiNo.getText());
							txtDtMOfFcraRegiNo.setFocus();
							//txtFromDtMonth.setFocus();
							return;
						}
						else
						{
							txtDtMOfFcraRegiNo.setFocus();
						}
					
						
						//txtDtMOfFcraRegiNo.setFocus();
					}
				/*	if(arg0.keyCode == SWT.CR||txtDtDOfFcraRegiNo.getText().length()==txtDtDOfFcraRegiNo.getTextLimit())
					{
						txtDtMOfFcraRegiNo.setFocus();
					}
				*/	if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtFcraRegiNo.setFocus();
					}
						
				if(arg0.keyCode == SWT.TAB)
				{
					if(txtDtDOfFcraRegiNo.getText().trim().equals(""))
					{
						txtDtDOfFcraRegiNo.setText("");
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOfFcraRegiNo.setFocus();
							}
						});
						return;
					}
				}

					}
			});
			
			txtDtMOfFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
					{
						if(!txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtMOfFcraRegiNo.getTextLimit())
						{
							txtDtMOfFcraRegiNo.setText("0"+ txtDtMOfFcraRegiNo.getText());
							txtDtYOfFcraRegiNo.setFocus();
							//txtFromDtMonth.setFocus();
							return;
						}
						else
						{
							txtDtYOfFcraRegiNo.setFocus();
						}
					
						//txtDtYOfFcraRegiNo.setFocus();
					}
					/*if(txtDtMOfFcraRegiNo.getText().length()==txtDtMOfFcraRegiNo.getTextLimit())
					{
						txtDtYOfFcraRegiNo.setFocus();
					}
					*/if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtDtDOfFcraRegiNo.setFocus();
					}
					if(arg0.keyCode == SWT.TAB)
					{
						if(txtDtMOfFcraRegiNo.getText().trim().equals(""))
						{
							txtDtMOfFcraRegiNo.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOfFcraRegiNo.setFocus();
								}
							});
							return;
						}
					}

				}
			});
			
			txtDtYOfFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode == SWT.CR || arg0.keyCode== SWT.KEYPAD_CR)
					{
						if(txtDtDOfFcraRegiNo.getText().equals("")&& txtDtMOfFcraRegiNo.getText().equals("")&& txtDtYOfFcraRegiNo.getText().equals(""))
						{
							txtAddress.setFocus();
							return;
						}
												
					if( ( !txtDtDOfFcraRegiNo.getText().equals("")&& !txtDtMOfFcraRegiNo.getText().equals("")&& !txtDtYOfFcraRegiNo.getText().equals("")))
					{
						if(!txtDtDOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtDOfFcraRegiNo.getText())> 31 || Integer.valueOf(txtDtDOfFcraRegiNo.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid date");
							msgdateErr.open();
							
							txtDtDOfFcraRegiNo.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}
				
						if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
						{	
							String dt = txtDtDOfFcraRegiNo.getText();
							txtDtDOfFcraRegiNo.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}	
						
						
						if(!txtDtMOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtMOfFcraRegiNo.getText())> 12 || Integer.valueOf(txtDtMOfFcraRegiNo.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid Month");
							msgdateErr.open();
							
							txtDtMOfFcraRegiNo.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOfFcraRegiNo.setFocus();
									return;
								}
								
							});
							return;
						}
						if(!txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtMOfFcraRegiNo.getTextLimit())
						{	
							String dt = txtDtMOfFcraRegiNo.getText();
							txtDtMOfFcraRegiNo.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}

						
						if(!txtDtYOfFcraRegiNo.getText().trim().equals("")&& Integer.valueOf(txtDtYOfFcraRegiNo.getText())<1990)
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
						    msgDayErr.open();
							txtDtYOfFcraRegiNo.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									txtDtYOfFcraRegiNo.selectAll();
									
								}
							});
							return;
						}
						else
						{
							txtAddress.setFocus();
						}

					/*	if(!txtDtYOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtYOfFcraRegiNo.getText())<10 && txtDtYOfFcraRegiNo.getText().length()< txtDtYOfFcraRegiNo.getTextLimit())
						{
							
								
								

							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
							//msgDayErr.open();
							//txtDtYOfFcraRegiNo.setText("");
							txtAddress.setFocus();
								//txtFromDtMonth.setFocus();
							//	return;
								
				
							return;
						}
						
						
						else
						{
							txtAddress.setFocus();
						}
*/						//	return;
						
					//	txtAddress.setFocus();
						
						
						/*if(!txtDtYOfFcraRegiNo.getText().trim().equals("")&& Integer.valueOf(txtDtYOfFcraRegiNo.getText())<1990)
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
						    msgDayErr.open();
							txtDtYOfFcraRegiNo.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									txtDtYOfFcraRegiNo.selectAll();
									
								}
							});
							return;
						}
						
					*/	
					
						if((!(txtDtDOfFcraRegiNo.getText().equals("")) && !(txtDtMOfFcraRegiNo.getText().equals("")) && !(txtDtYOfFcraRegiNo.getText().equals(""))))
						{
					DateValidate dv = new DateValidate(Integer.valueOf(txtDtMOfFcraRegiNo.getText()) ,Integer.valueOf(txtDtDOfFcraRegiNo.getText()) ,Integer.valueOf(txtDtYOfFcraRegiNo.getText()));
					String validationResult = dv.toString();
					if(validationResult.substring(2,3).equals("0"))
					{
						MessageBox msgErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgErr.setText("Validation Date Error!");
						msgErr.setMessage("You have entered invalid Date");
						msgErr.open();
					
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOfFcraRegiNo.setFocus();
								txtDtDOfFcraRegiNo.selectAll();
								
							}
					});
					return;
					}
					
						}

						//txtAddress.setFocus();
		
					}
					else
					{
						if(txtDtDOfFcraRegiNo.getText().trim().equals("") )
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a date in DD format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOfFcraRegiNo.setFocus();
								return;	
								}
							});
							return;
						}
						
						
						if(txtDtMOfFcraRegiNo.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Month in MM format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}
						
					
						if(txtDtYOfFcraRegiNo.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}

					}
					txtAddress.setFocus();	
					}
					/*if(txtDtYOfFcraRegiNo.getText().length()==txtDtYOfFcraRegiNo.getTextLimit())
					{
						txtAddress.setFocus();
					}
					*/if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtDtMOfFcraRegiNo.setFocus();
					}
					if(arg0.keyCode == SWT.TAB)
					{
						if(txtDtYOfFcraRegiNo.getText().trim().equals(""))
						{
							txtDtYOfFcraRegiNo.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
								}
							});
							return;
						}
					}

				}
			});
			
			txtEmailId.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
					{
						txtTeliphoneNo.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtVatNo.setFocus();
					}
				}
			});
			 
			txtTeliphoneNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode == SWT.CR)
					{
						txtFaxNo.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtEmailId.setFocus();
					}
				}
			});
			
			txtFaxNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode == SWT.CR)
					{
						txtWebsite.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtTeliphoneNo.setFocus();
					}
				}
			});
			
			txtWebsite.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode == SWT.CR)
					{
						txtPAN.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtFaxNo.setFocus();
					}
				}
			});
			
			txtPAN.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode == SWT.CR)
					{
						txtServiceTaxNo.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtWebsite.setFocus();
					}
				}
			});
			
			txtServiceTaxNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode == SWT.CR | arg0.keyCode== SWT.TAB | arg0.keyCode == SWT.KEYPAD_CR )
					{
						btnSave.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtPAN.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_LEFT || arg0.keyCode == SWT.ARROW_RIGHT || arg0.keyCode == SWT.ARROW_DOWN)
					{
						arg0.doit = false;
						return;
					}
				}
			});
			
			btnSave.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode == SWT.ARROW_RIGHT)
					{
						btnSkip.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_LEFT)
					{
						btnBack.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						txtServiceTaxNo.setFocus();
					}
				}
				
			});

			btnSkip.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode == SWT.ARROW_LEFT)
					{
						btnSave.setFocus();
					}
					if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
					{
						btnSave.notifyListeners(SWT.Selection, new Event());
						
					}
					
				}
				
			});
			
			dropdownState.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					long now = System.currentTimeMillis();
					if (now > searchTexttimeout){
				         searchText = "";
				      }
					searchText += Character.toLowerCase(arg0.character);
					searchTexttimeout = now + 500;					
					for(int i = 0; i < dropdownState.getItemCount(); i++ )
					{
						if(dropdownState.getItem(i).toLowerCase().startsWith(searchText ) ){
							//arg0.doit= false;
							dropdownState.select(i);
							dropdownState.notifyListeners(SWT.Selection ,new Event()  );
							break;
						}
					}
				}
			});
			
			dropdownCity.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					long now = System.currentTimeMillis();
					if (now > searchTexttimeout){
				         searchText = "";
				      }
					searchText += Character.toLowerCase(arg0.character);
					searchTexttimeout = now + 500;					
					for(int i = 0; i < dropdownCity.getItemCount(); i++ )
					{
						if(dropdownCity.getItem(i).toLowerCase().startsWith(searchText ) ){
							//arg0.doit= false;
							dropdownCity.select(i);
							dropdownCity.notifyListeners(SWT.Selection ,new Event()  );
							break;
						}
					}
				}
			});
			
			dropdownCountry.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					long now = System.currentTimeMillis();
					if (now > searchTexttimeout){
				         searchText = "";
				      }
					searchText += Character.toLowerCase(arg0.character);
					searchTexttimeout = now + 500;					
					for(int i = 0; i < dropdownCountry.getItemCount(); i++ )
					{
						if(dropdownCountry.getItem(i).toLowerCase().startsWith(searchText ) ){
							//arg0.doit= false;
							dropdownCountry.select(i);
							dropdownCountry.notifyListeners(SWT.Selection ,new Event()  );
							break;
						}
					}
				}
			});
			

			btnBack.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
					{

						dispose();					
						startupForm st=new startupForm();
				
					}
					
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						btnSave.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_RIGHT )
					{
						
						btnSave.setFocus();
					}
					if( arg0.keyCode == SWT.ARROW_LEFT || arg0.keyCode == SWT.ARROW_DOWN)
					{
						arg0.doit = false;
					}
						}
			});			
			
			
			
			/*txtDtYOrg.addKeyListener(new KeyAdapter() {
				@Override 
				txtTeliphoneNo txtFaxNo txtWebsite txtPAN txtServiceTaxNo
				txtDtDOfFcraRegiNo;
		        txtDtMOfFcraRegiNo;
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtFcraRegiNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtDtMOrg.setFocus();
					}
				}
			});
			
			txtFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						//txtDtYOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
						txtDtDOfFcraRegiNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtDtYOrg.setFocus();
					}
				}
			});
			
			/*txtFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyRelesed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtDtDOfFcraRegiNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtDtYOrg.setFocus();
					}
				}
			});
			
			txtDtDOfFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(txtDtDOfFcraRegiNo.getText().length()==txtDtDOfFcraRegiNo.getTextLimit())
					{
						//txtDtDOfFcraRegiNo.traverse(SWT.TRAVERSE_TAB_NEXT);
						txtDtMOfFcraRegiNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtFcraRegiNo.setFocus();
					}
				}
			});
			
			txtDtMOfFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(txtDtMOfFcraRegiNo.getText().length()==txtDtMOfFcraRegiNo.getTextLimit())
					{
						//txtDtMOfFcraRegiNo.traverse(SWT.TRAVERSE_TAB_NEXT);
						txtDtYOfFcraRegiNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtDtDOfFcraRegiNo.setFocus();
					}
				}
			});
			
			txtDtYOfFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(txtDtYOfFcraRegiNo.getText().length()==txtDtYOfFcraRegiNo.getTextLimit())
					{
						//txtDtYOfFcraRegiNo.traverse(SWT.TRAVERSE_TAB_NEXT);
						txtAddress.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtDtMOfFcraRegiNo.setFocus();
					}
				}
			});
			
			txtDtYOfFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtAddress.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtDtMOfFcraRegiNo.setFocus();
					}
				}
			});
			
			
			
			txtAddress.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						dropdownCountry.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtDtYOfFcraRegiNo.setFocus();
					}
				}			
			});
			
			dropdownCountry.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						dropdownState.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP && dropdownCountry.getSelectionIndex()==0)
					{
						txtAddress.setFocus();
					}
				}
			});
			
			dropdownCountry.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						//txtDtYOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
						dropdownState.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtAddress.setFocus();
					}
				}
			});
			
			
			
			dropdownState.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						//txtDtYOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
						dropdownCity.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						dropdownCountry.setFocus();
					}
				}
			});
			
			
			dropdownCity.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						//txtDtYOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
						txtPostalCode.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						dropdownState.setFocus();
					}
				}
			});
			
			txtPostalCode.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtMvatNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						dropdownCity.setFocus();
					}
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			
			
			txtMvatNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtEmailId.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtPostalCode.setFocus();
					}
				}
			});
			
			txtMvatNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtEmailId.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtPostalCode.setFocus();
					}
				}
			});
			
			txtEmailId.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtTeliphoneNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtMvatNo.setFocus();
					}
				}
			});
			
			txtEmailId.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtTeliphoneNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtMvatNo.setFocus();
					}
				}
			});
			
			txtTeliphoneNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtFaxNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtEmailId.setFocus();
					}
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtTeliphoneNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtFaxNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtEmailId.setFocus();
					}
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtFaxNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtWebsite.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtTeliphoneNo.setFocus();
					}
				}
			});
			
			txtFaxNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtWebsite.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtTeliphoneNo.setFocus();
					}
				}
			});

			
			txtWebsite.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtPAN.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtFaxNo.setFocus();
					}
				}
			});
			
			txtWebsite.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtPAN.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtFaxNo.setFocus();
					}
				}
			});

			
			txtPAN.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtServiceTaxNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtWebsite.setFocus();
					}
				}
			});
			
			txtPAN.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						txtServiceTaxNo.setFocus();
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtWebsite.setFocus();
					}
				}
			});

			
			txtServiceTaxNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						btnBack.setFocus();
						btnBack.notifyListeners(SWT.Selection, new Event());
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtPAN.setFocus();
					}
				}
			});
			
			
			txtServiceTaxNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyReleased(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						btnBack.setFocus();
						btnBack.notifyListeners(SWT.Selection, new Event());
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtPAN.setFocus();
					}
				}
			});*/
			
			/*btnBack.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						//btnSave.setFocus();
						btnSave.notifyListeners(SWT.Selection, new Event());
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtServiceTaxNo.setFocus();
					}
				}
			});
			*/
		/*	btnSave.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR)
					{
						//btnSkip.setFocus();
						btnSkip.notifyListeners(SWT.Selection, new Event());
					}
					if(arg0.keyCode==SWT.ARROW_UP && arg0.keyCode == SWT.KEYPAD_CR)
					{
						//btnBack.setFocus();
						btnBack.notifyListeners(SWT.Selection, new Event());
					}
				}
			});
		*/	
			/*btnSkip.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						//btnSave.setFocus();
						btnSave.notifyListeners(SWT.Selection, new Event());
					}
				}
			});
			*/
			txtDtDOrg.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
							arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtDtMOrg.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
							arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtDtYOrg.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
							arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtDtDOfFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
							arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtDtMOfFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
							arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtDtYOfFcraRegiNo.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
							arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			
			
			txtDtDOrg.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					/*txtDtDOrg.setBackground(Background);
					txtDtDOrg.setForeground(Foreground);
					*/
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
				
					
			/*		if(!txtDtDOrg.getText().equals("") && (Integer.valueOf(txtDtDOrg.getText())> 31 || Integer.valueOf(txtDtDOrg.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid date");
						msgdateErr.open();
						
						txtDtDOrg.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOrg.setFocus();
							}
						});
						return;
					}
			*/		if(!txtDtDOrg.getText().equals("") && Integer.valueOf ( txtDtDOrg.getText())<10 && txtDtDOrg.getText().length()< txtDtDOrg.getTextLimit())
					{
						txtDtDOrg.setText("0"+ txtDtDOrg.getText());
					//	txtDtMOrg.setFocus();
						//txtFromDtMonth.setFocus();
						return;
					}
				}
				/*@Override
				public void focusGained(FocusEvent arg0) {
					txtDtDOrg.setBackground(FocusBackground);
					txtDtDOrg.setForeground(FocusForeground);
					
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}*/
			});
			
			
			
			txtDtMOrg.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					/*txtDtDOfFcraRegiNo.setBackground(Background);
					txtDtDOfFcraRegiNo.setForeground(Foreground);
				*/
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
										
					/*if(!txtDtMOrg.getText().equals("") && (Integer.valueOf(txtDtMOrg.getText())> 12 || Integer.valueOf(txtDtMOrg.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid Month");
						msgdateErr.open();
						
			
						Display.getCurrent().asyncExec(new Runnable() {
							
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOrg.setText("");
								txtDtMOrg.setFocus();
								//return;
								
							}
							
						});
						return;
					}
			*/		if(!txtDtMOrg.getText().equals("") && Integer.valueOf ( txtDtMOrg.getText())<10 && txtDtMOrg.getText().length()< txtDtDOrg.getTextLimit())
					{	
						String dt = txtDtMOrg.getText();
						txtDtMOrg.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						//txtDtYOrg.setFocus();
						return;
					}
				}
/*@Override
				public void focusGained(FocusEvent arg0) {
	txtDtDOfFcraRegiNo.setBackground(FocusBackground);
	txtDtDOfFcraRegiNo.setForeground(FocusForeground);
				
	// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
*/			});
			txtDtYOrg.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {

					/*if(txtDtDOrg.getText().trim().equals("") && txtDtMOrg.getText().trim().equals("") && txtDtYOrg.getText().trim().equals(""))
					{
					//	return;
						txtFcraRegiNo.setFocus();
						return;
					}
					*///	txtDtYOrg.setBackground(Background);
					//txtDtYOrg.setForeground(Foreground);
					
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					
				/*	if(txtDtDOrg.getText().trim().equals("") )
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a date in DD format.");
						//msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOrg.setFocus();
								
							}
						});
						return;
					}
					
					
					if(txtDtMOrg.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Month in MM format.");
						//msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOrg.setFocus();
								
							}
						});
						return;
					}
					
				
					if(txtDtYOrg.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
						//msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtFcraRegiNo.setFocus();
								
							}
						});
						return;
					}
					
				*/	
					/*if(txtDtYOrg.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
						//msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtFcraRegiNo.setFocus();
								
							}
						});
						return;
					}
					*/
/*					if(!txtDtYOrg.getText().equals(""))
					{
						if(!txtDtYOrg.getText().trim().equals("")&& Integer.valueOf(txtDtYOrg.getText())<1990)
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
						    //msgDayErr.open();
							txtDtYOrg.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOrg.setFocus();
									txtDtYOrg.selectAll();
									
								}
							});
							return;
						}

						
										if(txtDtYOrg.getText().length( ) < txtDtYOrg.getTextLimit())
					{
						MessageBox msgYearErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgYearErr.setText("Validation Date Error!");
						msgYearErr.setMessage("Please enter year in 4 digits format (YYYY)");
						msgYearErr.open();
						
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOrg.setFocus();
								txtDtYOrg.setText("");
								
							}
						});
						return;
						
					}
				}*/
					/*DateValidate dv = new DateValidate(Integer.valueOf(txtDtMOrg.getText()) ,Integer.valueOf(txtDtDOrg.getText()) ,Integer.valueOf(txtDtYOrg.getText()));
					String validationResult = dv.toString();
					if(validationResult.substring(2,3).equals("0"))
					{
						MessageBox msgErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgErr.setText("Validation Date Error!");
						msgErr.setMessage("You have entered invalid Date");
						//msgErr.open();
					
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOrg.setFocus();
								
							}
					});
					return;
					}
					*/
					
					Calendar cal = Calendar.getInstance();
					try 
					{
						cal.set(Integer.valueOf(Integer.valueOf(txtDtYOrg.getText())-1),( Integer.valueOf(txtDtMOrg.getText())-1 )  , (Integer.valueOf(txtDtDOrg.getText())-1) );
					} catch (NumberFormatException e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					
					cal.add(Calendar.YEAR , 1);
					Date nextYear = cal.getTime();
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String FinalDate = sdf.format(nextYear);
					
					
					/*Calendar cal = Calendar.getInstance();
					cal.set(Integer.valueOf(Integer.valueOf(txtDtYOrg.getText())-1),( Integer.valueOf(txtDtMOrg.getText())-1 )  , (Integer.valueOf(txtDtDOrg.getText())-1) );
					cal.add(Calendar.YEAR , 1);
					Date nextYear = cal.getTime();
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String FinalDate = sdf.format(nextYear);
					*/
					/*txtToDtDay.setText(FinalDate.substring(0,2) );
					txtToDtMonth.setText(FinalDate.substring(3,5));
					txtToDtYear.setText(FinalDate.substring(6));*/
					

				}
				//@Override
				/*public void focusGained(FocusEvent arg0) {
					txtDtYOrg.setBackground(FocusBackground);
					txtDtYOrg.setForeground(FocusForeground);
					
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}*/
			});
			
			
			
			txtDtDOrg.addVerifyListener(new VerifyListener() {
				
				@Override
				public void verifyText(VerifyEvent arg0) {
					// TODO Auto-generated method stub
//					/*if(verifyFlag== false)
//					{
//						arg0.doit= true;
//						return;
//					}*/
					switch (arg0.keyCode) {
		            case SWT.BS:           // Backspace
		            case SWT.DEL:          // Delete
		            case SWT.HOME:         // Home
		            case SWT.END:          // End
		            case SWT.ARROW_LEFT:   // Left arrow
		            case SWT.ARROW_RIGHT:  // Right arrow
		            case SWT.TAB:
		            case SWT.CR:
		            case SWT.KEYPAD_CR:
		            case SWT.KEYPAD_DECIMAL:
		                return;
		        }
					if(arg0.keyCode==46)
					{
						return;
					}
		        if (!Character.isDigit(arg0.character)) {
		            arg0.doit = false;  // disallow the action
		        }

				}
			});
			
			
			
		txtDtMOrg.addVerifyListener(new VerifyListener() {
				
				@Override
				public void verifyText(VerifyEvent arg0) {
					// TODO Auto-generated method stub
//					/*if(verifyFlag== false)
//					{
//						arg0.doit= true;
//						return;
//					}*/
					switch (arg0.keyCode) {
		            case SWT.BS:           // Backspace
		            case SWT.DEL:          // Delete
		            case SWT.HOME:         // Home
		            case SWT.END:          // End
		            case SWT.ARROW_LEFT:   // Left arrow
		            case SWT.ARROW_RIGHT:  // Right arrow
		            case SWT.TAB:
		            case SWT.CR:
		            case SWT.KEYPAD_CR:
		            case SWT.KEYPAD_DECIMAL:
		                return;
		        }
					if(arg0.keyCode==46)
					{
						return;
					}
		        if (!Character.isDigit(arg0.character)) {
		           arg0.doit = false;  // disallow the action
		        }

				}
			});
		
		txtDtYOrg.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		txtDtMOfFcraRegiNo.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		
	txtDtDOfFcraRegiNo.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
//			/*if(verifyFlag== false)
//			{
//				arg0.doit= true;
//				return;
//			}*/
			switch (arg0.keyCode) {
	        case SWT.BS:           // Backspace
	        case SWT.DEL:          // Delete
	        case SWT.HOME:         // Home
	        case SWT.END:          // End
	        case SWT.ARROW_LEFT:   // Left arrow
	        case SWT.ARROW_RIGHT:  // Right arrow
	        case SWT.TAB:
	        case SWT.CR:
	        case SWT.KEYPAD_CR:
	        case SWT.KEYPAD_DECIMAL:
	            return;
	    }
			if(arg0.keyCode==46)
			{
				return;
			}
	    if (!Character.isDigit(arg0.character)) {
	        arg0.doit = false;  // disallow the action
	    }

		}
	});
	txtDtYOfFcraRegiNo.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
//			/*if(verifyFlag== false)
//			{
//				arg0.doit= true;
//				return;
//			}*/
			switch (arg0.keyCode) {
	        case SWT.BS:           // Backspace
	        case SWT.DEL:          // Delete
	        case SWT.HOME:         // Home
	        case SWT.END:          // End
	        case SWT.ARROW_LEFT:   // Left arrow
	        case SWT.ARROW_RIGHT:  // Right arrow
	        case SWT.TAB:
	        case SWT.CR:
	        case SWT.KEYPAD_CR:
	        case SWT.KEYPAD_DECIMAL:
	            return;
	    }
			if(arg0.keyCode==46)
			{
				return;
			}
	    if (!Character.isDigit(arg0.character)) {
	        arg0.doit = false;  // disallow the action
	    }

		}
	});

		
			txtDtDOfFcraRegiNo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					/*txtDtDOfFcraRegiNo.setBackground(Background);
					txtDtDOfFcraRegiNo.setForeground(Foreground);
				*/
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
										
			/*		if(!txtDtDOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtDOfFcraRegiNo.getText())> 31 || Integer.valueOf(txtDtDOfFcraRegiNo.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid date");
						msgdateErr.open();
						
						txtDtDOfFcraRegiNo.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOfFcraRegiNo.setFocus();
								
							}
							
						});
						return;
					}
			*/		if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
					{	
						String dt = txtDtDOfFcraRegiNo.getText();
						txtDtDOfFcraRegiNo.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						return;
					}
				}
/*@Override
				public void focusGained(FocusEvent arg0) {
	txtDtDOfFcraRegiNo.setBackground(FocusBackground);
	txtDtDOfFcraRegiNo.setForeground(FocusForeground);
				
	// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
*/			});
			
			txtDtMOfFcraRegiNo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					/*txtDtDOfFcraRegiNo.setBackground(Background);
					txtDtDOfFcraRegiNo.setForeground(Foreground);
				*/
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
										
				/*	if(!txtDtMOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtMOfFcraRegiNo.getText())> 12 || Integer.valueOf(txtDtMOfFcraRegiNo.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid Month");
						msgdateErr.open();
						
						txtDtMOfFcraRegiNo.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOfFcraRegiNo.setFocus();
								return;
								
							}
							
						});
						
						return;
					}
				*/	if(!txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
					{	
						String dt = txtDtMOfFcraRegiNo.getText();
						txtDtMOfFcraRegiNo.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						return;
					}
				}
/*@Override
				public void focusGained(FocusEvent arg0) {
	txtDtDOfFcraRegiNo.setBackground(FocusBackground);
	txtDtDOfFcraRegiNo.setForeground(FocusForeground);
				
	// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
*/			});
			
			
			txtDtYOfFcraRegiNo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {

					/*if(txtDtDOfFcraRegiNo.getText().trim().equals("") && txtDtMOfFcraRegiNo.getText().trim().equals("") && txtDtYOfFcraRegiNo.getText().trim().equals(""))
					{
					//	return;
						txtAddress.setFocus();
						return;
					}
					*///	txtDtYOrg.setBackground(Background);
					//txtDtYOrg.setForeground(Foreground);
					
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					
					/*if(txtDtDOfFcraRegiNo.getText().trim().equals("") )
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a date in DD format.");
						//msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOfFcraRegiNo.setFocus();
								
							}
						});
						return;
					}
					
					
					if(txtDtMOfFcraRegiNo.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Month in MM format.");
						//msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
								
							}
						});
						return;
					}
					
				
					if(txtDtYOfFcraRegiNo.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
						//msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtAddress.setFocus();
								
							}
						});
						return;
					}
					
			*/
					/*if(txtDtYOfFcraRegiNo.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
						//msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtAddress.setFocus();
								
							}
						});
						return;
					}
					*/
					
				/*	if(!txtDtYOfFcraRegiNo.getText().equals(""))
					{
						if(!txtDtYOfFcraRegiNo.getText().trim().equals("")&& Integer.valueOf(txtDtYOfFcraRegiNo.getText())<1990)
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
						    msgDayErr.open();
							txtDtYOfFcraRegiNo.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									txtDtYOfFcraRegiNo.selectAll();
									
								}
							});
							return;
						}

						
						
						
										if(txtDtYOfFcraRegiNo.getText().length( ) < txtDtYOfFcraRegiNo.getTextLimit())
					{
						MessageBox msgYearErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgYearErr.setText("Validation Date Error!");
						msgYearErr.setMessage("Please enter year in 4 digits format (YYYY)");
						msgYearErr.open();
						
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
								txtDtYOfFcraRegiNo.setText("");
						return;		
							}
						});
						return;
						
					}
				}
*/					/*DateValidate dv = new DateValidate(Integer.valueOf(txtDtMOfFcraRegiNo.getText()) ,Integer.valueOf(txtDtDOfFcraRegiNo.getText()) ,Integer.valueOf(txtDtYOfFcraRegiNo.getText()));
					String validationResult = dv.toString();
					if(validationResult.substring(2,3).equals("0"))
					{
						MessageBox msgErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgErr.setText("Validation Date Error!");
						msgErr.setMessage("You have entered invalid Date");
						msgErr.open();
					
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOfFcraRegiNo.setFocus();
								
							}
					});
					return;
					}
					*/
					
					Calendar cal = Calendar.getInstance();
					try 
					{
						cal.set(Integer.valueOf(Integer.valueOf(txtDtYOfFcraRegiNo.getText())-1),( Integer.valueOf(txtDtMOfFcraRegiNo.getText())-1 )  , (Integer.valueOf(txtDtDOfFcraRegiNo.getText())-1) );
					} catch (NumberFormatException e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					cal.add(Calendar.YEAR , 1);
					Date nextYear = cal.getTime();
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String FinalDate = sdf.format(nextYear);
					
					
					/*Calendar cal = Calendar.getInstance();
					cal.set(Integer.valueOf(Integer.valueOf(txtDtYOfFcraRegiNo.getText())-1),( Integer.valueOf(txtDtMOfFcraRegiNo.getText())-1 )  , (Integer.valueOf(txtDtDOfFcraRegiNo.getText())-1) );
					cal.add(Calendar.YEAR , 1);
					Date nextYear = cal.getTime();
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					String FinalDate = sdf.format(nextYear);
					*/
					/*txtToDtDay.setText(FinalDate.substring(0,2) );
					txtToDtMonth.setText(FinalDate.substring(3,5));
					txtToDtYear.setText(FinalDate.substring(6));*/
					

				}
				//@Override
				/*public void focusGained(FocusEvent arg0) {
					txtDtYOrg.setBackground(FocusBackground);
					txtDtYOrg.setForeground(FocusForeground);
					
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}*/
			});
			

			
/*			txtDtYOfFcraRegiNo.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
//					txtDtYOfFcraRegiNo.setBackground(Background);
	//				txtDtYOfFcraRegiNo.setForeground(Foreground);
				
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					if(txtDtDOfFcraRegiNo.getText().trim().equals("") )
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a date in DD format.");
						//msgDayErr.open();
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOfFcraRegiNo.setFocus();
								
							}
							
						});
						return;
						
					}
					if(txtDtMOfFcraRegiNo.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Month in MM format.");
						//msgDayErr.open();
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
								
							}
							
						});
						
						return;
						
					}
					
					if(!txtDtYOfFcraRegiNo.getText().trim().equals("")&& Integer.valueOf(txtDtYOfFcraRegiNo.getText())<1990)
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Error!");
						msgDayErr.setMessage("You have entered an Invalid Year.");
					    msgDayErr.open();
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
								txtDtYOfFcraRegiNo.setText("");
								
								//txtDtYOfFcraRegiNo.selectAll();
							return;	
							}
						});
						//return;
					}


					try {
						if(txtDtYOfFcraRegiNo.getText().trim().equals("")&& Integer.valueOf(txtDtYOfFcraRegiNo.getText())<=0)
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
							msgDayErr.open();
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									
								}
							});
							return;
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(txtDtYOfFcraRegiNo.getText().length( ) < txtDtYOfFcraRegiNo.getTextLimit())
					{
						MessageBox msgYearErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgYearErr.setText("Validation Date Error!");
						msgYearErr.setMessage("Please enter year in 4 digits format (YYYY)");
						//msgYearErr.open();
						
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
								txtDtYOfFcraRegiNo.setText("");
								
							}
						});
						return;
						
					}
					try {
						DateValidate dv = new DateValidate(Integer.valueOf(txtDtMOfFcraRegiNo.getText()) ,Integer.valueOf(txtDtDOfFcraRegiNo.getText()) ,Integer.valueOf(txtDtYOfFcraRegiNo.getText()));
						String validationResult = dv.toString();
						if(validationResult.substring(2,3).equals("0"))
						{
							MessageBox msgErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgErr.setText("Validation Date Error!");
							msgErr.setMessage("You have entered invalid Date");
							msgErr.open();
						
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
								
							}
						});
						return;
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						Calendar cal = Calendar.getInstance();
						cal.set(Integer.valueOf(Integer.valueOf(txtDtYOfFcraRegiNo.getText())-1),( Integer.valueOf(txtDtMOfFcraRegiNo.getText())-1 )  , (Integer.valueOf(txtDtDOfFcraRegiNo.getText())-1) );
						cal.add(Calendar.YEAR , 1);
						Date nextYear = cal.getTime();
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						String FinalDate = sdf.format(nextYear);
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					txtToDtDay.setText(FinalDate.substring(0,2) );
					txtToDtMonth.setText(FinalDate.substring(3,5));
					txtToDtYear.setText(FinalDate.substring(6));
					

				}
				@Override
				public void focusGained(FocusEvent arg0) {
					txtDtYOfFcraRegiNo.setBackground(FocusBackground);
					txtDtYOfFcraRegiNo.setForeground(FocusForeground);
				
					// TODO Auto-generated method stub
				//	super.focusGained(arg0);
				}
			});*/
			
		}

		public static String toTitleCase(String input) 
		{
		    StringBuilder titleCase = new StringBuilder();
		    boolean nextTitleCase = true;
		    char second_char=0;
		    String[] seperated_by_space =input.split(" "); 
		    try {
				for(int i= 0; i<seperated_by_space.length; i++)
				{
					if (seperated_by_space[i].length() > 1)
						{
							second_char = seperated_by_space[i].charAt(1);
						} 
						else if (seperated_by_space[i].length() == 1)
						{
							second_char = seperated_by_space[i].charAt(0);
						}
						if (Character.isDigit(second_char))
						{
							for (char c : seperated_by_space[i].toCharArray())
							{

								titleCase.append(c);
							}	
						}
						if (!Character.isAlphabetic(second_char) &&!Character.isDigit(second_char) )
						{
							for (char c : seperated_by_space[i].toCharArray())
							{

								titleCase.append(c);
							}
						}
						if (Character.isLowerCase(second_char))
						{
							for (char c : seperated_by_space[i].toCharArray())
							{
								if (nextTitleCase) {
									c = Character.toTitleCase(c);
									nextTitleCase = false;
								}

								titleCase.append(c);
							}
						} 
						else if (Character.isUpperCase(second_char)) 
						{
							for (char c : seperated_by_space[i].toCharArray())
							{

								titleCase.append(c);
							}
						}
						
					
					
					if (i != seperated_by_space.length - 1) 
					{
						titleCase.append(" ");
						nextTitleCase = true;
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    return titleCase.toString();
		}	
	
		
		
		public void makeaccessible(Control c)
		{
			/*
			 * getAccessible() method is the method of class Controlwhich is the
			 * parent class of all the UI components of SWT including Shell.so when
			 * the shell is made accessible all the controls which are contained by
			 * that shell are made accessible automatically.
			 */
			c.getAccessible();
		}


		
		protected void checkSubclass()
		{
			//this is blank method so will disable the check that prevents subclassing of shells.
		}

		private void showView()
		{
			while(! this.isDisposed())
			{
				if(! this.getDisplay().readAndDispatch())
				{
					this.getDisplay().sleep();
					if ( ! this.getMaximized())
					{
						this.setMaximized(true);
					}
				}
				
			}
			this.dispose();


		}

	
}
