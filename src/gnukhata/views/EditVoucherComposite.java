package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.transactionController;
import gnukhata.controllers.reportmodels.AddVoucher;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;



public class EditVoucherComposite extends Composite
{
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	Color lightBlue;
	Color tableBackground ;
	Color tableForeground ;
	
	int tblIndex;
	boolean msgflag = false;
	int rowcntr;
	boolean rowfocusflag=false;
	
	boolean editAmt = false;
	boolean accflag=false;
	
	String strOrgName;
	String strFromYear;
	String strToYear;
	
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	
	Label lblvouchertype;
	
	List<Combo> CrDrFlags = new ArrayList<Combo>();
	
	//List<Text> DrAmounts = new ArrayList<Text>();
	List<Text> CrAmounts = new ArrayList<Text>();
		
	
	Combo holdfocuscombo;
	
	
	
	Button btnConfirm;
	Button btnBack;
	
	int voucherEditCode = 0;
	int VoucherEditFlag = 0;
	boolean psdrilldown = false;
	String tbType = null;
	boolean narrationFlag = false;
	String AccountName = "";
	String endDate;
	String startDate = null;
	boolean ledgerDrilldown;
	String selectproject ="" ;
	boolean tbdrilldown = false;
	String PN="";
	int crdrleft = 1;
	int crdrright = 13;
	int accountsleft = 13;
	int accountsright = 50;
	int dramountleft = 50;
	int dramountright = 72;
	int cramountleft = 72;
	int cramountright = 94;
	/*int removeleft = 85;
	int removeright = 99;*/
	int currenttop = 1;
	int incrementby = 8;
	int grpVoucherWidth = 0;
	int grpVoucherHeight = 0;
	
	boolean dualflag;
	//NumberFormat nf;
	int totalWidth = 0;
	Group grpEditVoucher;
	boolean verifyFlag = false; 
	boolean dualledgerflag;
	
	String oldaccname;
	String oldfromdate;
	String oldenddate;
	String oldselectproject;
	String oldprojectname;
	boolean oldnarration;
	String crdrcombofocus = "";
	boolean findvoucher;
	ArrayList<String> SelectedAccounts = new ArrayList<String>();
	//ArrayList<String> CrSelectedAccounts = new ArrayList<String>();
	long searchTexttimeout = 0;
	String searchText;
	Rectangle grpBounds;
	static Display display;
	double totalDrAmount = 0.00;
	double totalCrAmount = 0.00;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	List<String> masterQueryParams = new ArrayList<String>();
	List<Object> detailQueryParams = new ArrayList<Object>();
	//Label lbldate;
	Text txtddate;
	Label dash1;
	Text txtmdate;
	Label dash2;
	Text txtyrdate;
	public static Label lblsavemsg;
	Label lblvoucherno;
	Text txtvoucherno;
	

	
	
	Group addvoucher;
	//Button btnConfirm;
	Button btnAddAccount;
	Label lblselprj;
	CCombo comboselprj;
	Label lblnarration;
	Text txtnarration;
	NumberFormat nf;
	//int rowIndex = 2;
		
	Group grpVoucher;
	Group grpLabel;
	//boolean totalRowCalled = false;
	//int grpVoucherHeight = 0;
		//int grpVoucherWidth = 0;
	String typeFlag;
	Label lbldate;
	
	TableViewer tblVoucherView;
	TableViewerColumn colAccountName;
	TableViewerColumn colDrAmount;
	TableViewerColumn colCrAmount;
	ArrayList<AddVoucher>  lstVoucherRows;
	//Label lblCrDr;
	Combo cmbCr_Dr;
	Label lblAccounts;
	CCombo cmbAccounts;
	Label lblCrDrAmount;
	Text txtCrDrAmount;
	Label totalcr_dr;
	Label totalDr;
	Text txttotalDr;
	Label totalCr;
    Text txttotalCr;
    Group grpEntry;
  //  Button btnConfirm;
    Boolean focusflag = false;
    int selectedIndex;
    boolean alterFlag = false;
    int search_flag;
    String[] search_values;
    //long searchTexttimeout = 0;
	//String searchText;
    /*Color bgbtnColor;
    Color fgbtnColor;
    Color bgcomboColor;
    Color fgcomboColor;
    Color bgtxtColor;
    Color fgtxtColor;
    Color fgtblColor;
    Color bgtblColor;
    */
    
		
	public EditVoucherComposite(Composite parent, int style,String voucherType, int voucherCode,boolean findvoucherflag, int editFlag, boolean tbdrilldown,boolean psdrilldown,String tbType, boolean ledgerDrilldown, String startDate,String oldfromdate1, String endDate,String oldenddate1, String AccountName,String oldaccname1,String ProjectName,String oldprojectname1, boolean narrationFlag,boolean narration,String selectproject,String oldselectproject1,boolean dualledgerflag,int searchFlag, String[] searchDetails) 
	{
		
		
		super(parent, style);
		search_flag=searchFlag;
		search_values=searchDetails;
		sdf.setLenient(false);
		typeFlag= voucherType;
		findvoucher=findvoucherflag;
		voucherEditCode = voucherCode;
		VoucherEditFlag = editFlag;
		nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		
		//oldvalues
		this.oldaccname=oldaccname1;
		this.oldfromdate=oldfromdate1;
		this.oldenddate=oldenddate1;
		this.oldselectproject=oldselectproject1;
		this.oldprojectname=oldprojectname1;
		this.oldnarration=narration;
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		
	
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		this.ledgerDrilldown= ledgerDrilldown;
		this.tbdrilldown = tbdrilldown;
		this.tbType= tbType;
		this.AccountName = AccountName;
		this.PN=ProjectName;
		this.narrationFlag= narrationFlag;
		this.psdrilldown = psdrilldown;
		this.selectproject= selectproject;
		this.startDate= startDate;
		this.endDate= endDate;
		this.dualledgerflag=dualledgerflag;
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout=new FormData();
	    
		    
	    Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(strOrgName.replace("&", "&&")+"\n"+"For Financial Year "+"From "+strFromYear+" To "+strToYear);
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(3);
		lblOrgDetails.setLayoutData(layout);
		
		lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display, "finallogo1.png");
		lblLogo.setImage(globals.logo);
		
				
		lblLine = new Label(this, SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,5);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		lblLine.setLayoutData(layout);
		
		
		lblvoucherno = new Label(this,SWT.NONE);
		lblvoucherno.setText("Voucher No *");
		lblvoucherno.setFont(new Font(display, "Time New Roman",12,SWT.NORMAL));
		layout =new FormData();
		layout.top = new FormAttachment(lblLine,5);
		layout.left = new FormAttachment(2);
		lblvoucherno.setLayoutData(layout);
		
				
		txtvoucherno = new Text(this,SWT.BORDER |SWT.NONE);
		if(VoucherEditFlag==1)
		{
			txtvoucherno.setEnabled(false);
			
		}
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,5);
		layout.left = new FormAttachment(lblvoucherno,5);
		layout.right = new FormAttachment(20);
		txtvoucherno.setLayoutData(layout);

		//txtvoucherno.setEditable(true);
		//txtvoucherno.setText(voucherno);
		//txtvoucherno.clearSelection();
		
		lblvouchertype = new Label(this, SWT.NONE);
		lblvouchertype.setFont(new Font(display, "Times New Roman", 20, SWT.ITALIC|SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,2);
		layout.left = new FormAttachment(44);
		//layout.right= new FormAttachment(20);
		lblvouchertype.setLayoutData(layout);
		lblvouchertype.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_RED));
		
		
		layout = new FormData();
		
		
		lbldate = new Label(this, SWT.NONE);
		lbldate.setText("&Date :");
		lbldate.setFont(new Font(display, "Time New Roman", 10, SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,5);
		layout.left = new FormAttachment(85);
		//layout.right = new FormAttachment(50);
		/*layout.bottom = new FormAttachment(12)*/;
		lbldate.setLayoutData(layout);

		
		
		
		txtddate = new Text(this, SWT.BORDER);
		txtddate.setTextLimit(2);
		txtddate.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,5);
		layout.left = new FormAttachment(lbldate,5);
		//layout.right = new FormAttachment(49);
		//layout.bottom = new FormAttachment(12);
		txtddate.setLayoutData(layout);
		txtddate.setEditable(true);
		txtddate.setSelection(0, 2);

		dash1 = new Label(this, SWT.NONE);
		dash1.setText("-");
		dash1.setFont(new Font(display, "Time New Roman", 14, SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,5);
		layout.left = new FormAttachment(txtddate,5);
		//layout.right = new FormAttachment(8);
		//layout.bottom = new FormAttachment(10);
		dash1.setLayoutData(layout);
		
		txtmdate = new Text(this, SWT.BORDER);
		txtmdate.setTextLimit(2);
		txtmdate.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,5);
		layout.left = new FormAttachment(dash1,5);
		//layout.right = new FormAttachment(60);
		//layout.bottom = new FormAttachment(12);
		txtmdate.setLayoutData(layout);
		txtmdate.setEditable(true);
		txtmdate.setSelection(0, 2);

		dash2 = new Label(this, SWT.NONE);
		dash2.setText("-");
		dash2.setFont(new Font(display, "Time New Roman", 14, SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,5);
		layout.left = new FormAttachment(txtmdate,5);
		//layout.right = new FormAttachment(61);
		//layout.bottom = new FormAttachment(10);
		dash2.setLayoutData(layout);
		
		txtyrdate = new Text(this, SWT.BORDER);
		txtyrdate.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,5);
		layout.left = new FormAttachment(dash2,5);
		//layout.right = new FormAttachment(66);
		//layout.bottom = new FormAttachment(12);
		txtyrdate.setTextLimit(4);
		txtyrdate.setLayoutData(layout);
		txtyrdate.setEditable(true);
		txtyrdate.setSelection(0, 4);
	
				
		//txtvoucherno.setEditable(false);
		/*if(VoucherEditFlag==2)
		{		 
		txtvoucherno.setEditable(true);
		}*/
		
		/*grpEditVoucher = new Group(this, SWT.BORDER);
		grpEditVoucher.setFont(new Font(display, "Time New Roman",10,SWT.RIGHT));
		layout = new FormData();
		layout.top = new FormAttachment(lblvouchertype,14);
		layout.left = new FormAttachment(5);
		layout.right = new FormAttachment(81);
		layout.bottom = new FormAttachment(68);
		grpEditVoucher.setLayoutData(layo4ut);*/
		//grpEditVoucher.setText("Edit"+"" +"Voucher");
		
		grpEditVoucher = new Group(this, SWT.BORDER | SWT.V_SCROLL);
		layout = new FormData();
		layout.top = new FormAttachment(lblvouchertype,14);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(71);
		

		//voucherTable.setLayoutData(layout);
		
		grpEditVoucher.setLayoutData(layout);
		grpEditVoucher.setLayout(new FormLayout());
		
		
		grpEntry = new Group(this, SWT.BORDER);
		FormData  entry = new FormData();
		entry.left = new FormAttachment(2);
		entry.right = new FormAttachment(98);
		entry.top =  new FormAttachment(grpEditVoucher, 5);
		entry.bottom = new FormAttachment(78);
		grpEntry.setLayoutData(entry);
		grpEntry.setLayout(new FormLayout());
		

		
		//this.setInitialVoucher();
		//this.makeaccssible(grpEditVoucher);
		
		Object[] projectlist=transactionController.getAllProjects();

		lblselprj = new Label(this,SWT.NONE);
		lblselprj.setText("Select Project :");
		lblselprj.setFont(new Font(display, "Time New Roman",10,SWT.RIGHT));
		layout = new FormData();
		layout.top=new FormAttachment(grpEntry, 5);
		layout.left=new FormAttachment(2);
		//layout.right=new FormAttachment(40);
		//layout.bottom=new FormAttachment(85);
		lblselprj.setLayoutData(layout);
		
		if (projectlist.length > 0) 
		{
			lblselprj.setVisible(true);
		}
		else 
		{
			lblselprj.setVisible(false);
		}
		
		
		comboselprj = new CCombo(this,SWT.READ_ONLY|SWT.BORDER);
		//comboselprj.setToolTipText("Select your Orgnization");
		layout = new FormData();
		layout.top=new FormAttachment(grpEntry, 5);
		layout.left=new FormAttachment(lblselprj);
		layout.right=new FormAttachment(35);
		layout.bottom=new FormAttachment(82);
		comboselprj.setLayoutData(layout);
		comboselprj.setVisible(true);
		comboselprj.add("No Project");

		comboselprj.setLayoutData(layout);
		if (projectlist.length > 0) 
		{
			comboselprj.setVisible(true);
			//comboselprj.add("No Project");
		}
		else 
		{
			comboselprj.setVisible(false);
		}
		
		String[] allProjects = gnukhata.controllers.transactionController.getAllProjects();
		for (int i = 0; i < allProjects.length; i++ )
		{
			comboselprj.add(allProjects[i]);
		}
		comboselprj.select(0);
		

		lblnarration = new Label(this,SWT.NONE);
		lblnarration.setText("Narration       : ");
		lblnarration.setFont(new Font(display, "Time New Roman",10,SWT.RIGHT));
		layout = new FormData();
		layout.top = new FormAttachment(grpEntry,5);
		layout.left = new FormAttachment(44);
		//layout.right = new FormAttachment(61);
		//layout.bottom = new FormAttachment(94);
		lblnarration.setLayoutData(layout);
		
		txtnarration = new Text(this, SWT.MULTI | SWT.BORDER|SWT.WRAP);
		txtnarration.setFont(new Font(display, "Time New Roman",10,SWT.None));
		layout = new FormData();
		layout.top = new FormAttachment( grpEntry,5);
		layout.left = new FormAttachment(lblnarration);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(84);
		txtnarration.setLayoutData(layout);
		lblsavemsg = new Label(this, SWT.NONE);
		//lblsavemsg.setText("Voucher saved with voucher number " );
		lblsavemsg.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD|SWT.COLOR_RED));
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(30);
		layout.right = new FormAttachment(70);
		layout.bottom = new FormAttachment(6);
		lblsavemsg.setLayoutData(layout);
		
		
		btnConfirm = new Button(this,SWT.PUSH);
		btnConfirm.setText("&Confirm");
		btnConfirm.setFont(new Font(display, "Time New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(txtnarration,15);
		layout.left = new FormAttachment(33);
		btnConfirm.setLayoutData(layout);
		
		btnBack = new Button(this,SWT.PUSH);
		btnBack.setText("&Back");
		btnBack.setFont(new Font(display, "Time New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(txtnarration,15);
		layout.left = new FormAttachment(btnConfirm,10);
		btnBack.setLayoutData(layout);
		
		btnAddAccount = new Button(this, SWT.PUSH);
		btnAddAccount.setText("Add Acc&ount");
		btnAddAccount.setFont(new Font(display, "Time New Roman", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(txtnarration,15);
		layout.left = new FormAttachment(btnBack,10);
		btnAddAccount.setLayoutData(layout);
		
		
		
		this.getAccessible();
		Object[] masterDetails = transactionController.getVoucherMaster(voucherCode);
		
		Object[] result=transactionController.getVoucherMaster(voucherCode);
		String strprjname=result[4].toString();
		
		typeFlag = masterDetails[2].toString();
		txtvoucherno.setText(masterDetails[0].toString());
		Display.getCurrent().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				txtvoucherno.selectAll();
			}
		});
		txtddate.setText(masterDetails[1].toString().substring(0,2));
		txtmdate.setText(masterDetails[1].toString().substring(3,5));
		txtyrdate.setText(masterDetails[1].toString().substring(6));
		//editVoucher.setText(masterDetails[2].toString());
		//typeFlag = masterDetails[2].toString();
		if(VoucherEditFlag==1) {
			lblvouchertype.setText("Edit " +typeFlag+" Voucher");
		}
		else{
			lblvouchertype.setText("Clone "+typeFlag + " Voucher");
		}
		txtnarration.setText(masterDetails[3].toString());
		comboselprj.setText(strprjname);
		
		
		
	ArrayList<AddVoucher> transactionDetails = transactionController.getVoucherDetails(voucherCode);
		
	this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		
		grpVoucherWidth = grpEditVoucher.getClientArea().width;
	grpVoucherHeight = grpEditVoucher.getClientArea().height;
	tblVoucherView = new TableViewer(grpEditVoucher, SWT.V_SCROLL| SWT.BORDER);
	FormData grpData = new FormData();
	grpData.top = new FormAttachment(0);
	grpData.left = new FormAttachment(0);
	grpData.right = new FormAttachment(100);
	grpData.bottom =  new FormAttachment(94);
	tblVoucherView.getTable().setLayoutData(grpData);
	tblVoucherView.getTable().setHeaderVisible(true);
	tblVoucherView.getTable().setLinesVisible(true);
	tblVoucherView.getTable().setFont(new Font(display,"UBUNTU",10,SWT.BOLD));
	
	lstVoucherRows = new ArrayList<AddVoucher>();
	
	colAccountName = new TableViewerColumn(tblVoucherView,SWT.NONE );
	colAccountName.getColumn().setWidth(44 * grpVoucherWidth/ 100 );
	colAccountName.getColumn().setText("                                       Account Name ");
	colAccountName.setLabelProvider(new ColumnLabelProvider(){
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
			AddVoucher row = (AddVoucher) element;
			return row.getAccountName();
			
			//return super.getText(element);
		}
	}
	
			);
	colDrAmount= new TableViewerColumn(tblVoucherView,SWT.NONE );
	colDrAmount.getColumn().setWidth(29 * grpVoucherWidth/ 100 );
	colDrAmount.getColumn().setText("Debit Amount                                           ");
	colDrAmount.getColumn().setAlignment(SWT.RIGHT);
	colDrAmount.setLabelProvider(new ColumnLabelProvider(){
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stubp
			AddVoucher row = (AddVoucher) element;
			NumberFormat nf = NumberFormat.getInstance();
			nf.setGroupingUsed(false);
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			
			return nf.format(row.getDrAmount());
			
			//return super.getText(element);
		}
	}
	
			);
	colCrAmount = new TableViewerColumn(tblVoucherView,SWT.NONE );
	colCrAmount.getColumn().setWidth(20* grpVoucherWidth/ 100 );
	colCrAmount.getColumn().setText("Credit Amount                             ");
	colCrAmount.getColumn().setAlignment(SWT.RIGHT);
	colCrAmount.setLabelProvider(new ColumnLabelProvider(){
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
			AddVoucher row = (AddVoucher) element;
			NumberFormat nf = NumberFormat.getInstance();
			nf.setGroupingUsed(false);
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			return nf.format(row.getCrAmount());
			
			//return super.getText(element);
		}
	}
	
			);
	 
	tblVoucherView.setContentProvider(new ArrayContentProvider());
	tblVoucherView.setInput( transactionDetails);
    grpData = new FormData();
	grpData.top = new FormAttachment(82);
	grpData.left = new FormAttachment(0);
	grpData.right=new FormAttachment(10);
	
	/*lblCrDr = new Label(grpEntry, SWT.NONE);
	lblCrDr.setText("Dr/Cr");
	grpData = new FormData();
	grpData.top = new FormAttachment(20);
	grpData.left = new FormAttachment(0);
	grpData.bottom = new FormAttachment(100);
	//grpData.right=new FormAttachment(10);
	lblCrDr.setLayoutData(grpData);*/
	
	cmbCr_Dr = new Combo(grpEntry, SWT.BORDER| SWT.READ_ONLY);
	cmbCr_Dr.setFont(new Font(display, "Times New Roman", 11, SWT.NORMAL));
	grpData= new FormData();
	grpData.top = new FormAttachment(10);
	grpData.left = new FormAttachment(0);
	grpData.bottom = new FormAttachment(90);
	//layout.right = new FormAttachment(84);
	//layout.bottom = new FormAttachment(57);
	cmbCr_Dr.add("Dr");
	cmbCr_Dr.select(0);	
	//cmbCr_Dr.setLayoutData(grpData);

	cmbCr_Dr.setLayoutData(grpData);
	//drpdwntrialbal.select(0);

	//cmbCr_Dr.setVisible(false);
	
	

	lblAccounts = new Label(grpEntry, SWT.NONE);
	lblAccounts.setText("Account Name");
	grpData = new FormData();
	grpData.top = new FormAttachment(20);
	//grpData.left = new FormAttachment(cmbCr_Dr,50);
	grpData.left = new FormAttachment(25);
	grpData.bottom = new FormAttachment(100);
	//grpData.right=new FormAttachment(10);
	lblAccounts.setLayoutData(grpData);
	
	cmbAccounts = new CCombo(grpEntry, SWT.BORDER | SWT.READ_ONLY);
	cmbAccounts.setFont(new Font(display, "Times New Roman", 11, SWT.NORMAL));
	grpData= new FormData();
	grpData.top = new FormAttachment(10);
	grpData.left = new FormAttachment(lblAccounts,8);
	///grpData.right = new FormAttachment(50);
	
	grpData.bottom = new FormAttachment(90);
	//layout.right = new FormAttachment(8);
	//layout.bottom = new FormAttachment(57);
	
	//cmbCr_Dr.setLayoutData(grpData);

	cmbAccounts.add("----------------------------Please Select-------------------------------");
	
	
cmbAccounts.select(0);

	
cmbAccounts.setLayoutData(grpData);

	
	lblCrDrAmount = new Label(grpEntry, SWT.NONE);
	lblCrDrAmount.setText("Amount");
	grpData = new FormData();
	grpData.top = new FormAttachment(20);
	grpData.left = new FormAttachment(85);
	grpData.bottom = new FormAttachment(100);
	//grpData.right=new FormAttachment(10	);
	lblCrDrAmount.setLayoutData(grpData);
	
	txtCrDrAmount = new Text(grpEntry, SWT.BORDER | SWT.RIGHT);
	txtCrDrAmount.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
	grpData= new FormData();
	grpData.top = new FormAttachment(10);
	grpData.left = new FormAttachment(lblCrDrAmount,8);
	grpData.bottom = new FormAttachment(90);
	grpData.right = new FormAttachment(100);
	//layout.bottom = new FormAttachment(12);
	txtCrDrAmount.setLayoutData(grpData);

	txtCrDrAmount.setText("0.00");
	

	
	//lblCrDr.setVisible(false);
	cmbCr_Dr.setVisible(false);
	lblAccounts.setVisible(false);
	cmbAccounts.setVisible(false);
	lblCrDrAmount.setVisible(false);
	txtCrDrAmount.setVisible(false);
	grpEntry.setVisible(false);
	
	Label totalcr_dr=new Label (grpEditVoucher, SWT.BORDER);
	totalcr_dr.setText("Total Amount");
	grpData = new FormData();
	grpData.top = new FormAttachment(tblVoucherView.getTable());
	grpData.left = new FormAttachment(0);
	//pData.bottom = new FormAttachment(100);
	grpData.right=new FormAttachment(10);
	totalcr_dr.setLayoutData(grpData);
	
	totalDr=new Label (grpEditVoucher, SWT.BORDER | SWT.RIGHT);
	totalDr.setText("0.00");
	grpData = new FormData();
	grpData.top = new FormAttachment(tblVoucherView.getTable());
	grpData.left = new FormAttachment(57);
	//pData.bottom = new FormAttachment(100);
	grpData.right=new FormAttachment(73);
	totalDr.setLayoutData(grpData);
	
	totalCr=new Label (grpEditVoucher, SWT.BORDER | SWT.RIGHT);
	totalCr.setText("0.00");
	grpData = new FormData();
	grpData.top = new FormAttachment(tblVoucherView.getTable());
	grpData.left = new FormAttachment(86);
	//pData.bottom = new FormAttachment(100);
	grpData.right=new FormAttachment(100	);
	totalCr.setLayoutData(grpData);
	for(int RowCounter = 0; RowCounter < tblVoucherView.getTable().getItemCount(); RowCounter ++ )
	{
		AddVoucher VoucherRow = (AddVoucher) tblVoucherView.getElementAt(RowCounter);
		totalDrAmount = totalDrAmount + VoucherRow.getDrAmount();
		totalCrAmount = totalCrAmount + VoucherRow.getCrAmount();
	}
	totalDr.setText(nf.format(totalDrAmount));
	totalCr.setText(nf.format(totalCrAmount));
	
	
	//this.pack();
	
	// this.setEvents();
	//this.setInitialVoucher();
	//this.setEvents();
	this.makeaccssible(grpEditVoucher);
	this.makeaccssible(this);

	
	
	//btnConfirm.setEnabled(false);
	//txtvoucherno.setSelection(0,txtvoucherno.getText().length());
	

		
		
		//Object[] transactionDetails = transactionController.showEditCloneVoucher(grandparent, style, voucherType, voucherCode, findvoucherflag, editFlag, tbdrilldown, psdrilldown, tbType, ledgerDrilldown, startDate, oldfromdate1, endDate, oldenddate1, AccountName, oldaccname1, ProjectName, oldprojectname1, narrationFlag, narration, selectproject, oldselectproject1, dualledgerflag);(voucherCode);
		
				
			
		this.setEvents();
		
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
	    BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
	    lightBlue = new Color(this.getDisplay(),215,242,251);
	    tableBackground = new Color(this.getDisplay(),255, 255, 214);
	    tableForeground =  new Color(this.getDisplay(),184, 255, 148);
	    
		
		globals.setThemeColor(this, Background, Foreground);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        //globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		tblVoucherView.getTable().setBackground(lightBlue);
		grpEntry.setBackground(lightBlue);
		grpEntry.pack();
		
		TableItem[] items = tblVoucherView.getTable().getItems();
		for (int rowid=0; rowid<items.length; rowid++){
		    if (rowid%2==0) 
		    {
		    	items[rowid].setBackground(tableBackground);
		    }
		    else {
		    	items[rowid].setBackground(tableForeground);
		    }
		}
		
		if(VoucherEditFlag==1)
		{
			txtddate.setFocus();
			txtvoucherno.setBackground(FocusBackground);
			txtvoucherno.setForeground(FocusForeground);

			/*txtddate.setFocus();
			txtddate.setSelection(0, 2);*/
		}
		
		if(VoucherEditFlag==2)
		{
			txtvoucherno.setFocus();
			txtvoucherno.setBackground(FocusBackground);
			txtvoucherno.setForeground(FocusForeground);

		}
		txtvoucherno.setBackground(FocusBackground);
		txtvoucherno.setForeground(FocusForeground);


	}
	
	
	
	private void setEvents()

	{
		selectedIndex=-1;
		btnConfirm.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				txtCrDrAmount.notifyListeners(SWT.TRAVERSE_TAB_NEXT , new Event());
				
				
				if(!txtddate.getText().equals("") && Integer.valueOf ( txtddate.getText())<10 && txtddate.getText().length()< txtddate.getTextLimit())
				{
					txtddate.setText("0"+ txtddate.getText());
				}
				if(!txtmdate.getText().equals("") && Integer.valueOf ( txtmdate.getText())<10 && txtmdate.getText().length()< txtmdate.getTextLimit())
				{
					txtmdate.setText("0"+ txtmdate.getText());
				}
				
				if(txtvoucherno.getText().trim().equals(""))
				{
					MessageBox msgVoucherCode = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
					msgVoucherCode.setText("Error!");
					msgVoucherCode.setMessage("Please Enter a Voucher Number");
					msgVoucherCode.open();
					txtvoucherno.setFocus();
					return;
				}
				if(txtddate.getText().trim().equals(""))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					msgDayErr.setText("Validation Date Error!");
					msgDayErr.setMessage("Please enter a Date.");
					msgDayErr.open();
					txtddate.setFocus();
					
					return;
				}
				if(txtmdate.getText().trim().equals(""))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					msgDayErr.setText("Month Validation Error!");
					msgDayErr.setMessage("Please enter a Month.");
					msgDayErr.open();
					txtmdate.setFocus();
					
					return;
				}
				
				
				if(txtyrdate.getText().trim().equals(""))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					msgDayErr.setText("Year Validation Error!");
					msgDayErr.setMessage("Please enter a Year.");
					msgDayErr.open();
					txtyrdate.setFocus();
					
					return;
				}
				if(!txtddate.getText().trim().equals("") && (Integer.valueOf(txtddate.getText())> 31 || Integer.valueOf(txtddate.getText()) <= 0) )
				{
					MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
					msgdateErr.setText("Date Validation Error!");
					msgdateErr.setMessage("You have entered an invalid Date");
					txtddate.setText("");
					txtddate.setFocus();
					msgdateErr.open();
					return;
				}
				if(!txtmdate.getText().trim().equals("") && (Integer.valueOf(txtmdate.getText())> 12 || Integer.valueOf(txtmdate.getText()) <= 0) )
				{
					MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
					msgdateErr.setText("Month Validation Error!");
					msgdateErr.setMessage("You have entered an invalid month");
					txtmdate.setText("");
					txtmdate.setFocus();
					msgdateErr.open();
					return;
				}
				
				if(!txtyrdate.getText().trim().equals("") && (Integer.valueOf(txtyrdate.getText())> 2100 || Integer.valueOf(txtyrdate.getText()) < 1900) )
				{
					MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
					msgdateErr.setText("Year Validation Error!");
					msgdateErr.setMessage("You have entered an invalid Year");
					txtyrdate.setText("");
					txtyrdate.setFocus();
					msgdateErr.open();
					return;
				}
				
				
				
				try {
					Date startDate = sdf.parse(txtyrdate.getText() + "-" + txtmdate.getText() + "-" + txtddate.getText());
					
					} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_ERROR);
					msg.setText("Error!");
					msg.setMessage("Invalid Date");
					txtddate.setFocus();
					msg.open();
					return;
				}

				
				for(rowcntr=0; rowcntr<  tblVoucherView.getTable().getItemCount(); rowcntr++)
				{		
					AddVoucher vRow = (AddVoucher) tblVoucherView.getElementAt(rowcntr);

					
					try {
						/*if ((CrDrFlags.get(0).toString()=="Dr" && Integer.valueOf(DrAmounts.get(rowcntr).toString())!=0 )||(CrDrFlags.get(1).toString()=="Cr" && Integer.valueOf(CrAmounts.get(rowcntr).toString())!=0)) 
						{*/
							//System.out.print("enter kiyaaaaaa");
							/*if (vRow.getAccountName().trim().equals("") )
									{
								msgflag = true;
								MessageBox msgaccerr = new MessageBox(new Shell(),
										SWT.ERROR | SWT.OK);
								msgaccerr.setMessage("Please select an account");
								msgaccerr.open();
								display.getCurrent().asyncExec(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										 tblVoucherView.getTable().setFocus();
										//accounts.get(rowcntr).setFocus();
									}
								});

								return;

							}
*/						//}
					} catch (NumberFormatException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

				
				if(VoucherEditFlag==1)
				{		
					masterQueryParams.clear();
					detailQueryParams.clear();
					
					if(totalCrAmount != totalDrAmount) 
					{
						MessageBox errMsg = new MessageBox(new Shell(), SWT.ERROR| SWT.OK |SWT.ICON_WARNING);
						errMsg.setText("Warning!");
						errMsg.setMessage("Dr and Cr amounts do not tally, please review your transaction again.");
						errMsg.open();
						txtCrDrAmount.setFocus();
						return;
						
						
					}
					
					if(totalDrAmount == 0 && totalCrAmount == 0)
					{
						MessageBox errMsg = new MessageBox(new Shell(), SWT.ERROR| SWT.OK |  SWT.ICON_WARNING);
						errMsg.setText("Warning!");
						errMsg.setMessage("Transaction with 0 value can't be save");
						errMsg.open();
						tblVoucherView.getTable().setFocus();
						return;
					}
					
					
					try {
						Date voucherDate = sdf.parse(txtyrdate.getText() + "-" + txtmdate.getText() + "-" + txtddate.getText());
						Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
						Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
						
						if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
						{
							MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK |  SWT.ICON_ERROR);
							errMsg.setText("Error!");
							errMsg.setMessage("The Voucher date you entered is not within the Financial Year");
							errMsg.open();
							txtddate.setFocus();
							return;
						}
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.getMessage();
					}
					
					
					
					/*for(int accountvalidation = 0;  accountvalidation < ; accountvalidation ++)
					{
						if( accounts.get(accountvalidation).getSelectionIndex() == -1 )
						{
							MessageBox msgaccerr = new MessageBox(new Shell(), SWT.ERROR |SWT.OK );
							msgaccerr .setMessage("Please select an account for completing the transaction");
							msgaccerr.open();
							accounts.get(accountvalidation).setFocus();
							editAmt=true;
							return;
							
						}
					}*/
					
					//all validations ok so now build master and detail query params.
					masterQueryParams.add(Integer.toString(voucherEditCode));
					masterQueryParams.add(txtyrdate.getText()+"-" + txtmdate.getText()+ "-" + txtddate.getText());
					if(comboselprj.getItemCount() >0 && comboselprj.getSelectionIndex() >= 0)
					{
						masterQueryParams.add(comboselprj.getItem(comboselprj.getSelectionIndex()));
					}
					else
					{
						masterQueryParams.add("No Project");
					}
					masterQueryParams.add(txtnarration.getText());
					for(int detailcounter = 0; detailcounter < tblVoucherView.getTable().getItemCount(); detailcounter ++ )
					{
						AddVoucher vRow = (AddVoucher) tblVoucherView.getElementAt(detailcounter);
						String[] DetailRow = new String[3];
						DetailRow[0]=(vRow.getAccountName());
						
						if(vRow.getDrCr().equals("Dr"))
						{
							DetailRow[1] =Double.toString(vRow.getDrAmount());
							DetailRow[2] =Double.toString(0.00);
						}
						
					if(vRow.getDrCr().equals("Cr"))
					{
						DetailRow[1] =Double.toString(0.00);
						DetailRow[2] = Double.toString(vRow.getCrAmount() );
					}
					detailQueryParams.add(DetailRow);
					}
				
					
					CustomDialog confirm = new CustomDialog(new Shell());
					confirm.SetMessage( "Are you sure ?");
					/*MessageBox	 Confirm = new MessageBox(new Shell(),SWT.NO | SWT.YES | SWT.ICON_QUESTION);
					Confirm.setMessage( "Do you wish to save the changes?");
					*/
					int answer = confirm.open();
					if( answer == SWT.YES)
					{
						if(transactionController.editTransaction(masterQueryParams, detailQueryParams))
						{
							if(ledgerDrilldown == true)
							{
								//call ledger from here.
								Composite grandParent = (Composite) btnConfirm.getParent().getParent();
								
								//reportController.showLedger(grandParent, AccountName, startDate, endDate, PN, narrationFlag, tbdrilldown, psdrilldown, tbType, selectproject);
								btnConfirm.getParent().dispose();
								if(dualledgerflag==false)
								{
								reportController.showLedger(grandParent, AccountName, startDate, endDate, PN, narrationFlag, tbdrilldown, psdrilldown, tbType, selectproject);
								
								}
								if(dualledgerflag==true)
								{
									reportController.showDualLedger(grandParent, AccountName,oldaccname,startDate,oldfromdate, endDate,oldenddate, PN,oldprojectname, narrationFlag,oldnarration, false,false, false,false, "","","", "",true,dualledgerflag);
								}
								//reportController.showLedger(grandParent, AccountName, startDate, endDate, PN, narrationFlag, tbdrilldown, psdrilldown, tbType, selectproject);
								
							
							}
							else
							{
							
							Composite grandParent = (Composite) btnConfirm.getParent().getParent();
							//VoucherTabForm.typeFlag = VoucherTabForm.typeFlag;
						    
						    
						   
							//if()
							if(findvoucher==true)
							{	
								
								FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(grandParent, SWT.NONE,findvoucher,true, search_flag,search_values);
								fdvoucher.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
								
								fdvoucher.combosearchRec.setFocus();
							}
							else
							{
								
								FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(grandParent, SWT.NONE,false, true, search_flag,search_values);
								fdvoucher.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
								fdvoucher.combosearchRec.setFocus();
							}
							 btnConfirm.getParent().dispose();
							/*FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(vtf.tfTransaction, SWT.NONE);
							vtf.tfTransaction.setSelection(1);
							vtf.tifdrecord.setControl(fdvoucher);
							fdvoucher.combosearchRec.setFocus();
							//fdvoucher.combosearchRec.setFocus();
*/							//vtf.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
							}
	
						}
						else
						{
							MessageBox err = new MessageBox(new Shell(), SWT.ERROR | SWT.OK | SWT.ICON_WARNING);
							err.setText("Warning!");
							err.setMessage("The voucher could not be updated.");
							err.open();
							return;
						}
					}
					else
					{
						txtddate.setFocus();
						txtddate.setSelection(0,2);
					}
				}	
				
				if(VoucherEditFlag==2)
				{
					masterQueryParams.clear();
					detailQueryParams.clear();
					if(totalCrAmount != totalDrAmount || totalDrAmount	 == 0 || totalCrAmount == 0) 
					{
						MessageBox errMsg = new MessageBox(new Shell(), SWT.ERROR| SWT.OK | SWT.ICON_WARNING);
						errMsg.setText("Warning!");
						errMsg.setMessage("Dr and Cr amounts do not tally, please review your transaction again.");
						errMsg.open();
						tblVoucherView.getTable().setFocus();	
						return;
						
					}			
					
					try {
						Date voucherDate = sdf.parse(txtyrdate.getText() + "-" + txtmdate.getText() + "-" + txtddate.getText());
						Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
						Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
						
						if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
						{
							MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK |SWT.ICON_ERROR );
							errMsg.setText("Error!");
							errMsg.setMessage("The Voucher date you entered is not within the Financial Year.");
							errMsg.open();
							txtddate.setFocus();
							txtddate.setSelection(0,2);
							return;
						}
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.getMessage();
					}
					
					
				//all validations ok so now build master and detail query params.
					masterQueryParams.add(txtvoucherno.getText());
					masterQueryParams.add(sdf.format(new Date()));
					masterQueryParams.add(txtyrdate.getText()+"-" + txtmdate.getText()+ "-" + txtddate.getText());
					masterQueryParams.add(typeFlag);
					if(comboselprj.getItemCount() > 0 && comboselprj.getSelectionIndex() > 0)
					{
						masterQueryParams.add(comboselprj.getItem(comboselprj.getSelectionIndex()));
					}
					else
					{
						masterQueryParams.add("No Project");
					}
					masterQueryParams.add(txtnarration.getText());
					masterQueryParams.add("");
					//masterQueryParams.add(txtPurchaseyrdate.getText()+"-"+txtPurchasemdate.getText()+"-"+txtPurchaseddate.getText());
					//masterQueryParams.add(null); 
					masterQueryParams.add(txtyrdate.getText()+"-" + txtmdate.getText()+ "-" + txtddate.getText());
					masterQueryParams.add("0.00");
					for(int detailcounter = 0; detailcounter < tblVoucherView.getTable().getItemCount(); detailcounter ++ )
					{
						AddVoucher vRow = (AddVoucher) tblVoucherView.getElementAt(detailcounter);
						String[] DetailRow = new String[3];
						//DetailRow[1] = vRow.getAccountName();
						DetailRow[0] = vRow.getDrCr();
						DetailRow[1] = vRow.getAccountName();
						if(vRow.getDrCr().equals("Dr"))
						{
							
							DetailRow[2] =Double.toString(vRow.getDrAmount());
							//DetailRow[2] =Double.toString(0.0);

						}
						
					if(vRow.getDrCr().equals("Cr"))
					{
						//DetailRow[1] =Double.toString(0.0);

						DetailRow[2] = Double.toString(vRow.getCrAmount());
					}
					detailQueryParams.add(DetailRow);
					}
									
					
					CustomDialog confirm = new CustomDialog(new Shell());
					confirm.SetMessage( "Do you wish to save ?");

					int answer = confirm.open();
					if( answer == SWT.YES)
					{
						if(transactionController.setTransaction(masterQueryParams, detailQueryParams) )
						{
							
							if(ledgerDrilldown == true)
							{
								//call ledger from here.
								Composite grandParent = (Composite) btnConfirm.getParent().getParent();
							
							
								//reportController.showLedger(grandParent, AccountName, startDate, endDate, PN, narrationFlag, tbdrilldown, psdrilldown, tbType, selectproject);
								btnConfirm.getParent().dispose();
								if(dualledgerflag==false)
								{
								reportController.showLedger(grandParent, AccountName, startDate, endDate, PN, narrationFlag, tbdrilldown, psdrilldown, tbType, selectproject);
								}
								if(dualledgerflag==true)
								{
									reportController.showDualLedger(grandParent, AccountName,oldaccname,startDate,oldfromdate, endDate,oldenddate, PN,oldprojectname, narrationFlag,oldnarration, false,false, false,false, "","","", "",true,dualledgerflag);
								}
								//reportController.showLedger(grandParent, AccountName, startDate, endDate, PN, narrationFlag, tbdrilldown, psdrilldown, tbType, selectproject);
								
							}
							else
							{
							
							Composite grandParent = (Composite) btnConfirm.getParent().getParent();
							//VoucherTabForm.typeFlag = VoucherTabForm.typeFlag;
						    //VoucherTabForm vtf = new VoucherTabForm(grandParent,SWT.NONE );
						    
							btnConfirm.getParent().dispose();
							if(findvoucher==true)
							{
								
								FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(grandParent, SWT.NONE,findvoucher,true, search_flag,search_values );
								fdvoucher.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);								fdvoucher.combosearchRec.setFocus();
							}
							else
							{
								
								FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(grandParent, SWT.NONE,false,true, search_flag,search_values);
								fdvoucher.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
								fdvoucher.combosearchRec.setFocus();
							}
							/*FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(vtf.tfTransaction, SWT.NONE);
							vtf.tfTransaction.setSelection(1);
							vtf.tifdrecord.setControl(fdvoucher);
							fdvoucher.combosearchRec.setFocus();
							//fdvoucher.combosearchRec.setFocus();
							vtf.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);*/
							}
						}
						else
						{
							MessageBox err = new MessageBox(new Shell(), SWT.ERROR | SWT.OK | SWT.ICON_ERROR);
							err.setText("Error!");
							err.setMessage("The Voucher could not be Cloned. ");
							err.open();
							return;
						}
					}
					else
					{
						txtvoucherno.setFocus();
						txtvoucherno.setSelection(0,4);
					}
				}
					
				
			}
					
		});

		btnAddAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Shell shell = new Shell();
				AddAccountPopup  dialog = new AddAccountPopup(shell);
				System.out.println(dialog.open());
				if(AddAccountPopup.cancelflag.equals(true))
				{
					shell.dispose();
					cmbAccounts.setFocus();
	
					//return;
				}
				else
				{
					int curindex = cmbAccounts.getSelectionIndex();
					if(typeFlag.equals("Contra"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
						cmbAccounts.select(curindex);
						cmbAccounts.setFocus();
					}
					if(typeFlag.equals("Journal"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
						cmbAccounts.select(curindex);
						cmbAccounts.setFocus();
					}
					if (typeFlag.equals("Payment")) 
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment(crdrcombofocus)));
						cmbAccounts.select(curindex);
						cmbAccounts.setFocus();
					}
					if (typeFlag.equals("Receipt")) 
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt(crdrcombofocus)));
						cmbAccounts.select(curindex);
						cmbAccounts.setFocus();
					}
					if (typeFlag.equals("Sales")) 
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales(crdrcombofocus)));
						cmbAccounts.select(curindex);
						cmbAccounts.setFocus();
					}
					if (typeFlag.equals("Purchase")) 
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase(crdrcombofocus)));
						cmbAccounts.select(curindex);
						cmbAccounts.setFocus();
					}
					if (typeFlag.equals("Credit Note")) 
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote(crdrcombofocus)));
						cmbAccounts.select(curindex);
						cmbAccounts.setFocus();
					}
					if (typeFlag.equals("Debit Note")) 
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote(crdrcombofocus)));
						cmbAccounts.select(curindex);
						cmbAccounts.setFocus();
					}
					if (typeFlag.equals("Sales Return")) 
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSalesReturn(crdrcombofocus)));
						cmbAccounts.select(curindex);
						cmbAccounts.setFocus();
					}
					if (typeFlag.equals("Purchase Return")) 
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchaseReturn(crdrcombofocus)));
						cmbAccounts.select(curindex);
						cmbAccounts.setFocus();
					}
				}
			}
		});
		


		
		btnBack.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
					if(ledgerDrilldown==true)
					{
						//call ledger from here.
						
						Composite grandParent = (Composite) btnBack.getParent().getParent();
						btnBack.getParent().dispose();
						if(dualledgerflag==false)
						{
						reportController.showLedger(grandParent, AccountName, startDate, endDate, PN, narrationFlag, tbdrilldown, psdrilldown, tbType, selectproject);
						}
						if(dualledgerflag==true)
						{
							reportController.showDualLedger(grandParent, AccountName,oldaccname,startDate,oldfromdate, endDate,oldenddate, PN,oldprojectname, narrationFlag,oldnarration, false,false, false,false, "","","", "",true,true);
						}
					
					}
					else
					{
						Composite grandParent = (Composite) btnBack.getParent().getParent();
						//VoucherTabForm.typeFlag = VoucherTabForm.typeFlag;
					    
					    
						
						
						if(findvoucher==true)
						{
							FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(grandParent, SWT.NONE,findvoucher,true, search_flag,search_values);
							fdvoucher.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
						}
						else
						{
							FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(grandParent, SWT.NONE,false,true, search_flag,search_values);
							fdvoucher.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
							fdvoucher.combosearchRec.setFocus();
						}
						btnBack.getParent().dispose();
						/*FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(vtf.tfTransaction, SWT.NONE);
						vtf.tfTransaction.setSelection(1);
						vtf.tifdrecord.setControl(fdvoucher);
						fdvoucher.combosearchRec.setFocus();
						//fdvoucher.combosearchRec.setFocus();
						vtf.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);*/
					}
				}
				
					
			
		});
		
		
		tblVoucherView.getControl().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (!rowfocusflag)
				{
					tblIndex=tblVoucherView.getTable().getSelectionIndex();
					tblVoucherView.getTable().setSelection(-1);
					
				}
				
				/*tblVoucherView.getControl().setBackground(Background);
				tblVoucherView.getControl().setForeground(Foreground);*/
			};
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if (!rowfocusflag)
				{
					tblVoucherView.getTable().setSelection(tblIndex);
					
				}
				
				focusflag = false;
			}
		});
		tblVoucherView.getControl().addKeyListener(new KeyAdapter() {
			
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				
				if(arg0.keyCode == SWT.DEL)
				{
						
					if (tblVoucherView.getTable().getSelectionIndex()>=0) {
						IStructuredSelection VoucherRow = (IStructuredSelection) tblVoucherView
								.getSelection();
						AddVoucher vr = (AddVoucher) VoucherRow
								.getFirstElement();
						SelectedAccounts.remove(vr.getAccountName());
						tblVoucherView.remove(vr);
						totalDrAmount = 0;
						totalCrAmount = 0;
						for (int RowCounter = 0; RowCounter < tblVoucherView
								.getTable().getItemCount(); RowCounter++) {
							AddVoucher VoucherRowForTotal = (AddVoucher) tblVoucherView
									.getElementAt(RowCounter);
							totalDrAmount = totalDrAmount
									+ VoucherRowForTotal.getDrAmount();
							totalCrAmount = totalCrAmount
									+ VoucherRowForTotal.getCrAmount();
						}
						totalDr.setText(nf.format(totalDrAmount));
						totalCr.setText(nf.format(totalCrAmount));
						if (totalDrAmount != totalCrAmount) {
							btnConfirm.setEnabled(false);
						}
						tblVoucherView.getTable().select(tblVoucherView.getTable().getSelectionIndex()+1);
					}
					if (tblVoucherView.getTable().getItemCount()==0) {
						grpEntry.setVisible(true);
						cmbCr_Dr.setVisible(true);
						lblAccounts.setVisible(true);
						cmbAccounts.setVisible(true);
						lblCrDrAmount.setVisible(true);
						txtCrDrAmount.setVisible(true);
						cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
						cmbCr_Dr.select(0);
						cmbCr_Dr.setFocus();
					}
					return;
				}
		
				
				if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
				{
					rowfocusflag = true;
				IStructuredSelection VoucherRow= (IStructuredSelection) tblVoucherView.getSelection();
				AddVoucher vr = (AddVoucher) VoucherRow.getFirstElement();
				try {
					if(vr.getAccountName().equals(""))
					{
						return;
					}
				} catch (NullPointerException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					cmbCr_Dr.removeAll();
					cmbCr_Dr.add("Dr");
					cmbCr_Dr.select(0);
					return;
				}
				

				grpEntry.setVisible(true);
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						btnConfirm.setEnabled(false);
					}
				});
				//lblCrDr.setVisible(true);
				cmbCr_Dr.setVisible(true);
				lblAccounts.setVisible(true);
				cmbAccounts.setVisible(true);
				lblCrDrAmount.setVisible(true);
				txtCrDrAmount.setVisible(true);
				cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
				tblVoucherView.getTable().setEnabled(false);
				try {
					selectedIndex = tblVoucherView.getTable().getSelectionIndex();
					alterFlag = true;
					//tblVoucherView.getTable().getItem(0).setChecked(true);
					if(vr.getDrCr().equals("Dr") )
					{
						totalDrAmount = totalDrAmount - vr.getDrAmount();
						totalDr.setText(nf.format(totalDrAmount));
						if (selectedIndex ==0) {
							cmbCr_Dr.removeAll();
							cmbCr_Dr.add("Dr");
							cmbCr_Dr.select(0);
							cmbCr_Dr.setEnabled(false);
						}
						else
						{
							cmbCr_Dr.setEnabled(true);
							cmbCr_Dr.select(0);
						}
					
					}
					if(vr.getDrCr().equals("Cr") )
					{
						totalCrAmount = totalCrAmount - vr.getCrAmount();
						totalCr.setText(nf.format(totalCrAmount));
						if (selectedIndex ==1) {
							cmbCr_Dr.removeAll();
							cmbCr_Dr.add("Cr");
							cmbCr_Dr.select(0);
							cmbCr_Dr.setEnabled(false);
						}
						else
						{
							cmbCr_Dr.setEnabled(true);
							cmbCr_Dr.select(1);
						}
					
					}

					
					tblVoucherView.remove(vr);
					tblVoucherView.insert(new AddVoucher("", "", 0.00, 0.00) ,selectedIndex );
					
					if(vr.getDrCr().equals("Dr"))
					{
						/*MessageBox msgdrcr = new MessageBox(new Shell(),SWT.OK);
						msgdrcr.setMessage("this row is of Dr type");
						msgdrcr.open();
						*/
						Display.getCurrent().asyncExec(new Runnable(){
							public void run()
							{
								cmbAccounts.setFocus();
							}
					});
				
						//cmbCr_Dr.select(0);
						txtCrDrAmount.setText(nf.format(Double.valueOf(vr.getDrAmount())));
						if(typeFlag.equals("Contra"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
							
							
						}
						if(typeFlag.equals("Payment"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));

							
						}
						if(typeFlag.equals("Journal"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
							
							
						}
						if(typeFlag.equals("Receipt"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));
							
							
						}
						if(typeFlag.equals("Sales"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

							
						}
						if(typeFlag.equals("Purchase"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
							
							
						}
						if(typeFlag.equals("Credit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));

							
						}
						if(typeFlag.equals("Debit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));
							
							
						}
						if(typeFlag.equals("Sales Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

							
						}
						if(typeFlag.equals("Purchase Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
							

						}
						for(int i = 0; i < cmbAccounts.getItemCount(); i++ )
						{
							if(cmbAccounts.getItem(i).equalsIgnoreCase(vr.getAccountName()))
							{
								//arg0.doit= false;
								cmbAccounts.select(i);
								cmbAccounts.notifyListeners(SWT.Selection ,new Event()  );
								accflag=true;
								break;
							}
						}
						
						
							if (!accflag) 
							{
								cmbAccounts.add(vr.getAccountName());
								cmbAccounts.select(cmbAccounts.getItemCount()-1);
								
							}
						accflag=false;
						
					MessageBox msgacc = new MessageBox(new Shell(),SWT.OK);
					msgacc.setMessage(vr.getAccountName());
					//msgacc.open();
					
					}
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					cmbCr_Dr.removeAll();
					cmbCr_Dr.add("Dr");
					cmbCr_Dr.select(0);
					return;
				}
				for(int i = 0; i < cmbAccounts.getItems().length; i++)
				{
					if(cmbAccounts.getItem(i).equals(vr.getAccountName()) )
					{
						cmbAccounts.select(i);
					}
				}
					
				if(vr.getDrCr().equals("Cr"))
				{
					Display.getCurrent().asyncExec(new Runnable(){
						public void run()
						{
							cmbAccounts.setFocus();
						}
				});
					//cmbCr_Dr.select(1);
					txtCrDrAmount.setText(nf.format(Double.valueOf(vr.getCrAmount())));
					if(typeFlag.equals("Contra"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
						
						
					}
					if(typeFlag.equals("Payment"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));

						
					}
					if(typeFlag.equals("Journal"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
						
						
					}
					if(typeFlag.equals("Receipt"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
						
						
					}
					if(typeFlag.equals("Sales"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

						
					}
					if(typeFlag.equals("Purchase"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
						
						
					}
					if(typeFlag.equals("Credit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));

						
					}
					if(typeFlag.equals("Debit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
						
						
					}
					if(typeFlag.equals("Sales Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

						
					}
					if(typeFlag.equals("Purchase Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
						

					}
					for(int i = 0; i < cmbAccounts.getItemCount(); i++ )
					{
						if(cmbAccounts.getItem(i).equalsIgnoreCase(vr.getAccountName()))
						{
							//arg0.doit= false;
							cmbAccounts.select(i);
							cmbAccounts.notifyListeners(SWT.Selection ,new Event()  );
							accflag=true;
							break;
						}
					}
					
					
						if (!accflag) 
						{
							cmbAccounts.add(vr.getAccountName());
							cmbAccounts.select(cmbAccounts.getItemCount()-1);
							
						}
					accflag=false;
				}
				
				}
				

				
			}
		});
		
		tblVoucherView.getControl().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
		
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				//if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
			{
				rowfocusflag = true;
		        IStructuredSelection VoucherRow= (IStructuredSelection) tblVoucherView.getSelection();
				AddVoucher vr = (AddVoucher) VoucherRow.getFirstElement();
				try {
					if(vr.getAccountName().equals(""))
					{
						return;
					}
				} catch (NullPointerException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					cmbCr_Dr.removeAll();
					cmbCr_Dr.add("Dr");
					cmbCr_Dr.select(0);
					return;
				}
						
				grpEntry.setVisible(true);
				Display.getCurrent().asyncExec(new Runnable() {
									
					@Override
					public void run() {
						// TODO Auto-generated method stub
						btnConfirm.setEnabled(false);
					}
				});
				//lblCrDr.setVisible(true);
				cmbCr_Dr.setVisible(true);
				lblAccounts.setVisible(true);
				cmbAccounts.setVisible(true);
				lblCrDrAmount.setVisible(true);
				txtCrDrAmount.setVisible(true);
				cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
				tblVoucherView.getTable().setEnabled(false);
				try {
					selectedIndex = tblVoucherView.getTable().getSelectionIndex();
					alterFlag = true;
					//tblVoucherView.getTable().getItem(0).setChecked(true);
					if(vr.getDrCr().equals("Dr") )
					{
						totalDrAmount = totalDrAmount - vr.getDrAmount();
						totalDr.setText(nf.format(totalDrAmount));
						if (selectedIndex ==0) {
							cmbCr_Dr.removeAll();
							cmbCr_Dr.add("Dr");
							cmbCr_Dr.select(0);
							cmbCr_Dr.setEnabled(false);
						}
						else
						{	cmbCr_Dr.setEnabled(true);
							cmbCr_Dr.select(0);
						}
					}
					if(vr.getDrCr().equals("Cr") )
					{
						totalCrAmount = totalCrAmount - vr.getCrAmount();
						totalCr.setText(nf.format(totalCrAmount));
						if (selectedIndex ==1) {
							cmbCr_Dr.removeAll();
							cmbCr_Dr.add("Cr");
							cmbCr_Dr.select(0);
							cmbCr_Dr.setEnabled(false);
						}
						else
						{
							cmbCr_Dr.setEnabled(true);
							cmbCr_Dr.select(1);
						}
					}

					
					tblVoucherView.remove(vr);
					tblVoucherView.insert(new AddVoucher("", "", 0.00, 0.00) ,selectedIndex );
					cmbAccounts.setFocus();
					if(vr.getDrCr().equals("Dr"))
					{
						/*MessageBox msgdrcr = new MessageBox(new Shell(),SWT.OK);
						msgdrcr.setMessage("this row is of Dr type");
						msgdrcr.open();
						*/
						cmbAccounts.setFocus();
						//cmbCr_Dr.select(0);
						txtCrDrAmount.setText(nf.format(Double.valueOf(vr.getDrAmount())));
						if(typeFlag.equals("Contra"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
							
							
						}
						if(typeFlag.equals("Payment"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));

							
						}
						if(typeFlag.equals("Journal"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
							
							
						}
						if(typeFlag.equals("Receipt"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));
							
							
						}
						if(typeFlag.equals("Sales"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

							
						}
						if(typeFlag.equals("Purchase"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
							
							
						}
						if(typeFlag.equals("Credit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));

							
						}
						if(typeFlag.equals("Debit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));
							
							
						}
						if(typeFlag.equals("Sales Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

							
						}
						if(typeFlag.equals("Purchase Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
							

						}
						
					
					for(int i = 0; i < cmbAccounts.getItemCount(); i++ )
					{
						if(cmbAccounts.getItem(i).equalsIgnoreCase(vr.getAccountName()))
						{
							//arg0.doit= false;
							cmbAccounts.select(i);
							cmbAccounts.notifyListeners(SWT.Selection ,new Event()  );
							accflag=true;
							break;
						}
					}
					
					
						if (!accflag) 
						{
							cmbAccounts.add(vr.getAccountName());
							cmbAccounts.select(cmbAccounts.getItemCount()-1);
							
						}
					accflag=false;
					}
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					cmbCr_Dr.removeAll();
					cmbCr_Dr.add("Dr");
					cmbCr_Dr.select(0);
					return;
				}
				for(int i = 0; i < cmbAccounts.getItems().length; i++)
				{
					if(cmbAccounts.getItem(i).equals(vr.getAccountName()) )
					{
						cmbAccounts.select(i);
					}
				}
					
				if(vr.getDrCr().equals("Cr"))
				{
					cmbAccounts.setFocus();
					//cmbCr_Dr.select(1);
					txtCrDrAmount.setText(nf.format(Double.valueOf(vr.getCrAmount())));
					if(typeFlag.equals("Contra"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
						
						
					}
					if(typeFlag.equals("Payment"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));

						
					}
					if(typeFlag.equals("Journal"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
						
						
					}
					if(typeFlag.equals("Receipt"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
						
						
					}
					if(typeFlag.equals("Sales"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

						
					}
					if(typeFlag.equals("Purchase"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
						
						
					}
					if(typeFlag.equals("Credit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));

						
					}
					if(typeFlag.equals("Debit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
						
						
					}
					if(typeFlag.equals("Sales Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

						
					}
					if(typeFlag.equals("Purchase Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
						

					}
					for(int i = 0; i < cmbAccounts.getItemCount(); i++ )
					{
						if(cmbAccounts.getItem(i).equalsIgnoreCase(vr.getAccountName()))
						{
							//arg0.doit= false;
							cmbAccounts.select(i);
							cmbAccounts.notifyListeners(SWT.Selection ,new Event()  );
							accflag=true;
							break;
						}
					}
					
					
						if (!accflag) 
						{
							cmbAccounts.add(vr.getAccountName());
							cmbAccounts.select(cmbAccounts.getItemCount()-1);
							
						}
					accflag=false;
				
				}
				for(int i = 0; i < cmbAccounts.getItems().length; i++)
				{
					if(cmbAccounts.getItem(i).equals(vr.getAccountName()) )
					{
						cmbAccounts.select(i);
					}
				}

				}
				

			
			}
		});
		
		cmbCr_Dr.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode ==  SWT.CR ||arg0.keyCode == SWT.KEYPAD_CR)
				{
					cmbAccounts.setFocus();
				}
			}
		});
		cmbCr_Dr.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Dr"))
						{
					if(typeFlag.equals("Contra"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Payment"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Journal"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Receipt"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Credit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Debit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
						
						cmbAccounts.select(0);

					}

					
						}
					if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Cr"))
							{
						if(typeFlag.equals("Contra"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
							
							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Payment"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));

							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Journal"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
							
							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Receipt"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
							
							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Sales"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Purchase"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
							
							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Credit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));

							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Debit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
							
							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Sales Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Purchase Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
							
							cmbAccounts.select(0);

						}

						
							}
			}
			});
		txtCrDrAmount.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
				}
		if(arg0.keyCode == 46)
		{
			return;
		}
		if(arg0.keyCode == 45||arg0.keyCode == 62)
		{
			  arg0.doit = false;
			
		}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }
	        

				
			}
		});
		
		txtCrDrAmount.addTraverseListener(new TraverseListener() {
			
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.detail== SWT.TRAVERSE_TAB_PREVIOUS)
				{
					cmbAccounts.setFocus();
					return;
				}
				if(arg0.detail== SWT.TRAVERSE_MNEMONIC)
				{
					arg0.doit = false;
					return;
				}
								if(arg0.detail == SWT.TRAVERSE_TAB_NEXT|| arg0.detail == SWT.TRAVERSE_RETURN)
				{
				//	focusflag = false;
									cmbCr_Dr.setEnabled(true);
									
									if(txtCrDrAmount.getText().trim().equals("")  )
									{
										MessageBox msgValueError = new MessageBox(new Shell(), SWT.OK | SWT.ICON_ERROR );
										msgValueError.setText("Error!");
										msgValueError.setMessage("Please enter a proper Amount.");
										msgValueError.open();
										arg0.doit = false;
										return;
											}
									
									//tblVoucherView.getTable().setEnabled(true);

									}
								if(arg0.keyCode == SWT.ARROW_UP)
								{
									cmbAccounts.setFocus();
									arg0.doit = false;
									return;
								}
								if(arg0.keyCode== SWT.ARROW_LEFT|| arg0.keyCode == SWT.ARROW_RIGHT)
								{
									arg0.doit = false;
									return;
								}
					arg0.doit = false;
					doTally();
					if (Double.valueOf(totalCr.getText().trim()).equals(Double.valueOf(totalDr.getText().trim())))
					{
						
							cmbCr_Dr.removeAll();
							cmbAccounts.removeAll();
							txtCrDrAmount.setText("");
							//lblCrDr.setVisible(false);
							cmbCr_Dr.setVisible(false);
							lblAccounts.setVisible(false);
							cmbAccounts.setVisible(false);
							tblVoucherView.getTable().setEnabled(true);
							lblCrDrAmount.setVisible(false);
							txtCrDrAmount.setVisible(false);
							grpEntry.setVisible(false);
							btnConfirm.setEnabled(true);
							if(comboselprj.getVisible())
							{
								comboselprj.setFocus();
							}
							else
							{
								txtnarration.setFocus();
							}
							return;
					}
						
					
					TableItem[] items = tblVoucherView.getTable().getItems();
					for (int rowid=0; rowid<items.length; rowid++){
					    if (rowid%2==0) 
					    {
					    	items[rowid].setBackground(tableBackground);
					    }
					    else {
					    	items[rowid].setBackground(tableForeground);
					    }
					}
					
				
				
			}
		});
		txtyrdate.addFocusListener(new FocusAdapter() {
/*			@Override
			
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				txtyrdate.clearSelection();
				txtyrdate.setBackground(FocusBackground);
				txtyrdate.setForeground(FocusForeground);
			}
*/			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
/*				txtyrdate.setBackground(Background);
				txtyrdate.setForeground(Foreground);
*/
				tblVoucherView.getTable().setFocus();

			}
		});
		txtyrdate.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode== SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
				{
					txtyrdate.notifyListeners(SWT.FocusOut,new Event() );
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{	
					txtmdate.setFocus();
				}

				
			}
		});
txtddate.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
					
				
				if(arg0.keyCode==46)
				{
					return;
				}
				if(arg0.keyCode == 45||arg0.keyCode == 62)
				{
					  arg0.doit = false;
					
				}
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	                return;
	        }
				if(arg0.keyCode  == 46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character))
	        {
	            arg0.doit = false;  // disallow the action
	        }

				

			}
		});
		
		txtmdate.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode==46)
				{
					return;
				}
				if(arg0.keyCode == 45||arg0.keyCode == 62)
				{
					  arg0.doit = false;
					
				}
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	                return;
	        }
				if(arg0.keyCode  == 46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character))
	        {
	            arg0.doit = false;  // disallow the action
	        }

				

			}
		});


		txtyrdate.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode==46)
				{
					return;
				}
				if(arg0.keyCode==45||arg0.keyCode == 62)
				{
					arg0.doit= false;
				}
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	                return;
	        }
				if(arg0.keyCode  == 46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character))
	        {
	            arg0.doit = false;  // disallow the action
	        }

				

			}
		});
		txtvoucherno.addFocusListener(new FocusAdapter() {
			@Override
			
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
			/*	txtvoucherno.setBackground(FocusBackground);
				txtvoucherno.setForeground(FocusForeground);
*/
				if(totalCrAmount==totalDrAmount && totalCrAmount!=0.00 && totalDrAmount!=0.00 )
				{
					btnConfirm.setEnabled(true);
				}
				else if(totalCrAmount!=totalDrAmount || totalCrAmount==0.00 || totalDrAmount==0.00)
				{
					btnConfirm.setEnabled(false);
				}
				if(! lblsavemsg.getText().equals(""))
				{
					Display.getCurrent().asyncExec(new Runnable(){
						public void run()
						{
							long now = System.currentTimeMillis();
							long lblTimeOUt = 0;
							while(lblTimeOUt < (now + 2000))
							{
								lblTimeOUt = System.currentTimeMillis();
							}
		/*					MessageBox endtimer = new MessageBox(new Shell(),SWT.OK);
							endtimer.setText("time up! now will empty the label");
							endtimer.open();
		*/
							lblsavemsg.setText("");

						}
				});
/*					MessageBox msgtime = new MessageBox(new Shell(),SWT.OK);
					msgtime.setMessage("lable is not empty will run timmer");
					msgtime.open();
*/



				}
			}
			/*public void focusLost(FocusEvent arg0) {
				//txtvoucherno.clearSelection();
				txtvoucherno.setBackground(Background);
				txtvoucherno.setForeground(Foreground);
			};
		*/
			
		});
		txtvoucherno.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0); 65 to 90 caps , 97 122
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||
						arg0.keyCode == SWT.KEYPAD_CR||arg0.keyCode == SWT.ARROW_LEFT||arg0.keyCode == SWT.ARROW_RIGHT)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
				if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
				{
					txtddate.setFocus();
				}
			}
			});
			
			txtddate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
					{
						txtmdate.setFocus();
						
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{	
						txtvoucherno.setFocus();
					}
				}
				
			});
			
			txtmdate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					
					if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
					{
						txtyrdate.setFocus();
						
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{	
						txtddate.setFocus();
					}
				}
			});
			
			
			
			comboselprj.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
				//	super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
					{
						txtnarration.setFocus();
						
					}
					if(arg0.keyCode == SWT.ARROW_UP && comboselprj.getSelectionIndex() == 0)
					{
						tblVoucherView.getTable().setFocus();
					}
					
					long now = System.currentTimeMillis();
					if (now > searchTexttimeout){
				         searchText = "";
				      }
					searchText += Character.toLowerCase(arg0.character);
					searchTexttimeout = now + 1000;					
					for(int i = 0; i < comboselprj.getItemCount(); i++ )
					{
						if(comboselprj.getItem(i).toLowerCase().startsWith(searchText ) ){
							//arg0.doit= false;
							comboselprj.select(i);
							comboselprj.notifyListeners(SWT.Selection ,new Event()  );
							break;
						}
					}
			
										
				}
			});
			
			cmbCr_Dr.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
					cmbCr_Dr.setBackground(FocusBackground);
					cmbCr_Dr.setForeground(FocusForeground);	
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					cmbCr_Dr.setBackground( Background);
					cmbCr_Dr.setForeground( Foreground);
				}
			});
			txtnarration.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					txtnarration.selectAll();
				}
				
			});
			
			btnAddAccount.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
					btnAddAccount.setBackground(FocusBackground);
					btnAddAccount.setForeground(BtnFocusForeground);	
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					btnAddAccount.setBackground( Background);
					btnAddAccount.setForeground( Foreground);
				}
			});
			btnConfirm.addFocusListener(new FocusAdapter() {
				 @Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
					 btnConfirm.setBackground(FocusBackground);
					 btnConfirm.setForeground(BtnFocusForeground);	
						 
				}
				 @Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					 btnConfirm.setBackground( Background);
					 btnConfirm.setForeground( Foreground);
				}
			});
			
			
		
			txtnarration.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method{} stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.TAB||arg0.keyCode == SWT.KEYPAD_CR )
					{
						arg0.doit=false;
					}
					
					if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.TAB||arg0.keyCode == SWT.KEYPAD_CR )
					{   btnConfirm.setVisible(true);
					
					
						if(btnConfirm.isEnabled())
						{
							btnConfirm.setFocus();
							btnConfirm.notifyListeners(SWT.Selection, new Event());
						}
						
						
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						if(comboselprj.isVisible())
						{
							comboselprj.setFocus();
						}
						else
						{
							tblVoucherView.getTable().setFocus();
						}
						
					}
				}
			});
			
			
			btnConfirm.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
				//	super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtnarration.setFocus();
						
					}
					
				}		
			});
			
			txtmdate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||
							arg0.keyCode == SWT.KEYPAD_CR||arg0.keyCode == SWT.ARROW_LEFT||arg0.keyCode == SWT.ARROW_RIGHT)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtyrdate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||
							arg0.keyCode == SWT.KEYPAD_CR||arg0.keyCode == SWT.ARROW_LEFT||arg0.keyCode == SWT.ARROW_RIGHT)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtddate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||
							arg0.keyCode == SWT.KEYPAD_CR||arg0.keyCode == SWT.ARROW_LEFT||arg0.keyCode == SWT.ARROW_RIGHT)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
					
				}
			});
			
			txtddate.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0)
				{
					/*txtddate.clearSelection();
					txtddate.setBackground(FocusBackground);
					txtddate.setForeground(FocusForeground);
					*/
					//verifyFlag = true;
					if(totalCrAmount==totalDrAmount && totalCrAmount!=0.00 && totalDrAmount!=0.00 )
					{
						btnConfirm.setVisible(true);
					}
					else if(totalCrAmount!=totalDrAmount || totalCrAmount==0.00 || totalDrAmount==0.00)
					{
						//btnConfirm.setVisible(false);
					}
					
				}
				
				
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					//verifyFlag = false;
//					txtddate.setBackground(Background);
//					txtddate.setForeground(Foreground);
//					
					
					
					if(!txtddate.getText().equals("") && Integer.valueOf ( txtddate.getText())<10 && txtddate.getText().length()< txtddate.getTextLimit())
					{
						txtddate.setText("0"+ txtddate.getText());
						//txtFromDtMonth.setFocus();
						return;
						
						
						
					}
					/*if(txtFromDtDay.getText().length()==2)
					   {
						   txtFromDtMonth.setFocus();
					   }*/
				}
			});
			
			txtmdate.addFocusListener(new FocusAdapter() {
				public void focusLost(FocusEvent arg0) {
					try {
						
						if(!txtmdate.getText().equals("") && Integer.valueOf ( txtmdate.getText())<10 && txtmdate.getText().length()< txtmdate.getTextLimit())
						{
							txtmdate.setText("0"+ txtmdate.getText());
							//txtFromDtMonth.setFocus();
							return;
							
							
							
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				};
			});
			
			cmbAccounts.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
					cmbAccounts.setListVisible(true);
					cmbAccounts.setBackground(FocusBackground);
					cmbAccounts.setForeground(FocusForeground);

					crdrcombofocus = cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex());
					
					
					focusflag = true;
					if(cmbAccounts.getSelectionIndex()>= 1  )
					{
						if(SelectedAccounts.contains(cmbAccounts.getItem(cmbAccounts.getSelectionIndex())))
						{
							SelectedAccounts.remove(cmbAccounts.getItem(cmbAccounts.getSelectionIndex()));
						}
					}
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					cmbAccounts.setBackground( Background);
					cmbAccounts.setForeground( Foreground);
					
					focusflag = false;
					

				}
				
			});
			
			txtCrDrAmount.addFocusListener(new FocusAdapter() {
			
				public void focusGained(FocusEvent arg0) {
					 
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
					//txtCrDrAmount.clearSelection();
					txtCrDrAmount.setBackground(FocusBackground);
					txtCrDrAmount.setForeground(FocusForeground);
					txtCrDrAmount.setSelection(0, txtCrDrAmount.getText().length() );
					//till here.
				}
				
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
				//	super.focusLost(arg0);
					txtCrDrAmount.setBackground( Background);
					txtCrDrAmount.setForeground( Foreground);
				}
				
			
			});
			cmbAccounts.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(((arg0.stateMask & SWT.ALT) == SWT.ALT) && arg0.character == 'o')
					{
						cmbAccounts.setListVisible(false);
						btnAddAccount.notifyListeners(SWT.Selection, new Event());
						return;
					}
					
					if(arg0.keyCode== SWT.CR|| arg0.keyCode == SWT.KEYPAD_CR )
					{
						cmbAccounts.setListVisible(false);
						txtCrDrAmount.setFocus();
					}
					
					long now = System.currentTimeMillis();
					if (now > searchTexttimeout){
				         searchText = "";
				      }
					searchText += Character.toLowerCase(arg0.character);
					searchTexttimeout = now + 1200;					
					for(int i = 0; i < cmbAccounts.getItemCount(); i++ )
					{
						if(cmbAccounts.getItem(i).toLowerCase().startsWith(searchText ) ){
							//arg0.doit= false;
							cmbAccounts.select(i);
							cmbAccounts.notifyListeners(SWT.Selection ,new Event()  );
							break;
				}}
				}
			});
				
	}
	private String[] getFilteredAccountList(String[] OrigList) {
		ArrayList<String> filterAccounts = new ArrayList<String>();
		
		for(int filtercounter = 0; filtercounter < OrigList.length; filtercounter++ )
		{
			if(!SelectedAccounts.contains(OrigList[filtercounter]))
			{
				filterAccounts.add(OrigList[filtercounter]);
				
			}
		}
		
		
		
		for (int i = 0; i < tblVoucherView.getTable().getItemCount(); i++) 
		{
			TableItem tbitm= tblVoucherView.getTable().getItem(i);
			System.out.println("acc name column: "+tbitm.getText(0));
			filterAccounts.remove(tbitm.getText(0));
		}

		filterAccounts.add(0,"----------------------------Please select----------------------------");
		String[] FinalList = new String[filterAccounts.size() ];
		for(int convert = 0; convert < filterAccounts.size(); convert ++)
		{
			FinalList[convert] = filterAccounts.get(convert); 
		}
		return FinalList;
	}
	private void doTally()
	{
		// from here
		// TODO Auto-generated method stub
		//super.focusLost(arg0);
		try {
			String testamount = nf.format(Double.parseDouble(txtCrDrAmount.getText().trim() ));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			MessageBox msgerr = new MessageBox(new Shell(),SWT.OK| SWT.ERROR |SWT.ICON_ERROR);
			msgerr.setText("Invalid Amount");
			msgerr.setMessage("You have entered an Invalid Amount");
			msgerr.open();
			tblVoucherView.getTable().setEnabled(false);
			return;
		}

		if(cmbAccounts.getSelectionIndex()== -1 || cmbAccounts.getSelectionIndex()== 0)
		{
			MessageBox msgvalidate = new MessageBox(new Shell(),SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
			msgvalidate.setText("Error!");
			msgvalidate.setMessage("Please Select an Account");
			//msgvalidate.open();
			cmbAccounts.setFocus();
				return;
			
		}
		Double amount = Double.valueOf(txtCrDrAmount.getText().trim());
//		if(cmbAccounts. )
		if(amount==0 || txtCrDrAmount.getText().trim().equals(""))
		{
			MessageBox msgamount = new MessageBox(new Shell(), SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
			msgamount.setText("Error!");
			msgamount.setMessage("Please Enter the Amount");
			//msgamount.open();
			Display.getCurrent().asyncExec(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					txtCrDrAmount.setFocus();
					txtCrDrAmount.setText("0.00");
				}
			});
			
			
			return;
		}
		

		if(cmbAccounts.getSelectionIndex()>= 1  )
		{
			SelectedAccounts.add(cmbAccounts.getItem(cmbAccounts.getSelectionIndex()));
			
		}
		
		if(tblVoucherView.getTable().getItemCount()<= 1)
		{
			if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Dr"))
				{
				/*MessageBox msgdr = new MessageBox(new Shell(),SWT.OK);
				msgdr.setMessage("dr is selected");
				msgdr.open();
				*/
				AddVoucher row = new AddVoucher("Dr", cmbAccounts.getItem(cmbAccounts.getSelectionIndex() ) , Double.parseDouble(txtCrDrAmount.getText() ),0 );
				totalDr.setText(nf.format(Double.parseDouble(txtCrDrAmount.getText())));
				cmbCr_Dr.removeAll();
				cmbCr_Dr.add("Cr");
				cmbCr_Dr.select(0);
				cmbCr_Dr.setEnabled(false);
				//txtCrDrAmount.setText();
				if(typeFlag.equals("Contra"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Payment"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Journal"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Receipt"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Sales"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Purchase"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Credit Note"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Debit Note"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Sales Return"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Purchase Return"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
					
					cmbAccounts.select(0);

				}
	//condition to either add new or edit existing row.
				cmbAccounts.setFocus();
				if(alterFlag == false)
				{
					tblVoucherView.add(row);
					tblVoucherView.getTable().setEnabled(true);
				}
				else
				{
					MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
					AddVoucher removableRow = (AddVoucher) tblVoucherView.getElementAt(selectedIndex);
					msgtest.setMessage("This is the first row and it is Dr and account name is "+ removableRow.getAccountName()  );
					//msgtest.open();
					tblVoucherView.remove(removableRow);
					
					tblVoucherView.insert(row, selectedIndex);
					tblVoucherView.getTable().setEnabled(true);
					alterFlag = false;
				}
				totalDrAmount = 0;
				totalCrAmount = 0;
				


				alterFlag = false;
				return;

				}

							if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Cr"))
		{
								AddVoucher row = new AddVoucher("Cr",cmbAccounts.getItem(cmbAccounts.getSelectionIndex()) , 0, Double.parseDouble(txtCrDrAmount.getText()));
								if(alterFlag == false)
								{
									MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
									msgtest.setMessage("alter flag is false and we are in Cr second row");
									//msgtest.open();

									tblVoucherView.add(row);
									tblVoucherView.getTable().setEnabled(true);
									cmbCr_Dr.setEnabled(true);
								}
								else
								{
									MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
									AddVoucher removeableRow = (AddVoucher) tblVoucherView.getElementAt(selectedIndex);
									tblVoucherView.remove(removeableRow);
									msgtest.setMessage("alter flag is true and we are in Cr second row .  The account name is " + removeableRow.getAccountName() );
									//msgtest.open();
									
									tblVoucherView.insert(row, selectedIndex);
									tblVoucherView.getTable().setEnabled(true);
									alterFlag = false;
								}
								

								totalCr.setText(nf.format(Double.parseDouble(txtCrDrAmount.getText())));
								totalDrAmount = 0;
								totalCrAmount = 0;
								for(int RowCounter = 0; RowCounter < tblVoucherView.getTable().getItemCount(); RowCounter ++ )
								{
									AddVoucher VoucherRow = (AddVoucher) tblVoucherView.getElementAt(RowCounter);
									totalDrAmount = totalDrAmount + VoucherRow.getDrAmount();
									totalCrAmount = totalCrAmount + VoucherRow.getCrAmount();
								}
								totalDr.setText(nf.format(totalDrAmount));
								totalCr.setText(nf.format(totalCrAmount));
								if(totalDrAmount == totalCrAmount)
								{
									
									cmbCr_Dr.removeAll();
									cmbAccounts.removeAll();
									txtCrDrAmount.setText("");
									//lblCrDr.setVisible(false);
									cmbCr_Dr.setVisible(false);
									lblAccounts.setVisible(false);
									cmbAccounts.setVisible(false);
									tblVoucherView.getTable().setEnabled(true);
									lblCrDrAmount.setVisible(false);
									txtCrDrAmount.setVisible(false);
									grpEntry.setVisible(false);
									btnConfirm.setEnabled(true);
									if(comboselprj.getVisible())
									{
										comboselprj.setFocus();
									}
									else
									{
										txtnarration.setFocus();
									}
									return;
								}
								if(totalDrAmount> totalCrAmount)
								{
									cmbCr_Dr.removeAll();
									cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
									cmbCr_Dr.select(1);
									if(typeFlag.equals("Contra"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Payment"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Journal"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Receipt"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Sales"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Purchase"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Credit Note"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Debit Note"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Sales Return"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Purchase Return"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
										
										cmbAccounts.select(0);

									}


									cmbCr_Dr.setFocus();
									txtCrDrAmount.setText(nf.format(totalDrAmount - totalCrAmount).toString());

								}
								if(totalCrAmount> totalDrAmount)
								{
									cmbCr_Dr.removeAll();
									cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
									cmbCr_Dr.select(0);
									if(typeFlag.equals("Contra"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Payment"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Journal"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Receipt"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Sales"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Purchase"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Credit Note"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Debit Note"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Sales Return"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));
										
										cmbAccounts.select(0);
									}
									if(typeFlag.equals("Purchase Return"))
									{
										cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
										
										cmbAccounts.select(0);

									}


									cmbCr_Dr.setFocus();
									txtCrDrAmount.setText(nf.format(totalCrAmount - totalDrAmount).toString());

								}
								

		}
							alterFlag = false;
							return;

		}
		if(tblVoucherView.getTable().getItemCount() > 1 )
		{
			//todo
			//cmbCr_Dr.setEnabled(true);
			if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Dr"))
			{
				AddVoucher row = new AddVoucher("Dr", cmbAccounts.getItem(cmbAccounts.getSelectionIndex() ) , Double.parseDouble(txtCrDrAmount.getText() ),0 );
				if(alterFlag == false)
				{
					tblVoucherView.add(row);
					tblVoucherView.getTable().setEnabled(true);
				}
				else
				{

					//MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
					AddVoucher removableRow = (AddVoucher) tblVoucherView.getElementAt(selectedIndex);
					//msgtest.setMessage("This is the first row and it is Dr and account name is "+ removableRow.getAccountName()  );
					//msgtest.open();
					tblVoucherView.remove(removableRow);
					
					tblVoucherView.insert(row, selectedIndex);
					tblVoucherView.getTable().setEnabled(true);
					alterFlag = false;
				}
				//process for tally of transaction follows.
				totalDrAmount = 0;
				totalCrAmount = 0;
				for(int RowCounter = 0; RowCounter < tblVoucherView.getTable().getItemCount(); RowCounter ++ )
				{
					AddVoucher VoucherRow = (AddVoucher) tblVoucherView.getElementAt(RowCounter);
					totalDrAmount = totalDrAmount + VoucherRow.getDrAmount();
					totalCrAmount = totalCrAmount + VoucherRow.getCrAmount();
				}
				totalDr.setText(nf.format(totalDrAmount));
				totalCr.setText(nf.format(totalCrAmount));
				if(totalDrAmount == totalCrAmount)
				{
					cmbCr_Dr.removeAll();
					cmbAccounts.removeAll();
					txtCrDrAmount.setText("");
					//lblCrDr.setVisible(false);
					cmbCr_Dr.setVisible(false);
					lblAccounts.setVisible(false);
					cmbAccounts.setVisible(false);
					tblVoucherView.getTable().setEnabled(true);
					lblCrDrAmount.setVisible(false);
					txtCrDrAmount.setVisible(false);
					if(comboselprj.getVisible())
					{
						comboselprj.setFocus();
					}
					else
					{
						txtnarration.setFocus();
					}
					return;
				}
				
				if(selectedIndex < (tblVoucherView.getTable().getItemCount() -1)&& selectedIndex!= -1 )
				{
					//don't add an extra row.
					cmbCr_Dr.removeAll();
					cmbAccounts.removeAll();
					txtCrDrAmount.setText("");
					//lblCrDr.setVisible(false);
					cmbCr_Dr.setVisible(false);
					lblAccounts.setVisible(false);
					cmbAccounts.setVisible(false);
					tblVoucherView.getTable().setEnabled(true);
					lblCrDrAmount.setVisible(false);
					txtCrDrAmount.setVisible(false);
					tblVoucherView.getTable().setFocus();
					tblVoucherView.getTable().select(selectedIndex+1);
					return;

				}
				if(totalDrAmount> totalCrAmount)
				{
					cmbCr_Dr.removeAll();
					cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
					cmbCr_Dr.select(1);
					MessageBox msgdr = new MessageBox(new Shell(),SWT.OK);
					msgdr.setMessage("we are in the condition where Dr is more so a Cr row will come for editing");
					//msgdr.open();
					if(typeFlag.equals("Contra"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Payment"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Journal"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Receipt"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Credit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Debit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
						
						cmbAccounts.select(0);

					}


					cmbCr_Dr.setFocus();
					MessageBox msgfocus = new MessageBox(new Shell(),SWT.OK);
					msgfocus.setMessage("focused on CRDR now we can take actions.");
					//msgfocus.open();
					txtCrDrAmount.setText(nf.format(totalDrAmount - totalCrAmount).toString());
					return;

				}
				if(totalCrAmount> totalDrAmount)
				{
					cmbCr_Dr.removeAll();
					cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
					cmbCr_Dr.select(0);
					if(typeFlag.equals("Contra"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
					
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Payment"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));
					
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Journal"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Receipt"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Credit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Debit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
						
						cmbAccounts.select(0);

					}


					cmbCr_Dr.setFocus();
					txtCrDrAmount.setText(nf.format(totalCrAmount - totalDrAmount).toString());

				}


			}
			MessageBox msgindex = new MessageBox(new Shell(),SWT.OK);
			msgindex.setMessage("the selected index is "+ Integer.toString(cmbCr_Dr.getSelectionIndex()));
		//	msgindex.open();
			if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Cr"))
			{
				AddVoucher row = new AddVoucher("Cr", cmbAccounts.getItem(cmbAccounts.getSelectionIndex() ) ,0.00, Double.parseDouble(txtCrDrAmount.getText() ));
				if(alterFlag == false)
				{

					tblVoucherView.add(row);
					tblVoucherView.getTable().setEnabled(true);
				}
				else
				{

					MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
					AddVoucher removableRow = (AddVoucher) tblVoucherView.getElementAt(selectedIndex);
					msgtest.setMessage("This is the first row and it is Dr and account name is "+ removableRow.getAccountName()  );
					//msgtest.open();
					tblVoucherView.remove(removableRow);
					
					tblVoucherView.insert(row, selectedIndex);
					tblVoucherView.getTable().setEnabled(true);
					alterFlag = false;
				}

				//process for tally of transaction follows.
				totalDrAmount = 0;
				totalCrAmount = 0;
				for(int RowCounter = 0; RowCounter < tblVoucherView.getTable().getItemCount(); RowCounter ++ )
				{
					AddVoucher VoucherRow = (AddVoucher) tblVoucherView.getElementAt(RowCounter);
					totalDrAmount = totalDrAmount + VoucherRow.getDrAmount();
					totalCrAmount = totalCrAmount + VoucherRow.getCrAmount();
				}
				totalDr.setText(nf.format(totalDrAmount));
				totalCr.setText(nf.format(totalCrAmount));
				if(totalDrAmount == totalCrAmount)
				{
					cmbCr_Dr.removeAll();
					cmbAccounts.removeAll();
					txtCrDrAmount.setText("");
					//lblCrDr.setVisible(false);
					cmbCr_Dr.setVisible(false);
					lblAccounts.setVisible(false);
					cmbAccounts.setVisible(false);
					tblVoucherView.getTable().setEnabled(true);
					lblCrDrAmount.setVisible(false);
					txtCrDrAmount.setVisible(false);
					grpEntry.setVisible(false);
					btnConfirm.setEnabled(true);
					if(comboselprj.getVisible())
					{
						comboselprj.setFocus();
					}
					else
					{
						txtnarration.setFocus();
					}
					return;
				}
				
				if(selectedIndex < (tblVoucherView.getTable().getItemCount() -1)&& selectedIndex!= -1  )
				{
					//don't add an extra row.
					cmbCr_Dr.removeAll();
					cmbAccounts.removeAll();
					txtCrDrAmount.setText("");
					//lblCrDr.setVisible(false);
					cmbCr_Dr.setVisible(false);
					lblAccounts.setVisible(false);
					cmbAccounts.setVisible(false);
					tblVoucherView.getTable().setEnabled(true);
					lblCrDrAmount.setVisible(false);
					txtCrDrAmount.setVisible(false);
					tblVoucherView.getTable().setFocus();
					tblVoucherView.getTable().select(selectedIndex+1);
					return;

				}

				if(totalDrAmount> totalCrAmount)
				{
					cmbCr_Dr.removeAll();
					cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
					cmbCr_Dr.select(1);
					if(typeFlag.equals("Contra"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Payment"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Journal"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Receipt"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Credit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Debit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));

						cmbAccounts.select(0);

					}


					cmbCr_Dr.setFocus();
					txtCrDrAmount.setText(nf.format(totalDrAmount - totalCrAmount).toString());

				}
				if(totalCrAmount> totalDrAmount)
				{
					cmbCr_Dr.removeAll();
					cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
					cmbCr_Dr.select(0);
					if(typeFlag.equals("Contra"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Payment"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Journal"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Receipt"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Credit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Debit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));

						cmbAccounts.select(0);

					}


					cmbCr_Dr.setFocus();
					txtCrDrAmount.setText(nf.format(totalCrAmount - totalDrAmount).toString());

			}

		}
		
		
		
		
		}
	if(tblVoucherView.getTable().getItemCount()>9 )
	{
	//tblVoucherView.scrollDown(0, 50);
	tblVoucherView.getTable().setTopIndex(tblVoucherView.getTable().getItemCount()-1 );

	}
	if(totalCrAmount != totalDrAmount|| totalCrAmount == 0 || totalDrAmount == 0)
	{
		btnConfirm.setEnabled(false); 
	}
	alterFlag = false;
	//till here.

	}


	public void makeaccssible(Control c) {
		c.getAccessible();
	}

}


	