package gnukhata.views;
import gnukhata.globals;
import gnukhata.controllers.accountController;
import gnukhata.controllers.reportmodels.AddMultipleAcc;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.GroupLayout.Group;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
 
public class AddMultipleAccounts extends Dialog {
	
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	
Group grp;
  String value;
  String Grpname;
  String SubGrpName;
  String accname;
  Double openingbalance;
  String suggestedAccountCode = "null";
  String TxtOpeningBal;
  String accname2;
  Object[] queryParams = new Object[8];
  public static String newAccount;
  public static Boolean cancelflag = false;
  public static Boolean saveflag = false;
  boolean DecimalFlag = false;
  boolean accountExists = false;
  String searchText = "";
  long searchTexttimeout = 0;
  ArrayList<AddMultipleAcc>  lstVoucherRows;

 
  /**
   * @param parent
 * @param subgrpname 
 * @param grpname 
   */
  public AddMultipleAccounts(Shell parent, String grpname, String subgrpname) {
    super(parent);
    Grpname=grpname;
    SubGrpName=subgrpname;
  }
 
  /**
   * @param parent
   * @param style
   */
 
 
  
  
/**
   * Makes the dialog visible.
   *
   * @return
   */
  
  public String open()
	{
	 
	  	Shell parent = getParent();
	 	final Shell shell = new Shell(parent, SWT.TITLE | SWT.BORDER | SWT.APPLICATION_MODAL);
		shell.setText("Create Multiple Accounts");
		final int shellwidth = shell.getClientArea().width;
		final int finshellwidth = shellwidth-(shellwidth*10/100);
		final int shellheight = shell.getClientArea().height;
		final int finshellheight = shellheight-(shellheight*10/100);
		shell.setSize(finshellwidth, finshellheight);
	//	final int insetX = 4, insetY = 4;
		FormLayout formlayout = new FormLayout();
		shell.setLayout(formlayout);
      
		
      final Label lblgroup = new Label(shell, SWT.NULL);
      lblgroup.setText("&Group Name:");
      FormData layout=new FormData();
      layout.top= new FormAttachment(4);
      //layout.bottom= new FormAttachment(16);
      layout.left= new FormAttachment(2);
      //layout.right= new FormAttachment(25);
      lblgroup.setLayoutData(layout);
      lblgroup.setVisible(true);
     
      
      final Text txtgroup = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER);      
      layout=new FormData();
      txtgroup.setText(Grpname);
      layout.top= new FormAttachment(3);
      layout.left= new FormAttachment(lblgroup,5);
      layout.right= new FormAttachment(44);
      txtgroup.setLayoutData(layout);
      queryParams[0] = Grpname;
      txtgroup.setEditable(false);
      
      
      
      final Label lblsubgroup = new Label(shell, SWT.NULL);
      lblsubgroup.setText("Sub-Group Name:");
      layout=new FormData();
      layout.top= new FormAttachment(4);
      //layout.bottom= new FormAttachment(16);
      layout.left= new FormAttachment(46);
      //layout.right= new FormAttachment(25);
      lblsubgroup.setLayoutData(layout);
           
      final Text txtsubgroup = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER);
      txtsubgroup.setText(SubGrpName);
      layout=new FormData();
      layout.top= new FormAttachment(3);
      layout.left= new FormAttachment(lblsubgroup,5);
      layout.right= new FormAttachment(98);
      txtsubgroup.setLayoutData(layout);
      queryParams[1]=SubGrpName;
      queryParams[2]="";
      txtsubgroup.setEditable(false);
      
            
      
      
       
		
		final Label lblaccountname = new Label(shell, SWT.NULL);
	      lblaccountname.setText("Account Name");
	      layout=new FormData();
	      layout.top= new FormAttachment(73);
	      //layout.bottom= new FormAttachment(16);
	      layout.left= new FormAttachment(3);
	      //layout.right= new FormAttachment(25);
	      lblaccountname.setLayoutData(layout);
	      	      
	      final Text txtaccountname = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER);
	      layout=new FormData();
	      layout.top= new FormAttachment(73);
	      layout.left= new FormAttachment(lblaccountname,5);
	      layout.right= new FormAttachment(50);
	      txtaccountname.setLayoutData(layout);
	      txtaccountname.setFocus();

	      final Label lblopeningbal = new Label(shell, SWT.NULL);
	      lblopeningbal.setText("Opening Balance");
	      layout=new FormData();
	      layout.top= new FormAttachment(73);
	      //layout.bottom= new FormAttachment(16);
	      layout.left= new FormAttachment(52);
	      layout.right= new FormAttachment(73);
	      lblopeningbal.setLayoutData(layout);
	           
	    
	      
	      final Text txtopeningbal = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER|SWT.RIGHT);
	      layout=new FormData();
	      layout.top= new FormAttachment(73);
	      layout.left= new FormAttachment(73);
	      layout.right= new FormAttachment(95);
	      txtopeningbal.setLayoutData(layout);
	      txtopeningbal.setText("0.00");

		  
	    	
      final Button btnSave = new Button(shell, SWT.NONE);
      btnSave.setText("&Finish");
      layout = new FormData();
      layout.top= new FormAttachment(89);
      layout.left= new FormAttachment(25);
      btnSave.setLayoutData(layout);
      
      final Button btnCancel = new Button(shell, SWT.NONE);
      btnCancel.setText("&Cancel");
      layout = new FormData();
      layout.top= new FormAttachment(89);
      layout.left= new FormAttachment(65);
      btnCancel.setLayoutData(layout);
      
      final TableViewer tblmultipleaccounts = new TableViewer(shell, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
	    tblmultipleaccounts.getTable().setFont(new Font(getDisplay(),"UBUNTU",10,SWT.BOLD));
	    tblmultipleaccounts.getTable().setLinesVisible (true);
		tblmultipleaccounts.getTable().setHeaderVisible (true);
		layout = new FormData();
		layout.top = new FormAttachment(13);
		layout.left= new FormAttachment(10);
		layout.bottom = new FormAttachment(70);
		layout.right= new FormAttachment(85);
      tblmultipleaccounts.getTable().setLayoutData(layout);
      tblmultipleaccounts.getControl().forceFocus();
      lstVoucherRows = new ArrayList<AddMultipleAcc>();
		
      final TableViewerColumn colaccName  = new TableViewerColumn(tblmultipleaccounts, SWT.None);
		colaccName.getColumn().setText("Account Name");
		colaccName.getColumn().setAlignment(SWT.CENTER);
		colaccName.getColumn().setWidth(40* finshellwidth /100);
		colaccName.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				AddMultipleAcc row = (AddMultipleAcc) element;
				return row.getAccountName();
				
				//return super.getText(element);
			}
		}
		
				);
		final TableViewerColumn colopeningbal  = new TableViewerColumn(tblmultipleaccounts, SWT.None);
		colopeningbal.getColumn().setText("Opening Balance");
		colopeningbal.getColumn().setAlignment(SWT.RIGHT);
		colopeningbal.getColumn().setWidth(20* finshellwidth /100);
		colopeningbal.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				AddMultipleAcc row = (AddMultipleAcc) element;
				NumberFormat nf = NumberFormat.getInstance();
				nf.setGroupingUsed(false);
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				
				if (!txtopeningbal.isVisible()) 
				{
					return "";
				}
				else{
					return nf.format(row.getOpeningBal());
				}
				
				//return super.getText(element);
			}
		}
		);
		tblmultipleaccounts.setContentProvider(new ArrayContentProvider());
		tblmultipleaccounts.setInput(lstVoucherRows);
      
      Background =  new Color(this.getDisplay() ,220 , 224, 227);
      Foreground = new Color(this.getDisplay() ,0, 0,0 );
      FocusBackground  = new Color(this.getDisplay(),78,97,114 );
      FocusForeground = new Color(this.getDisplay(),255,255,255);
      BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);

      txtaccountname.setFocus();
      txtaccountname.setBackground(FocusBackground);
      txtaccountname.setForeground(FocusForeground);
      
      txtaccountname.addKeyListener(new KeyAdapter() {
    @Override
    	public void keyPressed(KeyEvent arg0) {
    		// TODO Auto-generated method stub
    		//super.keyPressed(arg0);
    	if(arg0.keyCode == SWT.ARROW_DOWN)
    	{
    		btnSave.setFocus();
    	}
    	
    	}	  
	});
  
	if(txtgroup.getText().trim().equals("Current Assets") || txtgroup.getText().trim().equals("Fixed Assets") || txtgroup.getText().trim().equals("Investment") || txtgroup.getText().trim().equals("Loans(Asset)"))
	{
		lblopeningbal.setText("Debit &Opening Balance");
	}
	if (txtgroup.getText().trim().equals("Capital") || txtgroup.getText().trim().equals("Corpus") || txtgroup.getText().trim().equals("Current Liability") || txtgroup.getText().trim().equals("Loans(Liability)") || txtgroup.getText().trim().equals("Miscellaneous Expenses(Asset)") || txtgroup.getText().trim().equals("Reserves") )
	{
		lblopeningbal.setText("Credit &Opening Balance");
	}
	if( txtgroup.getText().trim().equals("Direct Income") || txtgroup.getText().trim().equals("Indirect Income") || txtgroup.getText().trim().equals("Direct Expense") || txtgroup.getText().trim().equals("Indirect Expense") )
	{
		lblopeningbal.setVisible(false);
		colopeningbal.getColumn().dispose();
		txtopeningbal.setVisible(false);
	
		//dropdownSubGroupName.add("No Sub-Group");
	}
	else
	{
		lblopeningbal.setVisible(true);
		txtopeningbal.setVisible(true);
	}
txtaccountname.addFocusListener(new FocusAdapter() {
	@Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
	//	super.focusGained(arg0);
		txtaccountname.setBackground(FocusBackground);
		txtaccountname.setForeground(FocusForeground);
		
		/*if(globals.session[5].toString().equals("manually") )
		{
			queryParams[4] = "manually";
			suggestedAccountCode = suggestedAccountCode + txtaccountname.getText().substring(0,1);
			suggestedAccountCode = accountController.getSuggestedAccountCode(suggestedAccountCode);
	//		txtAccountCode.setText(suggestedAccountCode);
			queryParams[7] = suggestedAccountCode;
		}
		else
		{
			queryParams[7] = "";
		}*/
		queryParams[4] = globals.session[5];
		if(queryParams[4] =="")
		{
			queryParams[7] = "";
		}
		
		}
	@Override
	public void focusLost(FocusEvent arg0) {
	// TODO Auto-generated method stub
	//	super.focusLost(arg0);
		txtaccountname.setBackground(Background);
		txtaccountname.setForeground(Foreground);
		
	
	}
});

txtopeningbal.addFocusListener(new FocusAdapter() {
	/*@Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusGained(arg0);
		txtopeningbal.setBackground(FocusBackground);
		txtopeningbal.setForeground(FocusForeground);
		String result = accountController.accountExists(txtaccountname.getText().trim());
		if (Integer.valueOf(result) == 1)
		{
			MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_WARNING);
			msg.setText("Warning!");
			msg.setMessage("The account name you entered already exists, please choose another name.");
			msg.open();
			
			Display.getCurrent().asyncExec(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					txtaccountname.setText("");
					txtaccountname.setFocus();
					
				}
			});
			return;
		
		}
		String accname1 = txtaccountname.getText().trim();
		if (tblmultipleaccounts.getTable().getItemCount()>0)
		{
			for (int rowIndex = 0; rowIndex < tblmultipleaccounts.getTable().getItemCount(); rowIndex++)
			{

				AddMultipleAcc AccountRow = (AddMultipleAcc) tblmultipleaccounts.getElementAt(rowIndex);
				String accountname = AccountRow.getAccountName();
				System.out.println(accountname);
				if (accountname.equals(accname1)) 
				{
					MessageBox msg = new MessageBox(new Shell(), SWT.OK	| SWT.ERROR | SWT.ICON_WARNING);
					msg.setText("Warning!");
					msg.setMessage("The account name you entered already exists, please choose another name.");
					msg.open();

					Display.getCurrent().asyncExec(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtaccountname.setText("");
							txtaccountname.setFocus();

						}
					});
					return;

				}

			}
		}

	}*/
	@Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusLost(arg0);
		txtopeningbal.setBackground(Background);
		txtopeningbal.setForeground(Foreground);
		
	}
});
      txtopeningbal.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_SUBTRACT:
	            
	                return;
				}
				if(arg0.keyCode == SWT.KEYPAD_DECIMAL&& DecimalFlag == false)
				{
					DecimalFlag = true;
					return;
				}
				if(arg0.keyCode == SWT.KEYPAD_DECIMAL&& DecimalFlag == true)
				{
					arg0.doit= false;
					return;
				}
				
		if(arg0.keyCode == 46)
		{
			return;
		}
		
		if(arg0.keyCode == 62)
		{
			  arg0.doit = false;
			
		}
		if(arg0.keyCode == 45)
		{
			return;
		}
	
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }
	        

				
			}
		});
      
      txtaccountname.addTraverseListener(new TraverseListener() {
		
		@Override
		public void keyTraversed(TraverseEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.detail == SWT.TRAVERSE_RETURN||arg0.detail == SWT.TRAVERSE_TAB_NEXT)
			{
				String result = accountController.accountExists(txtaccountname.getText().trim());
				if (Integer.valueOf(result) == 1)
				{
					MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_WARNING);
					msg.setText("Warning!");
					msg.setMessage("The account name you entered already exists, please choose another name.");
					msg.open();
					
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtaccountname.setText("");
							txtaccountname.setFocus();
							
						}
					});
					return;
				
				}
				if (txtaccountname.getText().trim().equals("Profit for the Year")||txtaccountname.getText().trim().equals("Loss for the Year")||txtaccountname.getText().trim().equals("Surplus for the Year")||txtaccountname.getText().trim().equals("Deficit for the Year"))
				{
					MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msg.setText("Warning!");
					msg.setMessage("This is a reserved account name, please choose another name.");
					txtaccountname.setText("");
					txtaccountname.setFocus();
					msg.open();
					return;
				}
				if(txtaccountname.getText().trim().equals(""))
	    		{
					MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msg.setText("Error");
					msg.setMessage("Please enter an Account Name");
					msg.open();
					return;
	    		}
				String accname1 = txtaccountname.getText().trim();
				if (tblmultipleaccounts.getTable().getItemCount()>0)
				{
					for (int rowIndex = 0; rowIndex < tblmultipleaccounts.getTable().getItemCount(); rowIndex++)
					{

						AddMultipleAcc AccountRow = (AddMultipleAcc) tblmultipleaccounts.getElementAt(rowIndex);
						String accountname = AccountRow.getAccountName();
						System.out.println(accountname);
						if (accountname.equals(accname1)) 
						{
							MessageBox msg = new MessageBox(new Shell(), SWT.OK	| SWT.ERROR | SWT.ICON_WARNING);
							msg.setText("Warning!");
							msg.setMessage("The account name you entered already exists, please choose another name.");
							msg.open();

							Display.getCurrent().asyncExec(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtaccountname.setText("");
									txtaccountname.setFocus();

								}
							});
							return;

						}

					}
				}
					if(txtaccountname.getText().trim().equals(""))
				{
					MessageBox msgvalidate = new MessageBox(new Shell(),SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
					msgvalidate.setText("Error!");
					msgvalidate.setMessage("Please give Account Name");
					//msgvalidate.open();
					txtaccountname.setFocus();
						return;
					
				}
				if(txtopeningbal.isVisible())
				{
					txtopeningbal.setFocus();
				}
				else {
					
					if(tblmultipleaccounts.getTable().getItemCount()>=0)
					{
						AddMultipleAcc row = new AddMultipleAcc(Double.parseDouble("0.00") , txtaccountname.getText().trim());
	
						
					tblmultipleaccounts.add(row);
					if(tblmultipleaccounts.getTable().getItemCount()>7)
					{
						tblmultipleaccounts.getTable().setTopIndex(tblmultipleaccounts.getTable().getItemCount()-1 );
					}
					
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtaccountname.setText("");
							//txtopeningbal.setText("0.00");
							txtaccountname.setFocus();		
							
							return;
						}
					});
				}
										
			}
		}
		}
	});
    
      
  	txtopeningbal.addTraverseListener(new TraverseListener() {
		
		@Override
		public void keyTraversed(TraverseEvent arg0) {
			// TODO Auto-generated method stub
			
							if(arg0.detail == SWT.TRAVERSE_RETURN)
							{

								if(txtaccountname.getText().trim().equals(""))
								{
									MessageBox msgvalidate = new MessageBox(new Shell(),SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
									msgvalidate.setText("Error!");
									msgvalidate.setMessage("Please enter an Account Name");
									msgvalidate.open();
									txtaccountname.setFocus();
										return;
									
								}
								
								
								if( txtopeningbal.getText().trim().startsWith(".") || txtopeningbal.getText().trim().endsWith(".") || txtopeningbal.getText().trim().endsWith("-")  )
								{
									MessageBox msgValueError = new MessageBox(new Shell(), SWT.OK | SWT.ICON_ERROR);
									msgValueError.setText("Error!");
									msgValueError.setMessage("Please enter a proper amount");
									msgValueError.open();
									Display.getCurrent().asyncExec(new Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											txtopeningbal.setFocus();
											txtopeningbal.setText("");
										}
									});      
									
									arg0.doit = false;
								
									return;
										}
								if(txtopeningbal.getText().equals(""))
								{
								txtopeningbal.setText("0.00");	
								}
							/*	if(txtopeningbal.getText().trim().startsWith("-"))
										{
									txtopeningbal.setText("-" + txtopeningbal.getText());
									arg0.doit = false;
									return;
									
										}
							*/	try {
								if(tblmultipleaccounts.getTable().getItemCount() >= 0)
								{
									AddMultipleAcc row = new AddMultipleAcc(Double.parseDouble(txtopeningbal.getText().trim()) , txtaccountname.getText().trim());
														
									tblmultipleaccounts.add(row);
									if(tblmultipleaccounts.getTable().getItemCount()>7)
									{
										tblmultipleaccounts.getTable().setTopIndex(tblmultipleaccounts.getTable().getItemCount()-1 );
									}

									Display.getCurrent().asyncExec(new Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
										//	txtaccountname.setText(arg0);
											txtaccountname.setText("");
											txtopeningbal.setText("0.00");
										
											txtaccountname.setFocus();		
										}
									});
								
								return;
								}
							} catch (NumberFormatException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								MessageBox msgvalidate = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
								msgvalidate.setText("Error!");
								msgvalidate.setMessage("Improper Opening Balance");
								msgvalidate.open();
								Display.getCurrent().asyncExec(new Runnable() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										txtopeningbal.setFocus();
										txtopeningbal.setText("");
									}
								});
								
								//msgvalidate.open();
								return;
								
								
							}
								tblmultipleaccounts.getTable().setEnabled(true);

								}
							if(arg0.detail == SWT.TRAVERSE_TAB_PREVIOUS || arg0.detail == SWT.TRAVERSE_ARROW_PREVIOUS)
							{
								txtaccountname.setFocus();
								arg0.doit = false;
								return;
							}
							if(arg0.detail==SWT.TRAVERSE_TAB_NEXT )
							{
								btnSave.setFocus();
							}
							
				arg0.doit = false;
				
				if(txtaccountname.getText().trim().equals(""))
				{
					MessageBox msgvalidate = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
					msgvalidate.setText("Error!");
					msgvalidate.setMessage("Please give Account Name");
					//msgvalidate.open();
					txtaccountname.setFocus();
						return;
					
				}
					
				
				
		}
	});
  	
  btnSave.addFocusListener(new FocusAdapter() {
	  @Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusGained(arg0);
		  btnSave.setBackground(FocusBackground);
		  btnSave.setForeground(BtnFocusForeground);
			
	}
	  @Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusLost(arg0);
		  btnSave.setBackground(Background);
		  btnSave.setForeground(Foreground);
			
	  }
});
  btnCancel.addFocusListener(new FocusAdapter() {
	  @Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusGained(arg0);
		  btnCancel.setBackground(FocusBackground);
		  btnCancel.setForeground(BtnFocusForeground);
			
	}
	  @Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusLost(arg0);
		  btnCancel.setBackground(Background);
		  btnCancel.setForeground(Foreground);
			
	  }
});
tblmultipleaccounts.getTable().addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		//super.widgetSelected(arg0);
		tblmultipleaccounts.getTable().setBackground(FocusBackground);
		tblmultipleaccounts.getTable().setForeground(FocusForeground);

	}
});
tblmultipleaccounts.getTable().addFocusListener(new FocusAdapter() {
	@Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
	//	super.focusLost(arg0);
		tblmultipleaccounts.getTable().setBackground(Background);
		tblmultipleaccounts.getTable().setForeground(Foreground);

	}
});
  	btnSave.addSelectionListener(new SelectionAdapter() {
  		@Override
  		public void widgetSelected(SelectionEvent arg0) {
  			// TODO Auto-generated method stub
  			//super.widgetSelected(arg0);
  			String result1 = accountController.accountExists(txtaccountname.getText().trim());
			if (Integer.valueOf(result1) == 1)
			{
				MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_WARNING);
				msg.setText("Warning!");
				msg.setMessage("The account name you entered already exists, please choose another name.");
				msg.open();
				
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtaccountname.setText("");
						txtaccountname.setFocus();
						
					}
				});
				return;
			
			}
			if (txtaccountname.getText().trim().equals("Profit for the Year")||txtaccountname.getText().trim().equals("Loss for the Year")||txtaccountname.getText().trim().equals("Surplus for the Year")||txtaccountname.getText().trim().equals("Deficit for the Year"))
			{
				MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
				msg.setText("Warning!");
				msg.setMessage("This is a reserved account name, please choose another name.");
				txtaccountname.setText("");
				txtaccountname.setFocus();
				msg.open();
				return;
			}
			String accname1 = txtaccountname.getText().trim();
			if (tblmultipleaccounts.getTable().getItemCount()>0)
			{
				for (int rowIndex = 0; rowIndex < tblmultipleaccounts.getTable().getItemCount(); rowIndex++)
				{

					AddMultipleAcc AccountRow = (AddMultipleAcc) tblmultipleaccounts.getElementAt(rowIndex);
					String accountname = AccountRow.getAccountName();
					System.out.println(accountname);
					if (accountname.equals(accname1)) 
					{
						MessageBox msg = new MessageBox(new Shell(), SWT.OK	| SWT.ERROR | SWT.ICON_WARNING);
						msg.setText("Warning!");
						msg.setMessage("The account name you entered already exists, please choose another name.");
						msg.open();

						Display.getCurrent().asyncExec(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtaccountname.setText("");
								txtaccountname.setFocus();

							}
						});
						return;

					}

				}
			}

  		for(int rowcount= 0 ; rowcount < tblmultipleaccounts.getTable().getItemCount(); rowcount++)
  		{
  			AddMultipleAcc AccountRow = (AddMultipleAcc) tblmultipleaccounts.getElementAt(rowcount);
  			accname=AccountRow.getAccountName();
  			
  			queryParams[3]=accname;
  			openingbalance=AccountRow.getOpeningBal();
  			System.out.println("accountname " +accname + "openingbalaec" +openingbalance);
  			if(openingbalance.equals("") || !txtopeningbal.isEnabled() )
			{
				queryParams[5] = "0.00";
				queryParams[6] = "0.00";
				
			}
			else
			{
				NumberFormat nf = NumberFormat.getInstance();
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				nf.setGroupingUsed(false);

				queryParams[5] = nf.format(openingbalance);
				queryParams[6] = nf.format(openingbalance);
			}
  			
  			boolean result = false;
  			queryParams[7]="";
  			accountController.setAccount(queryParams);
  			/*MessageBox msgamount = new MessageBox(new Shell(), SWT.OK );
			msgamount.setText("Information");
			msgamount.setMessage("Account Added Successfully");
			msgamount.open();*/
			
  			
  			System.out.println(result);
  			
  		}
  		if(txtaccountname.getText().trim().equals("") && tblmultipleaccounts.getTable().getItemCount()==0)
  		{
  			MessageBox msgamount = new MessageBox(new Shell(), SWT.OK | SWT.ERROR );
			msgamount.setText("Error!");
			msgamount.setMessage("Please enter the Account Name");
			msgamount.open();
			Display.getCurrent().asyncExec(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
				txtaccountname.setFocus();	
				}
			});
			
			return;

  		}
if(!txtaccountname.getText().trim().equals(""))
	{
		
  			try {
				accname2=txtaccountname.getText().trim();
				queryParams[3]=accname2;
				if (!txtopeningbal.getText().trim().equals("") ) {
					openingbalance = Double.parseDouble(txtopeningbal.getText().trim());
				}
				if (txtopeningbal.getText().trim().equals("")) 
				{
					openingbalance= 0.00;
				}
				System.out.println("accountname " +accname + "openingbalaec" +openingbalance);
				if(openingbalance.equals("") || !txtopeningbal.isEnabled() )
				{
					queryParams[5] = "0.00";
					queryParams[6] = "0.00";
					
				}
				else
				{
					NumberFormat nf = NumberFormat.getInstance();
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					nf.setGroupingUsed(false);

					queryParams[5] = nf.format(openingbalance);
					queryParams[6] = nf.format(openingbalance);
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				MessageBox msgValue = new MessageBox(new Shell(), SWT.OK | SWT.ICON_ERROR);
				msgValue.setText("Error!");
				msgValue.setMessage("Please enter a proper amount");
				txtopeningbal.setFocus();
				msgValue.open();
				return;
				
			}
  			System.out.println("q0= " +queryParams[0] + "q1= " +queryParams[1]  +"q2= " +queryParams[2] +"q3= " +queryParams[3] +"q4= " +queryParams[4] +"q5= " +queryParams[5] +"q6= " +queryParams[6] +"q7= " +queryParams[7]);
  			boolean result = false;
  			queryParams[7]="";
  			accountController.setAccount(queryParams);
  			/*MessageBox msgamount = new MessageBox(new Shell(), SWT.OK );
			msgamount.setText("Information");
			msgamount.setMessage("Account Added Successfully");
			msgamount.open();
		
*/  			System.out.println(result);
			
  			
	}
			
			saveflag = true;
			shell.dispose();
	
			
  		
  		
  		}
	});
  	btnSave.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e){
			
			if(e.keyCode == SWT.ARROW_RIGHT);
			{
				//buttonCancel.setFocus();
				btnCancel.setFocus();
			}

			if(e.keyCode == SWT.ARROW_UP)
			{
				txtaccountname.setFocus();
			}
		}
		
	});
	btnCancel.addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyReleased(arg0);
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtaccountname.setFocus();
			}			
			else
			{						
				arg0.doit = false;
			}
			if(arg0.keyCode==SWT.ARROW_LEFT)
			{
				btnSave.setFocus();
			}
		}
	});

      btnCancel.addListener(SWT.Selection, new Listener() {
          public void handleEvent(Event event) {
            //value = null;
        	  //MessageBox msgconfirm = new MessageBox(new Shell(),SWT.YES| SWT.NO|SWT.ICON_QUESTION);
        		CustomDialog msgconfirm = new CustomDialog(new Shell());
     
        	  msgconfirm.SetMessage("Are you sure ?");
				int answer = msgconfirm.open();
				if (answer== SWT.YES) 
				{
					  cancelflag = true;
					  shell.dispose();
				}
				else
				{
					return;
				}
            
          }
        });
      tblmultipleaccounts.getControl().addKeyListener(new KeyAdapter() {
    	  @Override
    	public void keyPressed(KeyEvent arg0) {
    		// TODO Auto-generated method stub
    	//	super.keyPressed(arg0);
    		  if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
				{
				IStructuredSelection VoucherRow= (IStructuredSelection) tblmultipleaccounts.getSelection();
				AddMultipleAcc vr = (AddMultipleAcc) VoucherRow.getFirstElement();
				try {
					if(vr.getAccountName().equals(""))
					{
						return;
					}
				} catch (NullPointerException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					txtaccountname.setFocus();
					return;
				}
				}
    	  }
	});
 /*tblmultipleaccounts.getControl().addMouseListener(new MouseAdapter() {
	 @Override
	public void mouseDoubleClick(MouseEvent arg0) {
		// TODO Auto-generated method stub
		//super.mouseDoubleClick(arg0);
		  IStructuredSelection VoucherRow= (IStructuredSelection) tblmultipleaccounts.getSelection();
			AddVoucher vr = (AddVoucher) VoucherRow.getFirstElement();
			try {
				if(vr.getAccountName().equals(""))
				{
					return;
				}
			} catch (NullPointerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				txtaccountname.setFocus();
				return;
			}
	 }
});
*/      shell.open();
   //   shell.pack();
  
	    Display display = parent.getDisplay();
	    while (!shell.isDisposed()) {
	      if (!display.readAndDispatch())
	        display.sleep();
	    }
	return value;
	}

 
  private Device getDisplay() {
	// TODO Auto-generated method stub
	return null;
}


  
  public static void main(String[] args) {
  
  }
}