package gnukhata.controllers.reportmodels;

public class ProjectList {
	
	private String srNo;
	private String projectName;
	private String projectAmount;

	public ProjectList(String srno,String prjname, String prjAmount) {
		// TODO Auto-generated constructor stub
		super();
		this.srNo = srno;
		this.projectName = prjname;
		this.projectAmount = prjAmount;
	}
	
	public String getSrNo() {
		return srNo;
	}
	
	public String getProjectName() {
		return projectName;
	}
	
	public String getProjectAmount() {
		return projectAmount;
	}
	
	@Override
	public String toString() {
		return "ProjectList [ProjectName=" + projectName + "]";
	}
	
}
