/**
 * 
 */
package gnukhata.controllers.reportmodels;

/**
 * @author inu
 *
 */
public class cashflowReport {
/**
	 * @param accName
	 * @param amounts
	 * @param accName1
	 * @param amounts1
	 */

	private String accName;
	private String amounts;
	private String accName1;
	private String amounts1;
	
	public cashflowReport(String accName, String amounts, String accName1,
			String amounts1) {
		super();
		this.accName = accName;
		this.amounts = amounts;
		this.accName1 = accName1;
		this.amounts1 = amounts1;
	}

	/**
	 * @return the accName
	 */
	public String getAccName() {
		return accName;
	}

	/**
	 * @return the amounts
	 */
	public String getAmounts() {
		return amounts;
	}

	/**
	 * @return the accName1
	 */
	public String getAccName1() {
		return accName1;
	}

	/**
	 * @return the amounts1
	 */
	public String getAmounts1() {
		return amounts1;
	}

}
